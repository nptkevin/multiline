{{--
  *
  *  @author: kevinuy@ninepinetech.com
  *
  *  Note:
  * 	Code flexibility within this file is only about 5-15%. Comments are provided for future references.
  *
  * 	Best Regards!
  *
--}}
@extends('layouts.app')

@section('styles')
	<style type="text/css" id="iframe-style">
		.wrapper{
			display: flex;
			align-items: stretch;
		}

		#sidebar{
			min-width: 300px;
			max-width: 300px;
			min-height: 100vh;
		}

		#sidebar.active{
			margin-left: -300px;
		}

		a[data-toggle="collapse"]{
			position: relative;
		}

		.dropdown-toggle::after{
			display: block;
			position: absolute;
			top: 50%;
			right: 20px;
			transform: translateY(-50%);
		}

		@media (max-width: 768px){
			#sidebar{
				margin-left: -300px;
			}
			#sidebar.active{
				margin-left: 0;
			}
		}

		body{
			background: #fafafa;
		}

		p{
			font-family: 'Poppins', sans-serif;
			font-size: 1em;
			font-weight: 300;
			line-height: 1.7em;
			color: #999;
		}

		main.main-content a,
		main.main-content a:hover,
		main.main-content a:focus{
			color: inherit;
			text-decoration: none;
			transition: all 0.3s;
		}

		#content{
			padding: 20px;
			width: 100%;
		}

		#sidebar{
			background: #3e3e3e;
			color: #fff;
			transition: all 0.3s;
		}

		#sidebar .sidebar-header{
			padding: 20px;
			background: #2e2e2e;
		}

		#sidebar ul.components{
			padding: 20px 0;
			border-bottom: 1px solid #3e3e3e;
		}

		#sidebar ul p{
			color: #fff;
			padding: 10px;
		}

		#sidebar ul li a{
			padding: 10px;
			font-size: 1.1em;
			display: block;
		}
		#sidebar ul li a:hover{
			color: #3e3e3e;
			background: #fff;
		}

		#sidebar ul li.active > a,
		#sidebar a[aria-expanded="true"]{
			color: #fff;
			background: #2e2e2e;
		}

		ul ul a{
			font-size: 0.75em !important;
			padding-left: 30px !important;
			background: #2e2e2e;
		}

		.pull-right{
			float: right;
		}

		.bet-table{
			margin-bottom: 0;
		}
			.bet-table tr[class$="-start"] td{
				border-top: 1px solid #dee2e6;
			}
				.bet-table th,
				.bet-table td{
					height: 25px;
					min-height: 25px;
					padding: 0.25rem;
					text-align: center;
					vertical-align: middle;

					border: none;
					font-size: 12px;

					transition: 1s ease;
						-webkit-transition: 1s ease;
						-moz-transition: 1s ease;
						-ms-transition: 1s ease;
						-o-transition: 1s ease;
				}

		.bet-table td.highlight{
			font-size: 14px;
			font-weight: bold;
		}

		.bet-table td.muted{
			background: #EAEAEA;
		}

		.bet-table td.ping-danger{
			animation-name: ping-danger;
			animation-duration: 3s;
			animation-iteration-count: infinite;
		}

		.bet-table td.ping-success{
			animation-name: ping-success;
			animation-duration: 3s;
			animation-iteration-count: infinite;
		}

		.bet-table td.no-top-border{
			padding-top: 0px;

			border-top: none;
		}

		.btn-add-to-fav,
		.btn-rmv-to-fav{
			color: #444444 !important;
		}
			.btn-add-to-fav:hover,
			.btn-rmv-to-fav:hover{
				color: #ff800f !important;
			}

		.fav{
			color: #ff800f !important;
		}

		.card{
			margin-bottom: 20px;
		}
			.card-body{
				padding: 0;
			}

		@keyframes ping-danger{
			from{
				background: rgba(227, 52, 47, 0.2);
				box-shadow: inset 0px -3px 0px 0px rgba(227, 52, 47, 1);
				font-weight: 700;
			} to{
				background: #FFFFFF;
				box-shadow: none;
				font-weight: 400;
			}
		}

		@keyframes ping-success{
			from{
				background: rgba(56, 193, 114, 0.2);
				box-shadow: inset 0px 3px 0px 0px rgba(56, 193, 114, 1);
				font-weight: 700;
			} to{
				background: #FFFFFF;
				box-shadow: none;
				font-weight: 400;
			}
		}
	</style>
@endsection

@section('content')
	<div class="container-fluid" style="margin-top: 67px; padding: 0;" ng-app="multiline">
		<div class="wrapper">
			<nav id="sidebar">
				<div class="sidebar-header">
					<span class="">Maximum Bet</span>
					<span class="pull-right">฿ {{ number_format(rand(500000, 1000000), 2) }}</span>
					<br />
					<span class="">Available Balance</span>
					<span class="pull-right">฿ {{ number_format(rand(5000, 10000), 2) }}</span>
					<br />
					<span class="">Open Bets</span>
					<span class="pull-right">฿ {{ number_format(rand(50, 100), 2) }}</span>
				</div>

				<ul class="list-unstyled components" ng-controller="leagueList">
					<li class="">
						<a href="#page_in-play" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">In-Running</a>

						<ul class="collapse show list-unstyled" id="page_in-play">
							@if(!empty($inplay[0]))
								@foreach($inplay AS $row)
									<li><a href="#" class="sidebar-link-league" ng-click="getLeagueData($event)" data-lid="{{ explode('-', $row[0]->leg_league_id)[1] }}" data-gs="in-play" data-rid="{{ $row[0]->leagueinfo_id }}">{{ $row[0]->leaguename }}</a></li>
								@endforeach
							@else
								<li><a href="#"><em>No games found.</em></a></li>
							@endif
						</ul>
					</li>
					<li class="">
						<a href="#page_today" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Today</a>

						<ul class="collapse show list-unstyled" id="page_today">
							@if(!empty($today[0]))
								@foreach($today AS $row)
									<li><a href="#" class="sidebar-link-league" ng-click="getLeagueData($event)" data-lid="{{ explode('-', $row[0]->leg_league_id)[1] }}" data-gs="today" data-rid="{{ $row[0]->leagueinfo_id }}">{{ $row[0]->leaguename }}</a></li>
								@endforeach
							@else
								<li><a href="#"><em>No games found.</em></a></li>
							@endif
						</ul>
					</li>
					<li class="">
						<a href="#page_early" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">Early</a>

						<ul class="collapse show list-unstyled" id="page_early">
							@if(!empty($early[0]))
								@foreach($early AS $row)
									<li><a href="#" class="sidebar-link-league" ng-click="getLeagueData($event)" data-lid="{{ explode('-', $row[0]->leg_league_id)[1] }}" data-gs="early" data-rid="{{ $row[0]->leagueinfo_id }}">{{ $row[0]->leaguename }}</a></li>
								@endforeach
							@else
								<li><a href="#"><em>No games found.</em></a></li>
							@endif
						</ul>
					</li>
				</ul>
			</nav>

			<div id="content">
				<section id="content_watchlist">
					<h4>
						<i class="far fa-eye"></i> &nbsp; Watchlist
					</h4>

					@if(!empty($watchlist))
						@foreach($watchlist AS $key => $row)
							<div class="card league_{{ $row['ggid'] }}" data-lid="card-league-{{ $row['lid'] }}">
								<div class="card-body">
									<table class="table bet-table">
										<tbody>
											<tr class="league-header">
												<th class="text-left">
													<a href="#" class="btn btn-xsm btn-circle btn-orange btn-league-collapse" data-target=".collapse-bet-{{ $row['lid'] }}"><i class="fas fa-angle-down"></i></a>
													<span class="btn btn-sm btn-link match-header">{{ $key }}</span>
												</th>
												<th width="120">FT 1X2</th>
												<th width="120">FT Handicap</th>
												<th width="120">FT O/U</th>
												<th width="120">FT O/E</th>
												<th width="120">1H 1X2</th>
												<th width="120">1H Handicap</th>
												<th width="120">1H O/U</th>
											</tr>
											@foreach($row AS $data)
												@if(is_array($data))
													@foreach($data AS $tr)
														<tr class="tr-rand-pick collapse show collapse-bet-{{ $row['lid'] }} linfo-{{ $tr->group_game_id }}{{ ($tr['schedule'] != "") && ($tr['team'] != "Draw") ? "-start" : "" }}" data-ggid="{{ $tr->group_game_id }}">
															<td class="highlight text-left">
																@if($tr['schedule'] != "")
																	<a class="btn btn-sm btn-rmv-to-fav fav" data-lid="league-{{ $row['lid'] }}" data-ggid="{{ $tr->group_game_id }}" data-gs="{{ $tr->game_schedule }}">
																		<i class="fas fa-star"></i>
																	</a>
																@else
																	&nbsp; &nbsp;
																@endif

																<span class="text-{{ $tr['schedule'] != "" ? "success" : "danger" }}">
																	@if($tr['schedule'] != "")
																		H
																	@elseif($tr['team'] == "Draw")
																		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
																	@else
																		&nbsp; &nbsp; &nbsp; A
																	@endif
																</span>
																&nbsp;
																<span class="">
																	{{ str_replace(array('*REDCARD_H*', '*REDCARD_C*'), '', $tr['team']) }}

																	@if(strpos($tr['team'], 'REDCARD'))
																		&nbsp; <span class="text-danger"><i class="fas fa-square"></i></span>
																	@endif
																</span>
																<span class="player-score pull-right"></span>
															</td>
															<td class="td-rand-pick" data-var="soc_col_1">{{ $tr->ft_1x2 }}</td>
															<td class="muted" data-var="soc_col_handicap_0">{{ $tr->ft_handicap }}</td>
															<td class="td-rand-pick" data-var="soc_col_handicap_1">{{ $tr->ft_ou }}</td>
															<td class="td-rand-pick" data-var="soc_col_handicap_2">{{ $tr->ft_oe }}</td>
															<td class="muted" data-var="soc_col_handicap_ou_0">{{ $tr->fh_1x2 }}</td>
															<td class="td-rand-pick" data-var="soc_col_handicap_ou_1">{{ $tr->fh_handicap }}</td>
															<td class="td-rand-pick" data-var="soc_col_handicap_ou_2">{{ $tr->fh_ou }}</td>
														</tr>
													@endforeach
												@endif
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						@endforeach
					@else
						<p id="empty-watchlist" align="center">
							<em>No games added to your Watchlist.</em>
						</p>
					@endif
				</section>

				<hr />

				<section id="content_in-play"></section>
				<section id="content_today"></section>
				<section id="content_early"></section>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		app.controller('leagueList', function($scope, $http, $compile){
			$scope.getLeagueData = function($item){
				{{--
				  *
				  *  Variable declaration of the selected `sidebar league` list
				  *
				  *  Legend:
				  *  	$lid 		Game `leg_league_id` from `leagueinfo` table
				  * 	$gs 		Game Schedule
				  						['in-play', 'today', 'early']
				  * 	$rid 		Game `leagueinfo_id` from `leagueinfo` table
				  *
				--}}
				var $this = $item.currentTarget;
				var $lid = $item.currentTarget.getAttribute("data-lid");
				var $gs = $item.currentTarget.getAttribute("data-gs");
				var $rid = $item.currentTarget.getAttribute("data-rid");

				{{--
				  *
				  *  Fetch scraped data from `soccer_scraped` table
				  *
				--}}
				$http({
					url: "{{ route('trade.leaguedata') }}",
					method: "POST",
					data: {
						lid: $lid,
						gs: $gs,
					},
				}).then(function($response){
					var $loop = ``;
					var $arrgrp = [];

					for(var $i = 0; $i < $response.data.linfo.length; $i++){
						var $_fav = ``, $_set = ``;

						if($.inArray($response.data.linfo[$i].group_game_id, $arrgrp) < 0){
							$_fav = `<a class="btn btn-sm btn-add-to-fav" data-lid="league-` + $rid + `" data-ggid="` + $response.data.linfo[$i].group_game_id + `" data-gs="` + $response.data.gs + `"><i class="far fa-star"></i></a>`;
							$_set = `linfo-` + $response.data.linfo[$i].group_game_id + `-start`;
							$arrgrp.push($response.data.linfo[$i].group_game_id);
						} else{
							$_fav = `<a class="">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</a>`;
						}

						$loop += `
							<tr class="tr-rand-pick collapse show collapse-bet-` + $rid + ` ` + $_set + `" data-ggid="` + $response.data.linfo[$i].group_game_id + `">
								<td class="highlight text-left">
									` + $_fav + `
									<span class="text-` + ($response.data.linfo[$i].schedule != "" ? "success" : "danger") + `">
										` + ($response.data.linfo[$i].schedule != "" ? "H" : ($response.data.linfo[$i].team == "Draw" ? "&nbsp; &nbsp;" : "A")) + `
									</span>
									&nbsp;
									<span class="">` + $response.data.linfo[$i].team.replace('*REDCARD_H*', '&nbsp; <span class="text-danger"><i class="fas fa-square"></i></span>').replace('*REDCARD_C*', '&nbsp; <span class="text-danger"><i class="fas fa-square"></i></span>') + `</span>
									<span class="player-score pull-right"></span>
								</td>
								<td class="td-rand-pick" data-var="soc_col_1">` + $response.data.linfo[$i].ft_1x2 + `</td>
								<td class="muted" data-var="soc_col_handicap_0">` + $response.data.linfo[$i].ft_handicap + `</td>
								<td class="td-rand-pick" data-var="soc_col_handicap_1">` + $response.data.linfo[$i].ft_ou + `</td>
								<td class="td-rand-pick" data-var="soc_col_handicap_2">` + $response.data.linfo[$i].ft_oe + `</td>
								<td class="muted" data-var="soc_col_handicap_ou_0">` + $response.data.linfo[$i].fh_1x2 + `</td>
								<td class="td-rand-pick" data-var="soc_col_handicap_ou_1">` + $response.data.linfo[$i].fh_handicap + `</td>
								<td class="td-rand-pick" data-var="soc_col_handicap_ou_2">` + $response.data.linfo[$i].fh_ou + `</td>
							</tr>`;
					}

					var $append = `
						<div class="card league_` + $response.data.lid + `" data-lid="card-league-` + $rid + `">
							<div class="card-body">
								<table class="table bet-table">
									<tbody>
										<tr class="league-header">
											<th class="text-left">
												<a href="#" class="btn btn-xsm btn-circle btn-orange btn-league-collapse" data-target=".collapse-bet-` + $rid + `"><i class="fas fa-angle-down"></i></a>
												<span class="btn btn-sm btn-link match-header">` + $response.data.leaguetitle + `</span>
											</th>
											<th width="120">FT 1X2</th>
											<th width="120">FT Handicap</th>
											<th width="120">FT O/U</th>
											<th width="120">FT O/E</th>
											<th width="120">1H 1X2</th>
											<th width="120">1H Handicap</th>
											<th width="120">1H O/U</th>
										</tr>
										` + $loop + `
									</tbody>
								</table>
							</div>
						</div>`;

					if(angular.element($this).hasClass('active')){
						angular.element($('.card.league_' + $response.data.lid)).remove();
						angular.element($this).removeClass('active');
					} else{
						angular.element($('section#content_' + $response.data.gs)).append($append);
						angular.element($this).addClass('active');
					}
				});
			};

			$(function(){
				{{--
				  *
				  *  `Add to Watchlist` functionality
				  *
				  *  Variable Legend:
				  *  	$lid 		`leagueinfo_id` from table `leagueinfo`
				  *  	$gs 		Game Schedule
				  * 	  				['in-play', 'today', 'early']
				  *  	$ggid 		`group_game_id` from table `soccer_scraped` determining the league's groupings per schedule
				  *
				--}}
				$(document).on('click', '.btn-add-to-fav', function(){
					var $this = $(this);
					var $lid = $this.data('lid').split('-')[1];
					var $gs = $this.data('gs');
					var $ggid = $this.data('ggid');
					var $data = {
						'_token' : '{{ csrf_token() }}',
						'lid' : $lid,
						'gs' : $gs,
						'ggid' : $ggid,
					};

					$.post('{{ route('trade.watchlist.save') }}', $data, function($response){
						var $response = $.parseJSON($response);
						var $tofav = ``, $header = ``;
						var $ctr = 0;

						{{--
						  *
						  *  Gather each row of selected tournament and append to `Watchlist` section
						  *
						  *  Note:
						  * 	#  Must append `.card` with complete attributes if league doesn't exist on the watchlist;
						  * 	#  League watchlist must have no duplicate leagues;
						  * 	#  Must remove `.card` if there are no more leagues under it;
						  *
						--}}
						$(document).find('tr[data-ggid="' + $ggid + '"]').each(function(){
							var $start = $ctr == 0 ? "linfo-" + $ggid + "-start" : "linfo-" + $ggid;

							$tofav += `<tr class="tr-rand-pick collapse show collapse-bet-` + $lid + ` ` + $start + `" data-ggid="` + $ggid + `">`;
							$tofav += $(this).html();
							$tofav += `</tr>`;
							$ctr++;
							$(this).remove();

							var $ifEmpty = $('#content_' + $gs).find('.card[data-lid="card-league-' + $lid + '"] tr.collapse-bet-' + $lid).length;

							if($ifEmpty == 0){
								$('.sidebar-link-league[data-rid="' + $lid + '"]').closest('li').remove();
								$('#content_' + $gs + ' .card[data-lid="card-league-' + $lid + '"]').remove();
							}
						});

						{{--
						  *
						  *  Check first if the League already exists under Watchlist section.
						  *
						--}}
						if($(document).find('#content_watchlist .card[data-lid="card-league-' + $lid + '"]').length){
							$header = $tofav;
							$('#content_watchlist').find('table').append($header);
						} else{
							$header = `
								<div class="card league_` + $ggid + `" data-lid="card-league-` + $lid + `">
									<div class="card-body">
										<table class="table bet-table">
											<tbody>
												<tr class="league-header">
													<th class="text-left">
														<a href="#" class="btn btn-xsm btn-circle btn-orange btn-league-collapse" data-target=".collapse-bet-` + $lid + `"><i class="fas fa-angle-down"></i></a>
														<span class="btn btn-sm btn-link match-header">` + $response.rowdata.leaguename + `</span>
													</th>
													<th width="120">FT 1X2</th>
													<th width="120">FT Handicap</th>
													<th width="120">FT O/U</th>
													<th width="120">FT O/E</th>
													<th width="120">1H 1X2</th>
													<th width="120">1H Handicap</th>
													<th width="120">1H O/U</th>
												</tr>
												` + $tofav + `
											</tbody>
										</table>
									</div>
								</div>`;

							$('#content_watchlist').append($header);
						}

						$('#empty-watchlist').hide();
						$('#content_watchlist').find('.btn-add-to-fav[data-lid="league-' + $lid + '"] > i').prop('class', 'fas fa-star');
						$('#content_watchlist').find('.btn-add-to-fav[data-lid="league-' + $lid + '"]').prop('class', 'btn btn-sm btn-rmv-to-fav');
						$('#content_watchlist').find('.btn-rmv-to-fav[data-lid="league-' + $lid + '"]').addClass('fav');
					});
				});

				{{--
				  *
				  *  `Remove to Watchlist` functionality
				  *
				--}}
				$(document).on('click', '.btn-rmv-to-fav', function(){
					var $this = $(this);
					var $lid = $this.data('lid').split('-')[1];
					var $gs = $this.data('gs');
					var $ggid = $this.data('ggid');
					var $data = {
						'_token' : '{{ csrf_token() }}',
						'lid' : $lid,
						'gs' : $gs,
						'ggid' : $ggid,
					};

					$.post('{{ route('trade.watchlist.delete') }}', $data, function($response){
						var $response = $.parseJSON($response);
						var $tofav = ``, $header = ``;
						var $ctr = 0;

						{{--
						  *
						  *  Gather each row of selected tournament and append to `Watchlist` section
						  *
						  *  Note:
						  * 	#  Must append `.card` with complete attributes if league doesn't exist on the watchlist;
						  * 	#  League watchlist must have no duplicate leagues;
						  * 	#  Must remove `.card` if there are no more leagues under it;
						  *
						--}}
						$(document).find('tr[data-ggid="' + $ggid + '"]').each(function(){
							var $start = $ctr == 0 ? "linfo-" + $ggid + "-start" : "linfo-" + $ggid;

							$tofav += `<tr class="tr-rand-pick collapse show collapse-bet-` + $response.rowdata.leagueinfo_id + ` ` + $start + `" data-ggid="` + $ggid + `">`;
							$tofav += $(this).html();
							$tofav += `</tr>`;
							$ctr++;
							$(this).remove();

							var $ifEmpty = $('#content_watchlist').find('.card[data-lid="card-league-' + $lid + '"] tr.collapse-bet-' + $lid).length;

							if($(document).find('#sidebar #page_' + $response.rowdata.game_schedule + ' a.sidebar-link-league[data-rid="' + $response.rowdata.leagueinfo_id + '"]').length == 0){
								var $prepend = `<li><a href="#" class="sidebar-link-league active" ng-click="getLeagueData($event)" data-lid="` + $response.rowdata.group_game_id + `" data-gs="` + $response.rowdata.game_schedule + `" data-rid="` + $response.rowdata.leagueinfo_id + `">` + $response.rowdata.leaguename + `</a></li>`;
								$prepend = $compile($prepend)($scope);

								if($('#sidebar #page_' + $response.rowdata.game_schedule + ' a.sidebar-link-league[data-rid="' + (parseInt($response.rowdata.leagueinfo_id) + 1) + '"]').length == 0)
									$('#sidebar #page_' + $response.rowdata.game_schedule).append($prepend);
								else
									$($prepend).insertBefore($('#sidebar #page_' + $response.rowdata.game_schedule + ' a.sidebar-link-league[data-rid="' + (parseInt($response.rowdata.leagueinfo_id) + 1) + '"]').parent());
							}

							if($ifEmpty == 0){
								$('#content_watchlist .card[data-lid="card-league-' + $lid + '"]').remove();
							}
						});

						{{--
						  *
						  *  Check first if the League already exists under Watchlist section.
						  *
						--}}
						if($(document).find('#content_' + $gs + ' .card[data-lid="card-league-' + $lid + '"]').length > 0){
							$header = $tofav;
							$('#content_' + $gs).find('table').append($header);
						} else{
							$header = `
								<div class="card league_` + $ggid + `" data-lid="card-league-` + $lid + `">
									<div class="card-body">
										<table class="table bet-table">
											<tbody>
												<tr class="league-header">
													<th class="text-left">
														<a href="#" class="btn btn-xsm btn-circle btn-orange btn-league-collapse" data-target=".collapse-bet-` + $lid + `"><i class="fas fa-angle-down"></i></a>
														<span class="btn btn-sm btn-link match-header">` + $response.rowdata.leaguename + `</span>
													</th>
													<th width="120">FT 1X2</th>
													<th width="120">FT Handicap</th>
													<th width="120">FT O/U</th>
													<th width="120">FT O/E</th>
													<th width="120">1H 1X2</th>
													<th width="120">1H Handicap</th>
													<th width="120">1H O/U</th>
												</tr>
												` + $tofav + `
											</tbody>
										</table>
									</div>
								</div>`;

							$('#content_' + $gs).append($header);
						}

						if($('#content_watchlist').find('.card').length == 0)
							$('#empty-watchlist').show();

						$('#empty-' + $response.rowdata.game_schedule).hide();
						$('#content_' + $response.rowdata.game_schedule).find('.btn-rmv-to-fav[data-lid="league-' + $lid + '"] > i').prop('class', 'far fa-star');
						$('#content_' + $response.rowdata.game_schedule).find('.btn-rmv-to-fav[data-lid="league-' + $lid + '"]').prop('class', 'btn btn-sm btn-add-to-fav');
						$('#content_' + $response.rowdata.game_schedule).find('.btn-add-to-fav[data-lid="league-' + $lid + '"]').removeClass('fav');
					});
				});

				setInterval(function(){
					var $tr = randomInt(parseInt($(document).find('.tr-rand-pick').length));
					var $td = randomInt(parseInt($(document).find('.tr-rand-pick').eq(0).find('.td-rand-pick').length));
					var $ping = [
						'danger',
						'success'
					];
					var $this = $(document).find('.tr-rand-pick').eq($tr).find('.td-rand-pick').eq($td);

					if(!$this.hasClass(function(index, className){
						return(className.match(/(^|\s)ping-\S+/g) || []).join(' ');
					})){
						$this.addClass('ping-' + $ping[randomPing($ping)]);

						setTimeout(function(){
							$this.removeClass(function(index, className){
								return(className.match(/(^|\s)ping-\S+/g) || []).join(' ');
							});
						}, 2000);
					}
				}, 2000);
			});

			function randomInt($max){
				return Math.floor(Math.random() * Math.floor($max));
			}

			function randomPing($ping){
				return Math.floor(Math.random() * $ping.length);
			}
		});
	</script>
@endsection