<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top" id="navbarMain">
	<div class="container-fluid">
		<a class="navbar-brand" href="/"><img src="{{ asset("images/logo.png") }}" ></a>

		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="navbarResponsive">
			<span class="navbar-toggler-icon"> </span>
		</button>

		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="#aboutMultiSection">About</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#platformSection">Platform</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#partnersMultiSection">Partners</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contactMultiSection">Contact</a>
				</li>
				<li class="nav-item">
					<a id="btn-login" href="{{ route('web.login.form') }}" class="nav-link btn btn-outline-secondary" href="#">Login</a>
				</li>

			</ul>
		</div>
	</div>
</nav>