<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{{ asset('css/fontawesome-main.css') }}">

	<!-- AngularJS -->
	<script type="text/javascript" src="{{ asset('js/angular.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/angular-animate.js') }}"></script>
	<script type="text/javascript">
		var app = angular.module('multiline', [], function($interpolateProvider){
			$interpolateProvider.startSymbol('{~');
			$interpolateProvider.endSymbol('~}');
		});
	</script>

	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('css/custom.css') }}" rel="stylesheet">

	@yield('styles')
</head>
<body>
	<div id="app">
		<nav class="navbar navbar-expand-md fixed-top navbar-light navbar-laravel" id="main-navbar">
			<div class="container-fluid" style="padding: 0;">
				<a class="navbar-brand" href="#">
					<img src="{{ asset('images/logo_alt.png') }}">
				</a>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#top-nav" aria-controls="top-nav" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="top-nav">
					@guest
					@else
						<ul class="navbar-nav mr-auto">
							<li class="nav-item"><a href="{{ route('trade') }}" class="nav-link {{ \Request::segment(1) == "trade" ? "active" : "" }}">Trade</a></li>
							<li class="nav-item dropdown">
								<a href="#" class="nav-link {{ \Request::segment(1) == "history" ? "active" : "" }}" id="navbarHistory" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									History &nbsp; <i class="fas fa-angle-down"></i>
								</a>

								<div class="dropdown-menu" aria-labelledby="navbarHistory">
									<a href="#" class="dropdown-item">Orders</a>
									<a href="#" class="dropdown-item">Bets</a>
									<a href="#" class="dropdown-item">Accounting</a>
								</div>
							</li>
							<li class="nav-item"><a href="#" class="nav-link {{ \Request::segment(1) == "openorders" ? "active" : "" }}">Open Orders</a></li>
							<li class="nav-item dropdown">
								<a href="#" class="nav-link {{ \Request::segment(1) == "settings" ? "active" : "" }}" id="navbarSettings" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									Settings &nbsp; <i class="fas fa-angle-down"></i>
								</a>

								<div class="dropdown-menu" aria-labelledby="navbarSettings">
									<a href="{{ route('settings.trade') }}" class="dropdown-item">Trade</a>
									<a href="#" class="dropdown-item">Bookies</a>
									<a href="#" class="dropdown-item">Account</a>
								</div>
							</li>
						</ul>
					@endguest

					<ul class="navbar-nav ml-auto">
						@guest
							<li class="nav-item">
								<a class="nav-link" href="{{ route('web.login.form') }}">{{ __('Login') }}</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="{{ route('web.register.form') }}">{{ __('Register') }}</a>
							</li>
						@else
							<li class="nav-item"><a href="#" class="nav-link">Feedback</a></li>
							<li class="nav-item"><a href="#" class="nav-link">Terms</a></li>
							<li class="nav-item dropdown">
								<a href="#" class="nav-link" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
									{{ Auth::user()->username }} &nbsp; <i class="fas fa-angle-down"></i>
								</a>

								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="{{ route('web.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
										{{ __('Logout') }}
									</a>

									<form id="logout-form" action="{{ route('web.logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
								</div>
							</li>
						@endguest
					</ul>
				</div>
			</div>
		</nav>

		<main class="main-content">
			@yield('content')
		</main>
	</div>

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/popper.js') }}"></script>

	<!-- JQuery UI -->
	<link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
	<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>

	<!-- Select2 JS -->
	<link rel="stylesheet" href="{{ asset('css/select2.css') }}">
	<link rel="stylesheet" href="{{ asset('css/select2-bootstrap.css') }}">
	<script type="text/javascript" src="{{ asset('js/select2.js') }}"></script>

	@yield('scripts')

</body>
</html>