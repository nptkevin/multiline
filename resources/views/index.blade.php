<!DOCTYPE html>
<html lang="en">
<head>
	@include("layouts.header-script")
	<style>
		#navbarMain, #headerSection{
			background: #1b1b1b !important;
		}

		#headerSectionTitle1{
			margin-bottom: 20px;

			color: #e0610c !important;
			font-size: 3rem;
			word-spacing: 10px;
		}

		#headerSectionTitle2{
			margin-bottom: 40px;

			font-size: 3rem;
			word-spacing: 10px;
		}

		.btn-xlg{
			padding: 5px 100px;

			border-radius: 20px;
			font-family: bebas !important;
			font-size: 2.75rem;
			word-spacing: 10px;
		}

		.btn-orange{
			background: #e0610c;
			color: #FFFFFF;
		}
			.btn-orange:hover{
				color: #FFFFFF;
			}

		#aboutMultiSection,
		#partnersMultiSection{
			background: #666565 !important;
		}
			#aboutMultiSection .headerSectionContentTitle,
			#partnersMultiSection .headerSectionContentTitle{
				margin-bottom: 50px;

				color: #1b1b1b !important;
				font-size: 4rem;
			}
			#aboutMultiSection .headerSectionContentDescription,
			#aboutMultiSection .thumb-description,
			#partnersMultiSection .headerSectionContentDescription{
				color: #FFFFFF !important;
			}

			#platformSection .headerSectionContentTitle{
				margin-bottom: 25px;

				color: #e0610c !important;
				font-size: 2.5rem;
			}
		#contactMultiSection{
			background: #e0610c !important;
		}

		.orange-controls{
			background: #fea468 !important;
			color: #FFFFFF !important;
		}

		select.orange-controls option{
			color: #FFFFFF !important;
		}

		::-webkit-input-placeholder { /* Chrome/Opera/Safari */
			color: #ffffff !important;
		}

		::-moz-placeholder { /* Firefox 19+ */
			color: #ffffff !important;;
		}

		:-ms-input-placeholder { /* IE 10+ */
			color: #ffffff !important;;
		}

		:-moz-placeholder { /* Firefox 18- */
			color: #ffffff !important;;
		}
	</style>
</head>
<body>
@include("layouts.header")
<!-- header section-->
<div id="headerSection" class="container-fluid h-100">
	<div class="row h-100 justify-content-center align-items-center">
		<div class="col-sm-4">
			<img src="{{ asset('images/main.png') }}">
		</div>

		<div class="col-sm-7 text-right" id="">
			<p class="text-right" id="headerSectionTitle1">Best Sports Betting Platform In Asia</p>
			<p class="text-right" id="headerSectionTitle2">For Professional Betting</p>
			<a href="#" class="btn btn-xlg btn-orange">JOIN NOW</a>
		</div>

		<div class="col-sm-1">&nbsp;</div>
	</div>
</div>
<!-- About Multiline Section-->
<div id="aboutMultiSection" class="container-fluid h-100">
	<div class="row h-100 justify-content-center align-items-center">
		<div class="text-center">
			<h1 class="text-center headerSectionContentTitle " id="">ABOUT MULTILINE</h1>
			<p class="text-center headerSectionContentDescription" id="">Multiline is the most advanced sports trading
				platform, designed exclusively for professional traders to give them the best odds<br>and highest stakes
				across multiple bookmakers and exchanges.
			</p>
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-3 col-lg-2 ">
					<img src="{{ asset("images/about-1.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Simultaneous<br>Execution</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2">
					<img src="{{ asset("images/about-2.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Global Liquidity</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2 ">
					<img src="{{ asset("images/about-3.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Comprehensive<br>Coverage</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2">
					<img src="{{ asset("images/about-4.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Speed</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2">
					<img src="{{ asset("images/about-5.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Completely<br>Impartial</p>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-3 col-lg-2 ">
					<img src="{{ asset("images/about-6.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">First to Market</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2 ">
					<img src="{{ asset("images/about-8.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Real-time Support</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2">
					<img src="{{ asset("images/about-9.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Multi-Channel</p>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-2">
					<img src="{{ asset("images/about-10.png") }}" class="img-fluid  rounded-circle">
					<p class="thumb-description">Currency & Odds<br>Standardization</p>
				</div>

			</div>
		</div>
	</div>
</div>
<!-- The  multipline  Sports  Trading  Platform Section-->
<div id="platformSection" class="container h-100">
	<div class="col-12 h-100 justify-content-center container align-items-center">
		<div class="row text-justify-content-left ">
				<div class="col-8">
					<p class="headerSectionContentTitle" id="">The multipline Sports Trading Platform</p>
					<p class="headerSectionContentDescription" id="">The Multiline betting platform is an easy to use,
						multi-channel interface showing real time<br>
						updated best odds, so that users can place aggregated orders at the highest liquidity in<br>
						milliseconds. This solves any problems associated with poor liquidity, price movements<br>
						and manual execution errors.
					</p>
					<div class="row justify-content-left align-items-center">
						<div class="col-6">
							<img src="{{ asset("images/plat-1.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Global Liquidity</p>
						</div>
						<div class="col-6">
							<img src="{{ asset("images/plat-2.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Odds formats (Decimal,<br>
								Hong Kong, American)</p>
						</div>
					</div>
					<div class="row justify-content-left align-items-center">
						<div class="col-6">
							<img src="{{ asset("images/plat-3.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Working Orders (auto-placement<br>
								once order prices match)</p>
						</div>
						<div class="col-6">
							<img src="{{ asset("images/plat-4.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Multiple currencies</p>
						</div>
					</div>
					<div class="row justify-content-left align-items-center">
						<div class="col-6">
							<img src="{{ asset("images/plat-5.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Favourite events and
								competition filters</p>
						</div>
						<div class="col-6">
							<img src="{{ asset("images/plat-6.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Comprehensive accounting<br>
								software & full audit history</p>
						</div>
					</div>
					<div class="row justify-content-left align-items-center">
						<div class="col-6">
							<img src="{{ asset("images/plat-7.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">Smart search bar<br>
								for faster browsing</p>
						</div>
						<div class="col-6">
							<img src="{{ asset("images/plat-8.png") }}"
								 class="img-fluid  rounded-circle img-thumb2" align="left">
							<p class="thumb-description2">White label customisation</p>
						</div>
					</div>
				</div>
				<div class="col-4">
					<img src="{{asset("images/mid.png")}}" class="" style="height: 80%; width: auto; margin-left: calc(50% - 35vh);">
				</div>

		</div>
	</div>
</div>
</div>
<!-- Our Reputable Partners-->
<div id="partnersMultiSection" class="container-fluid h-100">
	<div class="row h-100 justify-content-center ">
		<div class="container">
			<h1 class="text-center headerSectionContentTitle " id="">Our Reputable Partners</h1>
			<br>
			<p class="headerSectionContentDescription" id="">
				Many of the world’s largest Bookmakers, in particular the Asia-based operators, partner with authorised
				Agents to provide account<br>
				and settlement services to their clients. The Agent’s role is to establish betting accounts, manage
				client funds (e.g. receive deposits,<br>
				send withdrawals, manage client credit and settle outstanding balances between the client and the
				bookmaker) and provide<br>
				customer support.<br>
				<br>
				At Multiline we focus entirely on providing a world-class execution platform. We don’t<br>
				provide betting accounts and we don’t accept client deposits. However, through our 10+ years of
				experience, we have established lasting relationships with a handful of<br>
				highly reputable Agents.<br>
				<br>
				If you require betting accounts, please let us know: we’d be happy to talk you through how the system
				works in more detail and<br>
				put you in contact with an Agent that best suits your requirements.
			</p>
		</div>
	</div>
</div>
<!-- contact us-->
<div id="contactMultiSection" class="container-fluid h-100">
	<div class="row h-100 justify-content-center ">
		<div class="container">
			<p class="text-center headerSectionContentTitle  white-text" id="">Contact Us</p>
			<br>
			<p class="text-center headerSectionContentDescription white-text" id="">Drop us a line. We usually respond
				within 24 hours of receiving your email.</p>
			<div class="form-group">
				<form>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<input class=" form-control orange-controls" id="name" name="name" placeholder="Name">
						</div>
						<div class="col-sm-12 col-md-6">
							<input class="form-control orange-controls" id="email" type="email" name="email"
								   placeholder="Email">
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<input class="form-control orange-controls" id="how" name="how"
								   placeholder="How did you hear about us?">
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<input class="form-control orange-controls" id="which" name="which"
								   placeholder="Which bookmakers do you currently bet with?">
						</div>
					</div>
					<div class="row justify-content-center text-center">
						<div class="col-7 justify-content-center">
							<label class="label-gray"> Do you currently access Multiline via an agent or under a
								whitelabel brand?</label><br>
							<div class="d-flex justify-content-center">
								<select class="col-6 form-control orange-controls" id="which" name="which"
										placeholder="Which bookmakers do you currently bet with?">
									<option label="Name" value=""></option>
								</select>
							</div>

						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-7  text-center">
							<label class="label-gray"> Please share your estimated monthly turnover/</label><br>
							<div class="d-flex justify-content-center">
								<select class="col-6 form-control orange-controls" id="share" name="share"
										placeholder="Which bookmakers do you currently bet with?">
									<option label="$0-$99,9999" value=""></option>
								</select>
							</div>
						</div>
					</div>
					<div class="row  justify-content-center">
						<div class="col-sm-12 col-md-3">

							<button id="" class="btn btn-dark btn-block">SUBMIT</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>

@include("layouts.footer")

</body>

</html>
