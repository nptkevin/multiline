@extends('CRM.layouts.dashboard')

@section('styles')
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <style>
    	.btn-dtactions{
    		margin: 0px 15px;
    	}

		.dataTables_wrapper .dataTables_paginate .paginate_button{
			margin: 0px !important;
			padding: 0px !important;
		}
			.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
				background: none !important;
				border: 1px solid #EEEEEE !important;
			}

		.modal-loader{
			padding: 30px 0px;

			color: #d2d2d2;
			font-size: 2rem;
			text-align: center;
		}

		#inquiry-table td:last-child{
			width: 25%;

			text-align: center;
		}

		#message{
			padding: 10px;

			background: #e8e8e8;
			border: 1px solid #444;
			border-radius: 5px;
			color: #444;
			font-family: monospace;
			font-size: 1.25rem;
		}

		#subject{
			font-size: 1.5rem;
		}

		.text-default{
			color: #b3b3b3;
		}
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact Inquiries</li>
    </ol>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">All Inquiries</h3>
				</div>

				<div class="box-body">
					<table id="inquiry-table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Date</th>
								<th><i class="ion ion-search"></i> &nbsp; Type</th>
								<th><i class="ion ion-search"></i> &nbsp; Sender</th>
								<th><i class="ion ion-search"></i> &nbsp; Subject</th>
								<th><i class="ion ion-search"></i> &nbsp; Message</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-inq">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Contact Inquiry Information</div>

				<div class="modal-body" style="display: contents;">
					<div class="modal-loader">
						<i class="ion ion-load-c fa-spin fa-5x fa-fw"></i>
						<br /><br />
						Fetching data
					</div>

					<div class="col-md-12 hidden" id="div-inq" style="padding: 20px; background: #FFF;">
						<span id="subject"></span>
						<span class="pull-right" id="type"></span>
						<br />
						<span class="text-default">
							<i class="ion ion-edit"></i> &nbsp; <span id="username"></span>
							<br />
							<i class="ion ion-calendar"></i> &nbsp; <span id="date"></span> &nbsp; &nbsp;
							<i class="ion ion-ios-clock-outline"></i> &nbsp; <span id="time"></span>
						</span>
						<br /><br />
						<p id="message"></p>
						<br />
						<button type="button" class="btn btn-sm btn-default btn-reply pull-right"><i class="ion ion-reply"></i> &nbsp; Reply</button>
					</div>

					<div class="col-md-12 hidden" id="div-reply" style="padding: 20px; padding-top: 0px; background: #FFF;">
						<hr />

						<form class="form-horizontal" id="frm-reply" action="{{ route('inquiry.crm.reply') }}" method="post">
							{{ csrf_field() }}
							<input type="hidden" name="contact_id" id="inq-id">

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Reply to <code>@<span id="inq-username"></span></code></label>
									<textarea class="form-control" name="message" id="reply-message" rows="5"></textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary btn-reply-submit pull-right" data-loading="{{ trans('loading.please_wait') }}">Submit Reply</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade in" id="modal-reply-history">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">Reply History</div>

				<div class="modal-body" style="display: contents;">
					<div class="modal-loader">
						<i class="ion ion-load-c fa-spin fa-5x fa-fw"></i>
						<br /><br />
						Fetching data
					</div>

					<div class="col-md-12 hidden" id="reply-history" style="padding: 20px; padding-top: 0px; background: #FFF;">
						<div class="list-group"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function(){
			$('#inquiry-table').DataTable({
				"processing": true,
				"serverSide": true,
				"stateSave": true,
				"responsive": true,
				"ajax": {
					"url": "{{ route('inquiry.datatable') }}",
					"error": function($err){
						if($err.status == 401){
							swal('Session Expired!', 'Please login again.', 'error').then($result => {
								window.location.href = '/admin/login';
							});
						}
					}
				},
				columnDefs: [
					{ targets: [0, 5], searchable: false },
					{ targets: [5], orderable: false }
				],
				"columns": [
					{ "data": "created_at" },
					{
						"data": "type",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.type ? 'N/A' : row.type;

							return data;
						}
					},
					{
						"data": "user_id",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.user_id ? 'N/A' : '<a href="{{ url('admin/accounts') }}/' + row.user_id + '#personal">' + row.account.username + '</a>';

							return data;
						}
					},
					{
						"data": "subject",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.subject ? 'N/A' : row.subject;

							return data;
						}
					},
					{
						"data": "message",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.message ? 'N/A' : row.message;

							return data;
						}
					},
					{
						"data": "contact_id",
						"render": function(data, type, row, meta){
							if(type === 'display')
								data = '<a href="#" data-id="' + row.contact_id + '" class="btn-dtactions modal-inq"><i class="ion ion-search"></i> &nbsp; View Message</a>';
								data += '<a href="#" data-id="' + row.contact_id + '" class="btn-dtactions crm-reply"><i class="ion ion-reply"></i> &nbsp; Reply</a>';
								data += '<a href="#" data-id="' + row.contact_id + '" class="btn-dtactions crm-reply-history"><i class="ion ion-ios-list-outline"></i> &nbsp; Reply History</a>';

							return data;
						}
					}
				]
			});

			$(document).on('click', '.modal-inq', function(){
				var $id = $(this).data('id');

        		$('#modal-inq').modal('show').find('.modal-loader').removeClass('hidden');
				$('#div-inq').addClass('hidden').find('span:not([class="text-default"]), p').empty();
				$('#div-reply').addClass('hidden');
				$('#inq-id').val($(this).data('id'));

				$.post("{{ route('inquiry.info') }}", { _token: "{{ csrf_token() }}", id: $id }, function($data, $status){
					if($status == "success"){
        				$('#div-inq').removeClass('hidden');
    					$('.modal-loader').addClass('hidden');
						$('#modal-inq').find('#subject').text($data.info.subject);
						$('#modal-inq').find('#type').text($data.info.type);
						$('#modal-inq').find('#type').prop('class', 'pull-right status status-' + $data.info.status);
						$('#modal-inq').find('#message').text($data.info.message);
						$('#modal-inq').find('#date').text($data.info.date);
						$('#modal-inq').find('#time').text($data.info.time);
						$('#modal-inq').find('#username').text($data.info.username);
						$('#modal-inq').find('#inq-username').text($data.info.username);
					}
				});
			});

			$(document).on('click', '.btn-reply', function(){
				$('#div-reply').removeClass('hidden');
				$('#inq-username').text($('#username').text());
				$('#reply-message').val('');
			});

			$('#frm-reply').on('submit', function($e){
				$e.preventDefault();

				var $frm = $(this);
				var $btn = $frm.find('button[type="submit"]');

				$btn.button('loading');

				$.post($frm.prop('action'), $frm.serialize(), function($data, $status){
					if($status == "success"){
						swal('Inquiry Reply', 'Message successfully sent!', 'success').then($res => {
							$('#div-inq').removeClass('hidden');
	    					$('.modal-loader').addClass('hidden');
	    					$('#modal-inq').modal('hide');
						});
					}

					$btn.button('reset');
				});
			});

			$(document).on('click', '.crm-reply', function(){
				$(this).closest('td').find('.modal-inq').click();
				$('#modal-inq').find('.btn-reply').click();
			});

			$(document).on('click', '.crm-reply-history', function(){
				$('.modal-loader').removeClass('hidden');
				$('#reply-history').addClass('hidden');
				$('#modal-reply-history').find('.list-group').empty();
				$('#modal-reply-history').modal('show');

				$.post('{{ route('inquiry.crm.replies') }}', { _token: '{{ csrf_token() }}', id: $(this).data('id') }, function($data, $status){
					if($status == "success"){
						if($data.length){
							for(var $i = 0; $i < $data.length; $i++){
								$('#modal-reply-history').find('.list-group').append('<div class="list-group-item">\
										<strong>' + $data[$i].title + '</strong>\
										<br />\
										<small>' + $data[$i].created_at + '</small>\
										<br /><br />\
										<p>' + $data[$i].message + '</p>\
									</div>');
							}
						} else{
							$('#modal-reply-history').find('.list-group').html('<p align="center"><br />No replies found.</p>');
						}

    					$('.modal-loader').addClass('hidden');
						$('#reply-history').removeClass('hidden');
					}
				});
			});
        });
    </script>
@endsection