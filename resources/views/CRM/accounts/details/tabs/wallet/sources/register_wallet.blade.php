<div class="wallet-source text-center">
    <h3>
        <span class="label label-success">{{ $register_wallet_name }}</span>
    </h3>

    <h3>
        <small>AMOUNT</small>
        <br>
        <code>{{ $register_wallet_amount }}</code>
    </h3>

    <h3>
        <small>TYPE</small>
        <br>
        <code>{{ $register_wallet_type }}</code>
    </h3>

    <br>

    <p>
        <i class="fa fa-calendar-check-o"></i> {{ $created_at }}
    </p>
</div>