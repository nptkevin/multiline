<div id="personal" class="tab-pane fade">
    <div class="row">
        <div class="col-md-4">
            <form name="formeditaccount" id="formeditaccount" action="{{ route('accounts.update', $account) }}#personal"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="form-group">
                    <label for="username-input" class="control-label">Username</label>
                    <input type="text" class="form-control" id="username-input" name="username"
                           placeholder="First Name" disabled value="{{ old('first_name', $account->username) }}">
                </div>

                <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="first-name-input" class="control-label">First Name</label>
                    <input type="text" class="form-control" id="first-name-input" name="first_name"
                           placeholder="First Name" value="{{ old('first_name', $account->first_name) }}">

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="last-name-input" class="control-label">Last Name</label>
                    <input type="text" class="form-control" id="last-name-input" name="last_name"
                           placeholder="Last Name" value="{{ old('last_name', $account->last_name) }}">

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>

                {{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
                    {{--<label for="email-input" class="control-label">Email</label>--}}
                    <input type="hidden" class="form-control" id="email-input"
                           name="email" placeholder="Email" value="{{ old('email', $account->email) }}">

                    {{--@if ($errors->has('email'))--}}
                        {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('email') }}</strong>--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</div>--}}


                <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }} temp-hide">
                    <label for="gender-input" class="control-label">Gender</label>
                    <select class="form-control" id="gender-input" name="gender">
                        <option value=""> -- Select Gender -- </option>
                        @foreach($genders as $gender)
                            <option {{ old('gender', $account->gender) == $gender ? "selected" : "" }}>{{ $gender }}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('gender'))
                        <span class="help-block">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('birth_date') ? ' has-error' : '' }}">
                    <label for="birth_date-input" class="control-label">Date of Birth</label>
                    <input type="text" class="form-control" data-date-end-date="-18y" id="birth-date-input" name="birth_date" placeholder="Select Date of Birth" value="{{ old('birth_date', $account->birth_date) }}">

                    @if ($errors->has('birth_date'))
                        <span class="help-block">
                            <strong>{{ $errors->first('birth_date') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="modal-edit-status" class="control-label">Country</label>
                    <select class="form-control" id="country_select" name="country"><option value="Afghanistan">Afghanistan</option><option value="Aland Islands">Aland Islands</option><option value="Albania">Albania</option><option value="Algeria">Algeria</option><option value="American Samoa">American Samoa</option><option value="Andorra">Andorra</option><option value="Angola">Angola</option><option value="Anguilla">Anguilla</option><option value="Antarctica">Antarctica</option><option value="Antigua And Barbuda">Antigua And Barbuda</option><option value="Argentina">Argentina</option><option value="Armenia">Armenia</option><option value="Aruba">Aruba</option><option value="Australia">Australia</option><option value="Austria">Austria</option><option value="Azerbaijan">Azerbaijan</option><option value="Bahamas">Bahamas</option><option value="Bahrain">Bahrain</option><option value="Bangladesh">Bangladesh</option><option value="Barbados">Barbados</option><option value="Belarus">Belarus</option><option value="Belgium">Belgium</option><option value="Belize">Belize</option><option value="Benin">Benin</option><option value="Bermuda">Bermuda</option><option value="Bhutan">Bhutan</option><option value="Bolivia">Bolivia</option><option value="Bosnia And Herzegovina">Bosnia And Herzegovina</option><option value="Botswana">Botswana</option><option value="Bouvet Island">Bouvet Island</option><option value="Brazil">Brazil</option><option value="British Indian Ocean Territory">British Indian Ocean Territory</option><option value="Brunei Darussalam">Brunei Darussalam</option><option value="Bulgaria">Bulgaria</option><option value="Burkina Faso">Burkina Faso</option><option value="Burundi">Burundi</option><option value="Cambodia">Cambodia</option><option value="Cameroon">Cameroon</option><option value="Canada">Canada</option><option value="Cape Verde">Cape Verde</option><option value="Cayman Islands">Cayman Islands</option><option value="Central African Republic">Central African Republic</option><option value="Chad">Chad</option><option value="Chile">Chile</option><option value="China">China</option><option value="Christmas Island">Christmas Island</option><option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option><option value="Colombia">Colombia</option><option value="Comoros">Comoros</option><option value="Congo">Congo</option><option value="Congo, Democratic Republic">Congo, Democratic Republic</option><option value="Cook Islands">Cook Islands</option><option value="Costa Rica">Costa Rica</option><option value="Cote D'Ivoire">Cote D'Ivoire</option><option value="Croatia">Croatia</option><option value="Cuba">Cuba</option><option value="Cyprus">Cyprus</option><option value="Czech Republic">Czech Republic</option><option value="Denmark">Denmark</option><option value="Djibouti">Djibouti</option><option value="Dominica">Dominica</option><option value="Dominican Republic">Dominican Republic</option><option value="Ecuador">Ecuador</option><option value="Egypt">Egypt</option><option value="El Salvador">El Salvador</option><option value="Equatorial Guinea">Equatorial Guinea</option><option value="Eritrea">Eritrea</option><option value="Estonia">Estonia</option><option value="Ethiopia">Ethiopia</option><option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option><option value="Faroe Islands">Faroe Islands</option><option value="Fiji">Fiji</option><option value="Finland">Finland</option><option value="France">France</option><option value="French Guiana">French Guiana</option><option value="French Polynesia">French Polynesia</option><option value="French Southern Territories">French Southern Territories</option><option value="Gabon">Gabon</option><option value="Gambia">Gambia</option><option value="Georgia">Georgia</option><option value="Germany">Germany</option><option value="Ghana">Ghana</option><option value="Gibraltar">Gibraltar</option><option value="Greece">Greece</option><option value="Greenland">Greenland</option><option value="Grenada">Grenada</option><option value="Guadeloupe">Guadeloupe</option><option value="Guam">Guam</option><option value="Guatemala">Guatemala</option><option value="Guernsey">Guernsey</option><option value="Guinea">Guinea</option><option value="Guinea-Bissau">Guinea-Bissau</option><option value="Guyana">Guyana</option><option value="Haiti">Haiti</option><option value="Heard Island &amp; Mcdonald Islands">Heard Island &amp; Mcdonald Islands</option><option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option><option value="Honduras">Honduras</option><option value="Hong Kong">Hong Kong</option><option value="Hungary">Hungary</option><option value="Iceland">Iceland</option><option value="India">India</option><option value="Indonesia">Indonesia</option><option value="Iran, Islamic Republic Of">Iran, Islamic Republic Of</option><option value="Iraq">Iraq</option><option value="Ireland">Ireland</option><option value="Isle Of Man">Isle Of Man</option><option value="Israel">Israel</option><option value="Italy">Italy</option><option value="Jamaica">Jamaica</option><option value="Japan">Japan</option><option value="Jersey">Jersey</option><option value="Jordan">Jordan</option><option value="Kazakhstan">Kazakhstan</option><option value="Kenya">Kenya</option><option value="Kiribati">Kiribati</option><option value="Korea">Korea</option><option value="Kuwait">Kuwait</option><option value="Kyrgyzstan">Kyrgyzstan</option><option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option><option value="Latvia">Latvia</option><option value="Lebanon">Lebanon</option><option value="Lesotho">Lesotho</option><option value="Liberia">Liberia</option><option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option><option value="Liechtenstein">Liechtenstein</option><option value="Lithuania">Lithuania</option><option value="Luxembourg">Luxembourg</option><option value="Macao">Macao</option><option value="Macedonia">Macedonia</option><option value="Madagascar">Madagascar</option><option value="Malawi">Malawi</option><option value="Malaysia">Malaysia</option><option value="Maldives">Maldives</option><option value="Mali">Mali</option><option value="Malta">Malta</option><option value="Marshall Islands">Marshall Islands</option><option value="Martinique">Martinique</option><option value="Mauritania">Mauritania</option><option value="Mauritius">Mauritius</option><option value="Mayotte">Mayotte</option><option value="Mexico">Mexico</option><option value="Micronesia, Federated States Of">Micronesia, Federated States Of</option><option value="Moldova">Moldova</option><option value="Monaco">Monaco</option><option value="Mongolia">Mongolia</option><option value="Montenegro">Montenegro</option><option value="Montserrat">Montserrat</option><option value="Morocco">Morocco</option><option value="Mozambique">Mozambique</option><option value="Myanmar">Myanmar</option><option value="Namibia">Namibia</option><option value="Nauru">Nauru</option><option value="Nepal">Nepal</option><option value="Netherlands">Netherlands</option><option value="Netherlands Antilles">Netherlands Antilles</option><option value="New Caledonia">New Caledonia</option><option value="New Zealand">New Zealand</option><option value="Nicaragua">Nicaragua</option><option value="Niger">Niger</option><option value="Nigeria">Nigeria</option><option value="Niue">Niue</option><option value="Norfolk Island">Norfolk Island</option><option value="Northern Mariana Islands">Northern Mariana Islands</option><option value="Norway">Norway</option><option value="Oman">Oman</option><option value="Pakistan">Pakistan</option><option value="Palau">Palau</option><option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option><option value="Panama">Panama</option><option value="Papua New Guinea">Papua New Guinea</option><option value="Paraguay">Paraguay</option><option value="Peru">Peru</option><option value="Philippines">Philippines</option><option value="Pitcairn">Pitcairn</option><option value="Poland">Poland</option><option value="Portugal">Portugal</option><option value="Puerto Rico">Puerto Rico</option><option value="Qatar">Qatar</option><option value="Reunion">Reunion</option><option value="Romania">Romania</option><option value="Russian Federation">Russian Federation</option><option value="Rwanda">Rwanda</option><option value="aint Barthelemy">Saint Barthelemy</option><option value="Saint Helena">Saint Helena</option><option value="Saint Kitts And Nevis">Saint Kitts And Nevis</option><option value="Saint Lucia">Saint Lucia</option><option value="Saint Martin">Saint Martin</option><option value="Saint Pierre And Miquelon">Saint Pierre And Miquelon</option><option value="Saint Vincent And Grenadines">Saint Vincent And Grenadines</option><option value="Samoa">Samoa</option><option value="San Marino">San Marino</option><option value="Sao Tome And Principe">Sao Tome And Principe</option><option value="Saudi Arabia">Saudi Arabia</option><option value="Senegal">Senegal</option><option value="Serbia">Serbia</option><option value="Seychelles">Seychelles</option><option value="Sierra Leone">Sierra Leone</option><option value="Singapore">Singapore</option><option value="Slovakia">Slovakia</option><option value="Slovenia">Slovenia</option><option value="Solomon Islands">Solomon Islands</option><option value="Somalia">Somalia</option><option value="South Africa">South Africa</option><option value="South Georgia And Sandwich Isl.">South Georgia And Sandwich Isl.</option><option value="Spain">Spain</option><option value="Sri Lanka">Sri Lanka</option><option value="Sudan">Sudan</option><option value="Suriname">Suriname</option><option value="Svalbard And Jan Mayen">Svalbard And Jan Mayen</option><option value="Swaziland">Swaziland</option><option value="Sweden">Sweden</option><option value="Switzerland">Switzerland</option><option value="Syrian Arab Republic">Syrian Arab Republic</option><option value="Taiwan">Taiwan</option><option value="Tajikistan">Tajikistan</option><option value="Tanzania">Tanzania</option><option value="Thailand">Thailand</option><option value="Timor-Leste">Timor-Leste</option><option value="Togo">Togo</option><option value="Tokelau">Tokelau</option><option value="Tonga">Tonga</option><option value="Trinidad And Tobago">Trinidad And Tobago</option><option value="Tunisia">Tunisia</option><option value="Turkey">Turkey</option><option value="Turkmenistan">Turkmenistan</option><option value="Turks And Caicos Islands">Turks And Caicos Islands</option><option value="Tuvalu">Tuvalu</option><option value="Uganda">Uganda</option><option value="Ukraine">Ukraine</option><option value="United Arab Emirates">United Arab Emirates</option><option value="United Kingdom">United Kingdom</option><option value="United States">United States</option><option value="United States Outlying Islands">United States Outlying Islands</option><option value="Uruguay">Uruguay</option><option value="Uzbekistan">Uzbekistan</option><option value="Vanuatu">Vanuatu</option><option value="Venezuela">Venezuela</option><option value="Viet Nam">Viet Nam</option><option value="Virgin Islands, British">Virgin Islands, British</option><option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option><option value="Wallis And Futuna">Wallis And Futuna</option><option value="Western Sahara">Western Sahara</option><option value="Yemen">Yemen</option><option value="Zambia">Zambia</option><option value="Zimbabwe">Zimbabwe</option></select>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="add-password-input" class="control-label">Password</label>
                            <div class="row">


                                <div class="col-md-8">
                                    <input type="password" disabled class="form-control" id="add-password-input" name="password"
                                           placeholder="" value="">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" class="btn btn-success hidden" id="generate_pass" data-toggle="popover" data-trigger="hover" data-content="Generate password"><i class="fa fa-refresh"></i></button>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label><input type="checkbox" id="change-password" value="">Change Password</label>
                            </div>

                            @if ($errors->has('password'))
                                <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif

                        </div>

                    </div>
                    <div class="col-md-6">

                        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="gender-input" class="control-label">Status</label>
                            <select class="form-control" id="status-input" name="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}" @if($status->status_id == $account->status_id) selected @endif>{{ $status->status_name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('status'))
                                <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                            @endif
                        </div>
                    </div>
                </div>





                {{--<div class="form-group">--}}
                    {{--<button type="button" id="reset-password-button" role="button" class="btn btn-warning">Reset Password</button>--}}
                {{--</div>--}}

                {{--<div class="form-group">--}}
                    {{--<p><br></p>--}}
                {{--</div>--}}

                @usercan('edit', 'accounts.update')
                <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">Save</button>
                @endusercan
            </form>
        </div>
    </div>
</div>

@include('CRM.accounts.forms.password_reset')

@section('scripts')
    @parent
    <script src="{{ asset('CRM/AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('styles')
    @parent
    <link rel="stylesheet" type="text/css" href="{{ asset('CRM/AdminLTE-2.4.2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
@endsection

@section('script')
    @parent
    <script src="{{ asset('CRM/Capital7-1.0.0/js/form-validation.js') }}"></script>

    <script>
//    var loadThumbnail = document.getElementById("img-thumbnail").complete;
//    var imgsrc= document.getElementById("img-thumbnail").src;
//    function loadImg(imgholder){
//       $('#img-thumbnail').attr('src',imgsrc);
//       if( $('#img-thumbnail').complete) $('#img-thumbnail').trigger('load');
//    }
        $(function () {

            var generatePassword = function(numLc, numUc, numDigits, numSpecial) {
                numLc = numLc || 4;
                numUc = numUc || 4;
                numDigits = numDigits || 4;
                numSpecial = numSpecial || 2;


                var lcLetters = 'abcdefghijklmnopqrstuvwxyz';
                var ucLetters = lcLetters.toUpperCase();
                var numbers = '0123456789';
                var special = '&@!#+';

                var getRand = function(values) {
                    return values.charAt(Math.floor(Math.random() * values.length));
                }

                //+ Jonas Raoni Soares Silva
                //@ http://jsfromhell.com/array/shuffle [v1.0]
                function shuffle(o){ //v1.0
                    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
                    return o;
                };

                var pass = [];
                for(var i = 0; i < numLc; ++i) { pass.push(getRand(lcLetters)) }
                for(var i = 0; i < numUc; ++i) { pass.push(getRand(ucLetters)) }
                for(var i = 0; i < numDigits; ++i) { pass.push(getRand(numbers)) }
                for(var i = 0; i < numSpecial; ++i) { pass.push(getRand(special)) }

                return shuffle(pass).join('');
            };

            $('#generate_pass').click(function(){

                var randPassword = generatePassword(3,3,1,1);

                $('#add-password-input').val(randPassword);
            });

            $('#change-password').change(function(){
                if (this.checked) {
                    $('#add-password-input').attr('type', 'text');
                    $('#add-password-input').prop('disabled', false);
                    $('#add-password-input').prop('required', true);
                    $('#generate_pass').removeClass('hidden')
                } else {
                    $('#add-password-input').val('');
                    $('#add-password-input').attr('type', 'password');
                    $('#add-password-input').prop('disabled', true);
                    $('#add-password-input').prop('required', false);
                    $('#generate_pass').addClass('hidden')

                }
            });

            $('#reset-password-button').click(function (e) {
                var modal = $('#modal-user-password-reset');
                var form = modal.find('form');
                $('#user-id-change-password').val('{{ $account->id }}');
//                form.attr('action', form.data('temp-action') + "/" + $(this).data('id') + form.data('append-action'));
                modal.modal('show');
            });

            $("#modal-user-password-reset").on("hidden.bs.modal", function () {
                var form = $('#formuserpasswordreset');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formuserpasswordreset').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
//                            location.reload();

                        swal({
                            'title' : 'Success',
                            'html' : 'Password has changed',
                            'type' : 'success'
                        })
                    }
                }).fail(function(xhr, status, error) {
                    assocErr(xhr.responseJSON.errors, form);
                    btn.button('reset');

                }).done(function () {
                    btn.button('reset');
                });
            });

            var country_text = '{{ $account->country??'' }}';
            if(country_text != ''){
                $("#country_select").val(country_text);
            }


            $('#birth-date-input').datepicker({
                clearBtn: true,
                format: "MM d, yyyy"
            });

            $('#formeditaccount').find('#country').select2({
                placeholder: "Select a country",
                data: countries
            }).val("{{ old('country', $account->country) }}").trigger('change');

            $('#formeditaccount').submit(function () {
                $(this).find(':submit').button('loading');
            });
//            if(loadThumbnail == false){
//                setTimeout(function(){ loadImg(); }, 2000);
//                }
        });
    </script>
@endsection