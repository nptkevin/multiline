@if($pending_deposits->count())
    <table id="deposit-tickets-table" class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Notes</th>
            <th>Date</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pending_deposits as $pending_deposit)
            <tr data-deposit-id="{{ $pending_deposit->wallet_deposit_id }}">
                <td data-name="{{ $pending_deposit->account->getDisplayName() }}">
                    {{ $pending_deposit->account->getDisplayName() }}
                </td>
                <td data-notes="{{ $pending_deposit->notes }}" title="{{ $pending_deposit->notes }}" class="ellipted">
                    {{ $pending_deposit->notes }}
                </td>
                <td data-date="{{ $pending_deposit->created_at }}">{{ $pending_deposit->created_at }}</td>
                <td>
                    <a href="{{ route('wallet.deposit.index') }}?wallet_deposit_id={{ $pending_deposit->wallet_deposit_id }}" class="btn btn-link">View</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <p class="text-muted text-center">{{ trans('validation.custom.pending_deposit.empty') }}</p>
@endif