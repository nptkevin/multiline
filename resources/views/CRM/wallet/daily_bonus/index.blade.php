@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Daily Bonus</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    @if($daily_bonuses->count())
                        <table id="daily-bonus-table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Currency</th>
                                <th>Ponits</th>
                                <th>Type</th>
                                @usercan('edit', 'daily_bonuses.update')
                                <th>Actions</th>
                                @endusercan
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($daily_bonuses as $daily_bonus)
                                <tr data-daily-bonus-id="{{ $daily_bonus->daily_bonus_id }}">
                                    <td data-currency-id="{{ $daily_bonus->currency_id }}">{{ $daily_bonus->currency->currency }}</td>
                                    <td data-points="{{ number_format($daily_bonus->points, 2) }}">{{ number_format($daily_bonus->points, 2) }}</td>
                                    <td data-type="{{ $daily_bonus->type }}">{{ $daily_bonus->type }}</td>
                                    @usercan('edit', 'daily_bonuses.update')
                                    <td>
                                        <button type="button" onclick="setFormEditDailyBonus(this);"
                                                class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></button>
                                    </td>
                                    @endusercan
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted">{{ trans('validation.custom.daily_bonus.empty') }}</p>
                    @endif
                </div>
                @usercan('add', 'daily_bonuses.store')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs btn-add-daily-bonus"
                            id="add-daily-bonus-btn" title="Add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                @endusercan
            </div>
        </div>
    </div>

    @include('CRM.wallet.daily_bonus.forms.add')
    @include('CRM.wallet.daily_bonus.forms.edit')
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        var setFormEditDailyBonus = function (src) {
            var form = $('#form-edit-daily-bonus');
            var tr = $(src).closest('tr');
            form.attr('data-daily-bonus-id', tr.data('daily-bonus-id'));
            form.find('input[name=points]').val(tr.find('td:eq(1)').data('points'));
            form.find('select[name=currency_id]').val(tr.find('td:eq(0)').data('currency-id')).trigger('change');
            form.find('select[name=type]').val(tr.find('td:eq(2)').data('type')).trigger('change');
            $('#modal-edit-daily-bonus').modal('show');
        };

        $(function () {
            $('#add-daily-bonus-btn').on('click', function () {
                $('#modal-add-daily-bonus').modal('show');
            });
        });
    </script>
@endsection