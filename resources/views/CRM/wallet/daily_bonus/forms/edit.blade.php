<div class="modal fade" id="modal-edit-daily-bonus" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-daily-bonus" id="form-edit-daily-bonus"
                  data-temp-action="/admin/wallet/daily_bonuses"
                  method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Daily Bonus</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="edit-type-input" class="col-sm-3 control-label">Type</label>

                        <div class="col-sm-8">
                            <select id="edit-type-input" class="form-control" name="type">
                                @foreach($types as $type)
                                    <option value="{{ $type }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-currency-id-input" class="col-sm-3 control-label">Currency</label>

                        <div class="col-sm-8">
                            <select id="edit-currency-id-input" class="form-control" name="currency_id">
                                @foreach($in_app_currencies as $currency)
                                    <option value="{{ $currency->currency_id }}">{{ $currency->currency }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edit-points-input" class="col-sm-3 control-label">Points</label>

                        <div class="col-sm-8">
                            <input type="number" step='0.01' class="form-control" name="points"
                                   id="edit-points-input"
                                   placeholder="Points">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'daily_bonuses.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        $(function () {
            $("#modal-edit-daily-bonus").on("hidden.bs.modal", function () {
                var form = $('#form-edit-daily-bonus');
                form.removeAttr('data-daily-bonus-id');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-daily-bonus').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.data('temp-action') + '/' + form.data('daily-bonus-id');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = form.data('temp-action');
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>

@endsection
