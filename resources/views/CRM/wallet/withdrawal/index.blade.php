@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Withdrawal</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="withdrawal-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Name/Email</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th>Bank</th>
                            <th style="width:15%;">Notes</th>
                            <th style="width:15%;">CRM Notes</th>

                            @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_attachments'))
                                <th style="width:15%;">Attachment</th>
                            @endif

                            @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_status') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_notes') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.updatenote'))
                                <th style="width:10%;">Action</th>
                            @endif
                            <th class="hidden_col">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wallet_withdrawals as $wallet_withdrawal)

                            @if($wallet_withdrawal->status->status_name == $pending->status_name)
                                 <tr style="background-color: #cfcfc463;"
                                    data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}">
                            @elseif($wallet_withdrawal->status->status_name == $approve->status_name)
                                 <tr style="background-color: #77dd7763;"  data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}">
                            @elseif($wallet_withdrawal->status->status_name == $deny->status_name)
                               <tr style="background-color: #ff69613d;"  data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}">
                                    @endif
                                    <td data-created_at="{{$wallet_withdrawal->created_at}}">{{$wallet_withdrawal->created_at->toDateString()}}
                                        <br> <span
                                                style="font-size:small;">{{$wallet_withdrawal->created_at->toTimeString()}} </span>
                                    </td>
                                    <td data-email_and_name="{{ $wallet_withdrawal->account->email}} / @if(isset($wallet_withdrawal->account->last_name) && isset($wallet_withdrawal->account->first_name))
                                    {{ucfirst($wallet_withdrawal->account->last_name). ', '.ucfirst($wallet_withdrawal->account->first_name)}}
                                    @else
                                    {{ucfirst($wallet_withdrawal->account->last_name).ucfirst($wallet_withdrawal->account->first_name)}}
                                    @endif">
                                        <a href="/admin/accounts/{{ $wallet_withdrawal->account->id}}#personal">

                                            @if(isset($wallet_withdrawal->account->last_name) && isset($wallet_withdrawal->account->first_name))
                                                {{  ucfirst($wallet_withdrawal->account->last_name). ', '.   ucfirst($wallet_withdrawal->account->first_name)}}
                                            @elseif(!isset($wallet_withdrawal->account->last_name) && !isset($wallet_withdrawal->account->first_name))
                                                N/A
                                            @else{{  ucfirst($wallet_withdrawal->account->last_name) .   ucfirst($wallet_withdrawal->account->first_name)}}
                                            @endif
                                            <br>
                                            {{ $wallet_withdrawal->account->email }}
                                        </a>
                                    </td>

                                    <td data-account="{{ is_null($wallet_withdrawal->wallet) ? 'N/A' : $wallet_withdrawal->wallet->wallet_account_number }}">
                                        {{ is_null($wallet_withdrawal->wallet) ? 'N/A' : $wallet_withdrawal->wallet->wallet_account_number }}
                                    </td>

                                    <td data-amount="{{ $wallet_withdrawal->currency->currency  .' '. number_format($wallet_withdrawal->amount, 8) }}">
                                        {{ $wallet_withdrawal->currency->currency  . ' ' . number_format($wallet_withdrawal->amount, 8) }}
                                    </td>
                                   <td data-bank-info-id="{{$wallet_withdrawal->bankinfo_id ? 'N/A' : $wallet_withdrawal->bankinfo_id}}">
                                       @if($wallet_withdrawal->bankinfo_id)
                                           <a href="/admin/accounts/{{ $wallet_withdrawal->user_id }}/?bankinfo_id={{$wallet_withdrawal->bankinfo_id}}#bank-info">
                                               Bank Information
                                           </a>
                                       @else
                                           N/A
                                       @endif
                                   </td>
                                    <td data-note="{{$wallet_withdrawal->notes}}">
                                        <a href="javascript:void(0)"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="{{$wallet_withdrawal->notes}}"> {{strlen($wallet_withdrawal->notes) > 25 ?  substr($wallet_withdrawal->notes,'0','25') . "..." : (strlen($wallet_withdrawal->notes) > 0 ?  $wallet_withdrawal->notes: "N/A") }} </a>
                                    </td>
                                    <td data-crm_note="{{$wallet_withdrawal->crm_note}}">

                                        <a href="javascript:void(0)"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           title="{{$wallet_withdrawal->crm_note}}"> {{strlen($wallet_withdrawal->crm_note) > 25 ?  substr($wallet_withdrawal->crm_note,'0','25') . "..." : (strlen($wallet_withdrawal->crm_note) > 0 ?  $wallet_withdrawal->crm_note: "N/A") }} </a>

                                    </td>
                                    <td data-attachment="{{ $wallet_withdrawal->attachments }}"
                                        data-wallet_withdrawal_id="{{ $wallet_withdrawal->wallet_withdrawal_id }}"
                                        class="ellipted">
                                        <span class="col-sm-10">
                                            @if(isset($wallet_withdrawal->attachments))
                                                {{-- <a href="javascript:void(0)" data-toggle="tooltip"data-placement="top" onclick='display_preview("{{$wallet_withdrawal->attachments}}")' > {{basename($wallet_withdrawal->attachments)}} </a> --}}
                                                With Attachment
                                            @else
                                                No Attachment
                                            @endif

                                        </span>
                                        <span class="pull-right col-sm-2">

                                        <span class="pull-right">
                                                                            <button type="button" role="button"
                                                                                    class="btn btn-default btn-xs"
                                                                                    onclick="upload(this);">
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                            <input id='attachment' type="file"
                                                                                   style="display:none"
                                                                                   data-wallet-withdrawal-id="{{$wallet_withdrawal->wallet_withdrawal_id}}"
                                                                                   class="attachments"></span>
                                        </span>


                                    </td>
                                    @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_status') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_notes') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.updatenote'))
                                        <td>
                                            @if(strtolower($wallet_withdrawal->status->status_name) == "pending")
                                                <button type="button" role="button" class="btn btn-xs btn-success btn-approval"
                                                        data-status-id="{{ $approve->status_id }}"
                                                        data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}"
                                                        onclick="setStatus(this);">
                                                    <i class="fa fa-check-circle"></i> Approve
                                                </button>
                                                <button type="button" role="button" class="btn btn-xs btn-danger btn-approval"
                                                        data-status-id="{{ $deny->status_id }}"
                                                        data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}"
                                                        onclick="setStatus(this);">
                                                    <i class="fa fa-ban"></i> Deny
                                                </button>

                                            @endif
                                            <button href="details_{{$wallet_withdrawal->wallet_withdrawal_id}}"
                                                    class="btn btn-primary btn-xs btn-approval btntoggle"
                                                    data-user-id="{{$wallet_withdrawal->wallet_withdrawal_id}}"

                                                    data-wallet-withdrawal-id="{{ $wallet_withdrawal->wallet_withdrawal_id }}"
                                                    data-view-id="details_{{$wallet_withdrawal->wallet_withdrawal_id}}"
                                            ><i class="fa fa-eye"></i> View
                                            </button>
                                        </td>
                                    @endif
                                    <td class="hidden_col"
                                        data-status="{{$wallet_withdrawal->status->status_name}}">{{ $wallet_withdrawal->status->status_name }}</td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--@include('CRM.wallet.withdrawal.forms.withdrawal')--}}
    @include('CRM.wallet.details')
@endsection

@section('style')
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css") }}">
    <style>
        .swal2-actions button {
            margin-right: 5px;
        }

        .ellipted div {
            max-width: 360px !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }

        th, td {
            text-align: center;
        }

        .right_aligned {
            text-align: right;
        }

        textarea.swal2-textarea {
            resize: none;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 0;
            border-radius: 4px;
        }

        .hidden_col {
            display: none;
        }

        .tooltip-inner {

            white-space: normal; /* you can also try white-space: normal; */
            max-width: 300px;
            overflow-wrap: break-word;

        }


    </style>
@endsection



@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>

    <!-- Select2 -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    @include('CRM.layouts.footerscript')
    <script>
        function setStatus(src) {
            var btn = $('.btn-approval');
            var status_before = '';
            var status_after = '';
            var tr = $(src).closest("tr");

            switch (parseInt($(src).data("status-id"))) {
                case parseInt('{{ $approve->status_id }}'):
                    status_after = "{{ ucfirst($approve->status_name) }}d";
                    status_before = "Approval";
                    break;
                case parseInt('{{ $deny->status_id }}'):
                    status_after = "{{ ucfirst($deny->status_name) }}";
                    status_before = "Deny";
                    break;
            }

            // swal({
            //     title: 'Are you sure set this item as ' + status_after + '?',
            //     type: 'warning',
            //     showCancelButton: true,
            //     confirmButtonColor: '#3085d6',
            //     cancelButtonColor: '#d33',
            //     confirmButtonText: 'Yes, set as ' + status_after + '!',
            //     cancelButtonText: 'No, cancel!',
            //     confirmButtonClass: 'btn btn-success',
            //     cancelButtonClass: 'btn btn-danger',
            //     buttonsStyling: false,
            //     reverseButtons: true
            // }).then(function (result) {
            //     if (result.value) {
                    swal({
                        title: 'Action: ' + status_after,
                        // text: 'Please input notes proceed.',
                        // input: 'textarea',
                        html: '<textarea class="swal2-textarea" id="swal2-notes" placeholder="Notes"></textarea><input type="password" class="swal2-input" id="swal2-pw" placeholder="Password">',
                        showCancelButton: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        confirmButtonText: 'Proceed',
                        showLoaderOnConfirm: true,
                        preConfirm: function() {
                            return new Promise(function(resolve) {
                                if ($('#swal2-notes').val() === false || $('#swal2-notes').val() === "") {
                                    swal.showValidationError('Note is required!');
                                    btn.button('reset');
                                    resolve(0);
                                } else if($('#swal2-pw').val() === false || $('#swal2-pw').val() === ""){
                                    swal.showValidationError('Password is required!');
                                    btn.button('reset');
                                    resolve(0);
                                } else if ($('#swal2-notes').val() && $('#swal2-pw').val()) {
                                    // swal({
                                    //     title: 'Confirm Transaction',
                                    //     text: 'Please input your password to proceed.',
                                    //     input: 'password',
                                    //     showCancelButton: true,
                                    //     allowEscapeKey: false,
                                    //     allowOutsideClick: false,
                                    //     confirmButtonText: 'Proceed',
                                    //     showLoaderOnConfirm: true,
                                    //     autocomplete: "new-password",
                                    //     preConfirm: function(password) {
                                    //         return new Promise(function(resolve) {
                                    //             if (password === false || password === ""){
                                    //                 swal.showValidationError('Password is required!');
                                    //                 resolve();
                                    //             }else if(password){
                                                    $.post('{{ route('wallet.withdrawal.set_status') }}', {
                                                        wallet_withdrawal_id: tr.data("wallet-withdrawal-id"),
                                                        status_id: $(src).data("status-id"),
                                                        _token: "{{ csrf_token() }}",
                                                        password: $('#swal2-pw').val(),
                                                        crm_notes: $('#swal2-notes').val()
                                                    }).done(function (response) {
                                                        console.log(response);
                                                        if (response.status == '{{ config('response.type.fail') }}' || response.status == '{{ config('response.type.error') }}') {
                                                            if (response.message) {
                                                                swal.showValidationError(response.message);
                                                            }

                                                            if (response.data) {
                                                                swal.showValidationError(response.data);
                                                            }

                                                            $('.swal2-confirm').closest('div').removeClass('swal2-loading').find('button').prop('disabled', false);
                                                        } else {
                                                            swal('Withdrawal ' + status_after + '!', '', 'success').then(function (result) {
                                                                if (result.value) {
                                                                    location.reload();
                                                                }
                                                            });
                                                        }
                                                    });
                                    //             }
                                    //         });
                                    //     },
                                    // });
                                }
                            });
                        },
                    });
            //     }
            // });
        }


            function upload(btn) {
                $(btn).parent().find("input.attachments").click();
            }

            function note(wallet_withdrawal_id) {
                alert("TODO");
            }

        $(function () {
            $('#withdrawal-table').DataTable({
                "stateSave": true,
                "responsive": true,
                columnDefs: [
                    {
                        targets: 5,
                        orderable: false,
                        searchable: false
                    },
                    {
                        targets: 7,
                        orderable: false,
                        searchable: false
                    },
                    {
                        targets: 0,
                        orderable: true,
                        searchable: true
                    }


                    @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_status') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.set_notes') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.withdrawal.updatenote'))
                    @endif
                ],
                "orderFixed": [8, 'desc'],
                "dom": 'lfrtip<"clear"><"legend">'
            });
            $("div.legend").html('<strong>LEGEND: </strong><br>' +
                '<div style="background-color: #d3d3d39e; width:100px; padding-left: 5px;"> PENDING </div>' +
                '<div style="background-color: #00a65a91; width:100px; padding-left: 5px;"> APPROVED </div>' +
                '<div style="background-color: #f4433661; width:100px; padding-left: 5px;" > DENIED </div> ' +
                '');
            $("div.clear").html('<hr>');

        });


        $(".attachments").on("change", function () {
            var file = $(this)[0].files[0];
            var formData = new FormData();
            formData.append("attachments", file);
            formData.append("wallet_withdrawal_id", $(this).data("wallet-withdrawal-id"));
            formData.append("_token", "{{ csrf_token() }}");

            $.ajax({
                type: "POST",
                url: "{{ route('wallet.withdrawal.set_attachments') }}",
                data: formData,
                contentType: false,
                processData: false
            }).done(function () {
                location.reload();
            });
        });

    </script>

@endsection