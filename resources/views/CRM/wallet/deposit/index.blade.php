@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Deposit</li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="deposit-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Account</th>
                            <th>Amount</th>
                            <th style="width:15%;">Notes</th>
                            <th style="width:15%;">CRM Notes</th>
                            <th style="width:15%;" class="hidden">Attachment</th>
                            <th class="hidden">Deposited To</th>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updatenote') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.changeStatus') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updateStatus'))
                                <th style="width:10%;">Action</th>
                            @endif
                            <th class="hidden_col">Status</th>
                        </tr>
                        </thead>
                        @foreach($walletDeposit as $deposit)

                            @if($deposit->status->status_name == $pending->status_name)
                                <tr style="background-color: #cfcfc463;"  data-wallet-deposit-id="{{ $deposit->deposit_id }}">
                            @elseif($deposit->status->status_name == $approve->status_name)
                                <tr style="background-color: #77dd7763;"  data-wallet-deposit-id="{{ $deposit->deposit_id }}">
                            @elseif($deposit->status->status_name == $deny->status_name)
                                <tr style="background-color: #ff69613d;"  data-wallet-deposit-id="{{ $deposit->deposit_id }}">
                                    @endif
                                    <td data-created_at="{{$deposit->created_at}}">{{$deposit->created_at->toDateString()}}  <br> <span style="font-size:small;">{{$deposit->created_at->toTimeString()}} </span></td>
                                    <td data-email_and_name="  @if(isset($deposit->account->last_name) && isset($deposit->account->first_name))
                                    {{ $deposit->account->email . " / " .  ucfirst($deposit->account->last_name). ', '.   ucfirst($deposit->account->first_name)}}
                                    @else
                                    {{ $deposit->account->email . " / " .  ucfirst($deposit->account->last_name) .   ucfirst($deposit->account->first_name)}}
                                    @endif"><a href="/admin/accounts/{{$deposit->account->id}}#personal">
                                            @if(isset($deposit->account->last_name) && isset($deposit->account->first_name))
                                                {{  (ucfirst($deposit->account->last_name). ', '.   ucfirst($deposit->account->first_name))}}
                                            @elseif(!isset($deposit->account->last_name) && !isset($deposit->account->first_name))
                                                Name not setup

                                            @else
                                                {{  (ucfirst($deposit->account->last_name) .   ucfirst($deposit->account->first_name))}}
                                            @endif
                                            <br>
                                                {{$deposit->account->email }}
                                        </a>
                                    </td>
                                    <td data-account="{{ isset($deposit->wallet) ? ($deposit->wallet->wallet_account_number) : "N/A"}}">{{ isset($deposit->wallet) ? ($deposit->wallet->wallet_account_number) : "N/A"}}</td>
                                    <td data-amount="{{ $deposit->currency->currency . ' ' . number_format($deposit->amount,8,'.',',') }}">{{ $deposit->currency->currency . ' ' . number_format($deposit->amount,8,'.',',') }}</td>
                                    <td data-note="{{$deposit->notes}}"><a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="{{$deposit->notes}}"> {{strlen($deposit->notes) > 25 ?  substr($deposit->notes,'0','25') . "..." : (strlen($deposit->notes) > 0 ?  $deposit->notes: "N/A") }} </a></td>
                                    <td data-crm_note="{{$deposit->crm_note}}"><a href="javascript:void(0)"  data-toggle="tooltip" data-placement="top" title="{{$deposit->crm_note}}"> {{strlen($deposit->crm_note) > 25 ?  substr($deposit->crm_note,'0','25') . "..." : (strlen($deposit->crm_note) > 0 ?  $deposit->crm_note : "N/A")}} </a></td>
                                    <td class="hidden" data-attachment="{{$deposit->attachments}}">
                                       @if(isset($deposit->attachments))
                                          {{-- <a href="javascript:void(0)" onclick='display_preview("{{$deposit->attachments}}")' > {{basename($deposit->attachments)}} </a> --}}
                                            <a href="javascript:void(0)" onclick='display_preview("{{$deposit->attachments}}")'><i class="fa fa-external-link"></i> &nbsp; With Attachment</a>
                                           @else
                                           No Attachment
                                        @endif
                                    </td>
                                    <td class="hidden" data-depositinfo="">
                                        {!! is_null($deposit->depositinfo) ? "N/A" : nl2br($deposit->depositinfo->information) !!}
                                    </td>
                                    @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updatenote') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.changeStatus') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updateStatus'))
                                        <td>
                                            @if($deposit->status->status_name == "Pending")
                                                <button href="javascript:void(0)"
                                                        class="btn btn-success btn-xs btn-approval"
                                                        data-user-id="{{$deposit->user_id}}"
                                                        data-deposit-id="{{$deposit->wallet_deposit_id }}"
                                                        data-approvalDescription="Approve"
                                                        onclick="processApproval(this)"><i class="fa fa-thumbs-up"></i>
                                                    Approve
                                                </button> <a href="javascript:void(0)" class="btn btn-danger btn-xs btn-approval"
                                                             data-user-id="{{$deposit->user_id}}"
                                                             data-deposit-id="{{$deposit->wallet_deposit_id }}"
                                                             data-approvalDescription="Denied"
                                                             onclick="processApproval(this);"><i
                                                            class="fa fa-thumbs-down"></i> Reject</a>
                                            @endif
                                            <a href="javascript:void(0)"
                                               class="btn btn-primary btn-xs btn-approval btntoggle"
                                               data-user-id="{{$deposit->user_id}}"
                                               data-deposit-id="{{$deposit->wallet_deposit_id }}" title="collapse"
                                               href="details_{{$deposit->wallet_deposit_id}}"
                                               data-view-id="details_{{$deposit->wallet_deposit_id}}"><i
                                                        class="fa fa-eye"></i> View</a>
                                        </td>
                                    @endif
                                    <td class="hidden_col"
                                        data-status="{{$deposit->status->status_name}}">{{ $deposit->status->status_name }}</td>
                                </tr>

                                @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('CRM.wallet.details')

@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css") }}">
    <style>
        .swal2-actions button {
            margin-right: 5px;
        }

        .ellipted div {
            max-width: 360px !important;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden;
        }
        th,td{
            text-align: center;
        }

        .right_aligned{
            text-align: right;
        }

        textarea.swal2-textarea {
            resize: none;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 0;
            border-radius: 4px;
        }

        .hidden_col {
            display: none;
        }


        .tooltip-inner {

            white-space: normal; /* you can also try white-space: normal; */
            max-width: 300px;
            overflow-wrap: break-word;

        }

    </style>
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>

    <!-- Select2 -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
@endsection

@section('script')
    @parent
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>
    @include('CRM.layouts.footerscript')
    <script>
        function processApproval(evt){
            var btn = $('.btn-approval');
            btn.button('loading');
            var wallet_deposit_id = evt.getAttribute('data-deposit-id');
            var approvalStatus = evt.getAttribute('data-approvalDescription');
            var user_id = evt.getAttribute('data-user-id');
            var flag = "Approve";

            if (approvalStatus == "Denied")
                flag = "Reject";

            // swal({
                // title: 'Are you sure?',
                // text: "You want to " + flag + " this deposit?",
                // type: 'question',
                // showCancelButton: true,
                // confirmButtonColor: '#3085d6',
                // cancelButtonColor: '#d33',
                // confirmButtonText: 'Yes!',
                // allowOutsideClick: false,
            // }).then(function (result) {
                // btn.button('reset');
                // if (result.value) {
                    swal({
                        title: "Action: " + flag,
                        // text: 'Please input notes proceed.',
                        // input: 'textarea',
                        html: '<textarea class="swal2-textarea" id="swal2-notes" placeholder="Notes"></textarea><input type="password" class="swal2-input" id="swal2-pw" placeholder="Password">',
                        showCancelButton: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        confirmButtonText: 'Proceed',
                        showLoaderOnConfirm: true,
                        allowOutsideClick: false,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                if ($('#swal2-notes').val() === false || $('#swal2-notes').val() === "") {
                                    swal.showValidationError('Note is required!');
                                    btn.button('reset');
                                    resolve(0);
                                } else if($('#swal2-pw').val() === false || $('#swal2-pw').val() === ""){
                                    swal.showValidationError('Password is required!');
                                    btn.button('reset');
                                    resolve(0);
                                } else if ($('#swal2-notes').val() && $('#swal2-pw').val()) {
                                    // swal({
                                    //     title: 'Confirm Transaction',
                                    //     text: 'Please input your password to proceed.',
                                    //     input: 'password',
                                    //     showCancelButton: true,
                                    //     allowEscapeKey: false,
                                    //     allowOutsideClick: false,
                                    //     confirmButtonText: 'Proceed',
                                    //     showLoaderOnConfirm: true,
                                    //     allowOutsideClick: false,
                                    //     preConfirm: function (password) {
                                    //         return new Promise(function (resolve) {
                                    //             if (password === false || password === "") {
                                    //                 swal.showValidationError('Password is required!');
                                    //                 btn.button('reset');
                                    //                 resolve(0);
                                    //             } else if (password) {
                                                    var response = "";
                                                    var data = {
                                                        '_token': '{{csrf_token()}}',
                                                        'user_id': user_id,
                                                        'wallet_deposit_id': wallet_deposit_id,
                                                        'approvalStatus': approvalStatus,
                                                        'crm-note': $('#swal2-textarea').val(),
                                                        'password': $('#swal2-pw').val()
                                                    };

                                                    $.post("deposit/updateStatus", data, function (result) {
                                                        response = result[0];
                                                        swal(
                                                            (result['message']).toUpperCase(),
                                                            (result[0]).toUpperCase(),
                                                            result[0]
                                                        ).then(function (result) {
                                                            console.log(result);
                                                            if (result.value) {
                                                                if (response == 'success')
                                                                    location.reload();
                                                            }
                                                        });
                                                    }).done(function (data) {
                                                        btn.button('reset');
                                                    });
                                //                 }
                                //             });
                                //         },
                                //     });
                                }
                            });
                        },
                    }).then(function(){
                        btn.button('reset');
                    });
                // }
            // });
            //end
        }

        $('#deposit-table').DataTable({
            "stateSave": true,
            "responsive": true,
            "columnDefs": [
                {
                    targets: 5,
                    orderable: false,
                    searchable: false
                },
                {
                    targets: 7,
                    orderable: false,
                    searchable: false,
                    visible:false
                },
                {
                    targets: 0,
                    orderable: true,
                    searchable: true
                }

                @if(Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updatenote') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.changeStatus') || Auth::guard('crm')->user()->canAccess('edit', 'wallet.deposit.updateStatus'))

                @endif

            ],
            "orderFixed": [8, 'desc'],
            "dom": 'lfrtip<"clear"><"legend">'
        });


        $("div.legend").html('<strong>LEGEND: </strong><br>' +
            '<div style="background-color: #d3d3d39e; width:100px; padding-left: 5px;"> PENDING </div>' +
            '<div style="background-color: #00a65a91; width:100px; padding-left: 5px;"> APPROVED </div>' +
            '<div style="background-color: #f4433661; width:100px; padding-left: 5px;" > DENIED </div> ' +
            '');
        $("div.clear").html('<hr>');
        $('#deposit-table').on('click', '.add-note-link', function () {
            var form = $("#form-deposit-note");
            $('#deposit_id').val($(this).data('deposit-id'));
            $('#deposit-note').val($(this).data('deposit-note'));
            $('#modal-note').modal('show');
            $('.close').show();
            $('#btn_save').hide();
        });

    </script>

@endsection