@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Bank Informations</li>
    </ol>
@endsection
@section('content')
    <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="bankinfo_tbl" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Bank Information</th>
                                <th>Currency Code</th>
                                <th>Status</th>
                                @usercan('edit', 'wallet.bankinformation.update')
                                <th>Action </th>
                                @endusercan
                            </tr>
                            </thead>
                        </table>
                    </div>
                    @usercan('add', 'wallet.bankinformation.store')
                    <div class="box-footer">
                        <button type="button" class="btn btn-success btn-xs btn-add-currency"
                                data-toggle="modal"
                                data-target="#modal-add-bankinfo" title="Add">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                    @endusercan
                </div>
            </div>
        </div>
        <div>
      
      
       <input type='text' id='currencies' value='{{$currencies}}' hidden>
       <input type='text' id='statuses' value='{{$statuses}}' hidden>
        </div>
        @include('CRM.wallet.bankinfo.forms.add')
        @include('CRM.wallet.bankinfo.forms.edit')
@endsection
@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        var bankinfoTbl;
         function reloadTbl(){
                bankinfoTbl.ajax.reload();
             }
          
        function formatTextInput(data){
            var raw=data.split("\n");
            var info="";
            raw.forEach(function(element) {
                info+=$.trim(element);
                info+='\n'
            });
            return info;
        }
        function loadData(){  
                bankinfoTbl=  $('#bankinfo_tbl').DataTable( {
                    "processing": true,
                    "serverSide": true,
                    "stateSave": true,
                    "responsive": true,
                    "ajax":  {
                    "url": "/admin/wallet/bankinformation/datatable",
                    "error": function($err){
                        if($err.status == 401){
                            alert('Your session is expired! Please login again.');
                            window.location.href = '/admin/login';
                        }
                        }
                    },
                    'createdRow': function (tr, data, dataIndex) {
                        $(tr).attr('data-bankinfo-id', data.depositinfo_id);
                        $(tr).attr('data-status-id',data.status_id);
                        $(tr).attr('data-currency-id',data.currency_id);
                        $(tr).attr('data-default-account',data.default);
                    },
                    columnDefs: [{
                    @usercan('edit', 'wallet.bankinformation.update')
                    'targets': 4,
                    orderable: false
                   @endusercan
                    }],
                    columns: [
                       
                        {data: 'bank_name'}, 
                        {
                            data: 'information',
                            "render": function (data, type, row, meta) {
                                var raw=data.split("\n");
                                var info="";
                                raw.forEach(function(element) {
                                  info+=element+"</br>";
                                });

                                return info;
                            }
                        },
                        {
                            data: 'currency_id',
                            "render" :  function (data, type, row, meta) {
                                @foreach($currencies as $currency)
                                    if(data == '{{ $currency->currency_id}}'){
                                        return "{{ $currency->currency }}";
                                    }
                                @endforeach
                                else return "Not set!";
                            }

                        },
                        {
                            data: 'status_id' ,
                            "render" :  function (data, type, row, meta, tr) {
                                @foreach($statuses as $status)
                                    if(data == '{{ $status->status_id}}'){
                                        var stat= "{{ $status->status_name }} ";
                                        var defact= "";
                                        if( row.default== true){
                                            defact="<span class='badge' style='background-color:#077ded'>Default</span>";
                                        }
                                        return stat +" "+defact;
                                    }
                                @endforeach
                                else return "Not set!";
                            }
                        }
                        @usercan('edit', 'wallet.bankinformation.update')
                       ,
                        {
                    
                            "data": null,
                            "render": function (data, type, row, meta) {
                                
                                if (type === 'display') {
                                    
                                    data = '<button type="button" onclick="setFormEditBankInfo(this);"  class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></button>';
                                    return data;
                                }else return "";
                            
                                }
                       
                        } 
                         @endusercan   
                    
                    ]
                } );

             }
      $( document ).ready(function() {
             loadData();
           
         });
    </script>
@endsection