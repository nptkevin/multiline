@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">{{ $page_title }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="transfer-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            @usercan('add', 'wallet.transfer.transfer')
                            <th>Actions</th>
                            @endusercan
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('CRM.wallet.transfer.forms.transfer')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>

    <!-- Select2 -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <script>
        $(function () {
            $('#transfer-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "responsive": true,
                "ajax": "/admin/wallet/transfer/datatable",
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('add', 'wallet.transfer.transfer'))
                    {
                        targets: 3,
                        orderable: false
                    }
                    @endif
                ],
                "columns": [
                    {
                        "data": 'username',
                    },
                    {
                        "data": 'first_name',
                    },
                    {
                        "data": 'last_name',
                    },
                        @if(Auth::guard('crm')->user()->canAccess('add', 'wallet.transfer.transfer'))
                    {
                        "data": null,
                        "render": function (data, type, row, meta) {
                            data = '';

                            if (type === 'display') {
                                @if(Auth::guard('crm')->user()->canAccess('add', 'wallet.transfer.transfer'))
                                    data += '<a href="#" data-user-id="' + row.id + '" class="transfer-wallet-link"><i class="fa fa-share-square-o"></i>{{ $page_title }} Funds</a>';
                                @endif
                            }
                            return data;
                        }
                    }
                    @endif
                ]
            });

            $('#transfer-table').on('click', '.transfer-wallet-link', function () {
                var tr = $(this).closest('tr');
                    var form = $('#form-transfer');
                form.find('#username').text(tr.find('td:eq(0)').text());
                form.find('input[name=user_id]').val($(this).data('user-id'));
                $('#modal-transfer').modal('show');
            });
        });
    </script>

@endsection