<div class="modal fade" id="modal-add-registration" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="form-add-registration" id="form-add-registration"
                  action="{{ route('registrations.store') }}"
                  method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Registration</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="add-type-input" class="col-sm-3 control-label">Type</label>

                        <div class="col-sm-8">
                            <select id="add-type-input" class="form-control" name="type">
                                @foreach($types as $type)
                                    <option value="{{ $type }}">{{ $type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-avatar-id-input" class="col-sm-3 control-label">Avatar</label>

                        <div class="col-sm-8">
                            <img src="http://placehold.it/100x100" alt="100x100"
                                 data-src="http://placehold.it/100x100" data-alt="100x100"
                                 data-title="100x100" title="100x100" height="100"
                                 class="img-responsive avatar-placeholder">
                            <input type="hidden" name="avatar_id">
                            <br>
                            <button type="button" role="button"  class="btn btn-default select-avatar-btn">
                                <i class="fa fa-smile-o"></i> Select avatar
                            </button>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-currency-id-input" class="col-sm-3 control-label">Currency</label>

                        <div class="col-sm-8">
                            <select id="add-currency-id-input" class="form-control" name="currency_id">
                                @foreach($in_app_currencies as $currency)
                                    <option value="{{ $currency->currency_id }}">{{ $currency->currency }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="add-amount-input" class="col-sm-3 control-label">Amount</label>

                        <div class="col-sm-8">
                            <input type="number" step='0.01' class="form-control" name="amount"
                                   id="add-amount-input"
                                   placeholder="Amount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'registrations.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    @parent

    <script>
        $(function () {
            $("#modal-add-registration").on("hidden.bs.modal", function () {
                var form = $('#form-add-registration');
                var img = form.find('.avatar-placeholder');
                form.find('.select-avatar-btn').prop('disabled', false);
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-add-registration').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.prop('action');

                $.post(url, form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = url;
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>

@endsection