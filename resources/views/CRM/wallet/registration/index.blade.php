@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Wallet</li>
        <li class="active">Registration</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    @if($registrations->count())
                        <table id="registration-table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>Avatar</th>
                                <th>Currency</th>
                                <th>Amount</th>
                                @usercan('edit', 'registrations.update')
                                <th>Actions</th>
                                @endusercan
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($registrations as $registration)
                                <tr data-registration-id="{{ $registration->registration_id }}">
                                    <td data-type="{{ $registration->type }}">{{ $registration->type }}</td>
                                    <td data-avatar-id="{{ $registration->avatar_id }}"
                                        data-avatar="{{ str_replace('1024x1024', '100x100', $registration->avatar->avatar) }}"
                                        data-name="{{ $registration->avatar->name }}"
                                        title="{{ $registration->avatar->name }}">
                                        <img src="{{ str_replace('1024x1024', '100x100', $registration->avatar->avatar) }}" height="25"
                                             alt="{{ $registration->avatar->name }}">
                                    </td>
                                    <td data-currency-id="{{ $registration->currency_id }}">{{ $registration->currency->currency }}</td>
                                    <td data-amount="{{ number_format($registration->amount, 2) }}">{{ number_format($registration->amount, 2) }}</td>
                                    @usercan('edit', 'registrations.update')
                                    <td>
                                        <button type="button" onclick="setFormEditRegistration(this);"
                                                class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-edit"></i></button>
                                    </td>
                                    @endusercan
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-muted">{{ trans('validation.custom.registration.empty') }}</p>
                    @endif
                </div>
                @usercan('add', 'registrations.store')
                @if(!empty(Registration::getTypes()))
                    <div class="box-footer">
                        <button type="button" class="btn btn-success btn-xs btn-add-registration"
                                id="add-registration-btn" title="Add">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                @endif
                @endusercan
            </div>
        </div>
    </div>

    @include('CRM.wallet.registration.forms.add')
    @include('CRM.wallet.registration.forms.edit')
    @include('CRM.customization.avatar.forms.select_avatar')
@endsection

@section('style')
    @parent

    <style>
        .avatars .caption p {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        .avatars img {
            height: 100px;
        }

        #modal-select-avatar .modal-dialog {
            overflow-y: initial !important
        }

        #modal-select-avatar .modal-body {
            height: 520px;
            overflow-y: auto;
        }

        #modal-select-avatar .modal-footer div {
            padding: 0;
        }

        #modal-select-avatar .modal-footer div .pagination {
            margin: 0;
        }
    </style>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/params.js") }}"></script>

    <script>
        var setFormEditRegistration = function (src) {
            var form = $('#form-edit-registration');
            var tr = $(src).closest('tr');
            form.attr('data-registration-id', tr.data('registration-id'));
            form.find('#edit-type-static').text(tr.find('td:eq(0)').data('type'));
            form.find('input[name=type]').val(tr.find('td:eq(0)').data('type')).trigger('change');
            form.find('input[name=amount]').val(tr.find('td:eq(3)').data('amount'));
            form.find('select[name=currency_id]').val(tr.find('td:eq(2)').data('currency-id')).trigger('change');
            form.find('input[name=avatar_id]').val(tr.find('td:eq(1)').data('avatar-id'));
            form.find('.avatar-placeholder').prop('src', tr.find('td:eq(1)').data('avatar'));
            form.find('.avatar-placeholder').prop('alt', tr.find('td:eq(1)').data('name'));
            form.find('.avatar-placeholder').prop('title', tr.find('td:eq(1)').data('name'));
            var url = '{{ route('customization.avatars.json') }}?per_page=' + $('#avatars-per-page').val() + "&type=" + tr.find('td:eq(0)').data('type') + "&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');

            getAvatars(url, form.get(0)).done(function () {
                $('#modal-edit-registration').modal('show');
            });
        };

        function getAvatars(url, src) {
            $('#current-url').val(url);
            var avatars = $('#modal-select-avatar').find('.avatars');

            return $.getJSON(url, function (response) {
                if (response.status == '{{ config('response.type.success') }}') {
                    avatars.html('<p class="text-center">' + '{!! trans('loading.processing') !!}' + '</p>');
                    avatars.data('form-src', $(src).prop('id'));
                    avatars.html(response.data.avatars.html);
                    $('#modal-select-avatar').find('.avatar-links').html(response.data.avatars.links);
                }
            });
        }

        $(function () {
            $('#add-registration-btn').on('click', function () {
                var url = '{{ route('customization.avatars.json') }}?per_page=' + $('#avatars-per-page').val() + "&type=" + $('#add-type-input').val() + "&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');

                getAvatars(url, $('#form-add-registration').get(0)).done(function () {
                    $('#modal-add-registration').modal('show');
                });
            });

            $('#edit-type-input').on('change', function () {
                var form = $(this).closest('form');
                var img = form.find('.avatar-placeholder');
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                getAvatars(setParam($('#current-url').val(), 'type', $(this).val()), form.get(0));
            });

            $('#add-type-input').on('change', function () {
                var form = $(this).closest('form');
                var img = form.find('.avatar-placeholder');
                img.prop('src', img.data('src'));
                img.prop('alt', img.data('alt'));
                img.prop('title', img.data('title'));
                getAvatars(setParam($('#current-url').val(), 'type', $(this).val()), form.get(0));
            });

            $('.avatars').on('click', '.avatar-entry', function (e) {
                e.preventDefault();
                var form = $('#' + $(this).closest('.avatars').data('form-src'));
                form.find('input[name=avatar_id]').val($(this).data('avatar-id'));
                form.find('.avatar-placeholder').prop('src', $(this).data('avatar'));
                form.find('.avatar-placeholder').prop('alt', $(this).data('name'));
                form.find('.avatar-placeholder').prop('title', $(this).data('name'));
                $('#modal-select-avatar').modal('hide');
            });

            $('.select-avatar-btn').on('click', function () {
                $('#modal-select-avatar').modal('show');
            });

            $("#avatars-search").on("keyup", function () {
                var form = $('#' + $(this).parent().parent().parent().parent().find('.avatars').data('form-src'));
                var type = null;

                if(form.find('select[name=type]').length) {
                    type = form.find('select[name=type]');
                } else {
                    type = form.find('input[name=type]');
                }
                var url = '{{ route('customization.avatars.json') }}?search=' + $(this).val() + '&per_page=' + $('#avatars-per-page').val() + "&type=" + type.val() + "&readonly=true&status_id={{ $active_id }}" + '&except=' + encodeURIComponent('{{ $excluded_avatar_ids }}');
                getAvatars(url, form.get(0));
            });

            $('.avatar-links').on('click', '.pagination a', function (e) {
                e.preventDefault();
                var form = $('#' + $(this).parent().parent().parent().parent().parent().find('.avatars').data('form-src'));
                var type = null;

                if(form.find('select[name=type]').length) {
                    type = form.find('select[name=type]');
                } else {
                    type = form.find('input[name=type]');
                }
                var url_lvl1 = setParam($(this).prop('href'), 'per_page', $('#avatars-per-page').val());
                var url_lvl2 = setParam(url_lvl1, 'type', type.val());
                getAvatars(url_lvl2, form.get(0));
            });

            $("#avatars-per-page").on("change", function () {
                var form = $('#' + $(this).parent().parent().parent().parent().find('.avatars').data('form-src'));
                var url_lvl1 = setParam($('#current-url').val(), 'per_page', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2, form.get(0));
            });
        });
    </script>
@endsection