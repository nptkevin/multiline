<div id="modal-update-transaction" class="modal fade" role="dialog" data-backdrop="static"
     data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong>NOTES</strong></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" name="formupdatenotes" id="formupdatenotes" action=""
                      method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea id="notes" class="form-control" name="notes"> </textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                @usercan('edit', 'transactions.updateNotes')
                <button type="submit" class="btn btn-success" onclick="formupdatenotes.submit()"><i
                            class="fa fa-paper-plane-o" aria-hidden="true"></i> Submit
                </button>
                @endusercan
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>