@extends('CRM.layouts.dashboard')

@section('style')
    <style>
        #tbl-transaction {
            table-layout: fixed;
        }

        #tbl-transaction td {
            text-overflow: ellipsis;
        }

        textarea#notes {
            resize: vertical;
        }

        #alert {
            color: #155724 !important;
            background-color: #d4edda !important;
            border-color: #c3e6cb !important;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transactions</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        @if (session('msg'))
            <div class="col-xs-12">
                <div id="alert" class="alert alert-success fade in alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>{{ session('msg') }} </strong>
                </div>
            </div>
        @endif

        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="tbl-transaction" class="table table-striped table-bordered" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Username</th>
                            <th>Status</th>
                            @usercan('edit', 'transactions.updateNotes')
                            <th>Actions</th>
                            @endusercan
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>{{ $transaction->transaction_id}} </td>
                                <td>{{ $transaction->created_at}} </td>
                                <td>{{ isset($transaction->transaction_detail->charge->charge_type->name) ? $transaction->transaction_detail->charge->charge_type->name : "N/A" }} </td>
                                <td>{{ isset($transaction->transaction_detail->charge->description) ? $transaction->transaction_detail->charge->description : "N/A" }} </td>
                                <td>{{ isset($transaction->transaction_detail->price) ? $transaction->transaction_detail->charge->currency->currency_symbol . ' ' . number_format($transaction->transaction_detail->price, 2) : "N/A" }} </td>
                                <td>{{ isset($transaction->account->username) ? $transaction->account->username : "N/A" }} </td>
                                <td>{{ isset($transaction->transaction_logs->first()->transaction_status->description) ? $transaction->transaction_logs->first()->transaction_status->description : "N/A" }} </td>
                                @usercan('edit', 'transactions.updateNotes')
                                <td>
                                    <button type="submit" class="btn-xs btn-warning btn"
                                            data-toggle="modal" data-target="#modal-update-transaction"
                                            data-notes="{{ $transaction->notes }}"
                                            data-transaction_id="{{ $transaction->transaction_id }}"><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"> </i> Notes
                                    </button>
                                </td>
                                @endusercan
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('CRM.transactions.forms.notes')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $('#tbl-transaction').DataTable({
                "paging": true,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'transactions.updateNotes'))
                    {
                        targets: 7,
                        orderable: false
                    }
                    @endif
                ]
            });

            $('#modal-update-transaction').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var transaction_id = button.data('transaction_id');

                if (transaction_id) {
                    $("#formupdatenotes").attr("action", "transactions/updateNotes/" + transaction_id);
                    var transaction_note = (button.data('notes'));
                    $('#notes').text(transaction_note);
                }
            });

            @if (session('msg'))
            $("#alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#alert").alert('close');
            });
            @endif
        });
    </script>
@endsection