<div class="modal fade" id="modal-add-module" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="statusmodule" id="statusmodule" action="{{ route('status.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Status</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="status-input" class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="status_type" value="{{$type_selected}}">
                                <input type="text" class="form-control" id="controller-input" name="status_name"
                                       placeholder="Status">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'status.store')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary" id="savestatus">Save changes</button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>