@extends('CRM.layouts.dashboard')

@section('style')
    <style type="text/css">
        #news-table tbody td{
            vertical-align: top;
        }
            #news-table tbody td:first-child{
                vertical-align: middle !important;
                width: 1% !important;
            }
            #news-table tbody td:nth-child(2){
                width: 20% !important;
            }
            #news-table tbody td:nth-child(3){
                width: 25% !important;

                color: #AEAEAE;
            }
            #news-table tbody td:nth-child(3) h5{
                color: #444444;
            }
            #news-table tbody td:nth-child(4){
                width: 25% !important;
            }
        #news-table .dropdown-menu{
            left: 35px;
            margin-top: -35px;
        }

        #news-table .datatable-img-coverphoto{
            max-width: 400px;
            width: 400px;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Products / Promotions</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="news-table" class="table">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Cover</th>
                            <th>Title</th>
                            <th>Excerpt</th>
                            <th>Status</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                {{-- @usercan('add', 'contents.store') --}}
                <div class="box-footer">
                    <a href="{{ route('crm.' . $content_type . '.create') }}" class="btn btn-success btn-xs" id="btn-add-content" title="Add">
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
                {{-- @endusercan --}}
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            $('#news-table').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [[2, "desc"]],
                "ajax": "{{ route('crm.' . $content_type . '.datatable') }}",
                columnDefs: [
                    {
                        targets: [0, 1, 3],
                        orderable: false
                    },
                    {
                        targets: [0, 1, 3],
                        searchable: false
                    }
                ],
                "columns": [
                    {
                        "data": "news_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = `<a href="#" target="_" class="btn btn-default"><i class="fa fa-external-link"></i> &nbsp; Preview</a>`;
                                data = `<div class="dropdown">
                                            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>

                                            <ul class="dropdown-menu">`;

                                if(row.status.status_name == "Active")
                                    data += `<li><a href="../news/` + row.link.replace(/\\/g,'/').replace( /.*\//, '' ) + `" target="_blank"><i class="fa fa-external-link"></i> &nbsp; Preview</a></li>
                                                <li class="divider"></li>`;

                                data += `<li><a href="/admin/{{ $content_type }}/get{{ $content_type }}/` + row.news_id +  `"><i class="fa fa-pencil"></i> &nbsp; Edit</a></li>
                                                <li><a href="#" onclick="deleteContent(this);" data-news-id="` + row.news_id +  `"><i class="fa fa-trash-o"></i> &nbsp; Delete</a></li>
                                            </ul>
                                        </div>`;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "coverphoto",
                        "render": function(data, type, row, meta){
                            if(type === "display")
                                data = '<img src="' + row.coverphoto + '" class="datatable-img-coverphoto">';

                            return data;
                        }
                    },
                    {
                        "data": "title",
                        "render": function(data, type, row, meta){
                            if(type === "display")
                                data = row.title;

                            return data;
                        }
                    },
                    {
                        "data": "content",
                        "render": function(data, type, row, meta){
                            if(type === 'display')
                                data = row.content.replace(/<[^>]*>/g, '').substr(0, 200) + (row.content.length > 200 ? "..." : "");

                            return data;
                        }
                    },
                    {
                        "data": "status_id",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.status.status_name;
                            }
                            return data;
                        }
                    },
                    {
                        "data": "created_at",
                        "render": function (data, type, row, meta) {
                            if (type === 'display') {
                                data = row.created_at;
                            }
                            return data;
                        }
                    },
                ]
            });
        });

        function deleteContent(src) {
            if ($(src).data('default')) {
                swal({
                    title: '{{ trans('swal.content.delete.default.title') }}',
                    html: '{{ trans('swal.content.delete.default.html') }}',
                    type: '{{ trans('swal.content.delete.default.type') }}'
                });
                return;
            }

            swal({
                title: '{{ trans('swal.content.delete.confirm.title') }}',
                html: '{{ trans('swal.content.delete.confirm.html') }}',
                type: '{{ trans('swal.content.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.content.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "POST",
                        url: "/admin/{{ $content_type }}/delete/" + $(src).data('news-id'),
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.content.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.content.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.content.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection
