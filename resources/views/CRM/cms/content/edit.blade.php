@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Products / Promotions</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <form name="form-edit-content" id="form-edit-content" action="{{ route('crm.' . $content_type . '.update') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <input type="hidden" name="news_id" value="{{ $news->news_id }}">
                        <input type="hidden" name="content_type" value="{{ ucfirst($content_type) }}">

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="control-label">Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title', $news->title) }}">
                            <small class="text-muted"><i class="fa fa-link"></i> &nbsp; News URL: </small> &nbsp; <code class="news-url">{{ $_SERVER['HTTP_HOST'] }}/{{ $content_type }}/<span id="news-url-editable">{{ preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $news->title)) }}</span></code>

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('coverphoto') ? ' has-error' : '' }}">
                            <img src="{{ $news->coverphoto }}" height="100">
                            <br />
                            <label for="coverphoto" class="control-label">Cover Photo</label>
                            <input type="file" class="form-control" id="coverphoto" name="coverphoto" placeholder="Cover Photo" value="{{ old('coverphoto', $news->coverphoto) }}" accept="image/*">
                            <small class="text-muted"><em>Image Dimension: 1170 x 383 &nbsp; | &nbsp; Maximum File size: 2 MB</em></small>

                            @if ($errors->has('coverphoto'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('coverphoto') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                            <label for="status_id" class="control-label">Status</label>
                            <select name="status_id" class="form-control" id="status_id">
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}"{{ $status['status_id'] == old('status_id', $news->status_id) ? ' selected' : '' }}>{{ $status->status_name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('status_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status_id') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="control-label">Content</label>
                            <textarea name="content" id="content" class="form-control" cols="30" rows="10">{{ old('content', $news->content) }}</textarea>

                            @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="hidden" name="dir" id="dir" value="{{ $dir }}">

                        <a href="{{ route('crm.' . $content_type . '.index') }}" class="btn btn-default">
                            <i class="fa fa-long-arrow-left"></i> &nbsp; Back to {{ ucfirst($content_type) }} List
                        </a>

                        @usercan('edit', 'contents.update')
                            <button type="button" role="button" data-loading-text='{{ trans('loading.please_wait') }}' class="btn btn-primary" id="edit-content-submit-button">
                                <i class="fa fa-save"></i> &nbsp; Save
                            </button>
                        @endusercan
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- TinyMCE -->
    <script src="{{ asset("CRM/Capital7-1.0.0/plugins/tinymce/js/tinymce/tinymce.js") }}"></script>
@endsection

@section('script')
    <script>
        $(function () {
            tinymce.init({
                selector: 'textarea#content',
                theme: 'modern',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                menubar: 'edit view insert format table',
                toolbar: 'insertfile undo redo | styleselect | bold italic sizeselect fontselect fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
                relative_urls: false,
                remove_script_host: false,
                convert_urls: true,
                images_upload_handler: function (blobInfo, success, failure) {
                    var xhr, formData;

                    xhr = new XMLHttpRequest();
                    xhr.withCredentials = false;
                    xhr.open('POST', '{{ route('crm.' . $content_type . '.upload') }}');

                    xhr.onload = function() {
                        var response;
                        response = JSON.parse(xhr.responseText);

                        if (response.status == '{{ config('response.type.success') }}') {
                            tinymce.activeEditor.insertContent('<img src="../../../' + response.data + '"/>');
                            tinymce.activeEditor.windowManager.close();
                            // success(response.data);
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            var err = '';

                            $.each(response.errors.content_image, function (k, v) {
                                err += v + '\n';
                            });
                            failure(err);
                            return;
                        }
                    };
                    formData = new FormData();
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('dir', '{{ $news->news_id }}');
                    formData.append('content_image', blobInfo.blob(), blobInfo.filename());
                    xhr.send(formData);
                }
            });

            $('#edit-content-submit-button').on('click', function () {
                $(this).button('loading');
                var new_default_val = $('#default').val();
                var old_default_val = $('#default').data('old-default');

                if (old_default_val == 1 && new_default_val != old_default_val) {
                    swal({
                        title: '{{ trans('swal.content.set_as_default_error.title') }}',
                        html: '{!! trans('swal.content.set_as_default_error.html') !!}',
                        type: '{{ trans('swal.content.set_as_default_error.type') }}'
                    });
                    $(this).button('reset');
                    return;
                }

                $('span#news-url-editable').text($('input#title').val().replace(/ /g, '-').replace(/[^A-Za-z0-9\-]/g, ''));
                $('#form-edit-content').submit();
            });

            $('input[name=default]').on('change', function () {
                if ($(this).is(':checked')) {
                    $(this).val(1);
                } else {
                    $(this).val(0);
                }
            });
        });
    </script>
@endsection
