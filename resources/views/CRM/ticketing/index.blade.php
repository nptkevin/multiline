@extends('CRM.layouts.dashboard')

@section('styles')
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">

    <style>
		.dataTables_wrapper .dataTables_paginate .paginate_button{
			margin: 0px !important;
			padding: 0px !important;
		}
			.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
				background: none !important;
				border: 1px solid #EEEEEE !important;
			}
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tickets</li>
    </ol>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">All Tickets</h3>
				</div>

				<div class="box-body">
					<table id="tickets-table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><i class="ion ion-search"></i> &nbsp; Ticket ID</th>
								{{-- <th>Value</th> --}}
								{{-- <th>Amount</th> --}}
								<th><i class="ion ion-search"></i> &nbsp; Status</th>
								<th>Date Created</th>
								<th><i class="ion ion-search"></i> &nbsp; Created By</th>
								<th>Expiration Date</th>
								<th>Actions</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/datatables-ellipsis.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function(){
			$('#tickets-table').DataTable({
				"processing": true,
				"serverSide": true,
				"stateSave": true,
				"responsive": true,
				"ajax": {
					"url": "{{ route('ticketing.datatable') }}",
					"error": function($err){
						if($err.status == 401){
							swal('Session Expired!', 'Please login again.', 'error').then($result => {
								window.location.href = '/admin/login';
							});
						}
					}
				},
				columnDefs: [
					{ targets: 5, orderable: false }
				],
				"columns": [
					{
						"data": "ticket_generated_id",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.ticket_generated_id ? 'N/A' : row.ticket_generated_id + ' <span class="badge pull-right">' + row.subticketcount.length + '</span>';

							return data;
						}
					},
					{
						"data": "ticket_status",
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = !row.ticket_status ? 'N/A' : row.status.status_name;

							return data;
						}
					},
					{
						"data": "expire_at",
						"render": function(data, type, row, meta){
							if(type === 'display')
								data = !row.expire_at ? 'N/A' : row.expire_at;

							return data;
						}
					},
					{
						"data": "user_id",
						"render": function(data, type, row, meta){
						if(type === 'display')
							$wallet = !row.wallet ? ' [-- Err: No wallet --]' : ' [' + row.wallet.currency.currency_symbol + ' : ' + row.wallet.wallet_account_number + ']';
							data = !row.user_id ? 'N/A' : row.trader_account.username + $wallet;

							return data;
						}
					},
					{ "data": "created_at" },
					{
						"data": null,
						"render": function (data, type, row, meta){
							if(type === 'display')
								data = '<a href="/admin/ticketing/details/' + row.trademarket_ticket_id + '"><i class="fa fa-link"></i> &nbsp; View</a>';

							return data;
						}
					}
				]
			});
        });
    </script>
@endsection