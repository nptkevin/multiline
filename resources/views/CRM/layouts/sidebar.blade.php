<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ asset((Auth::guard('crm')->user()->image) ? Auth::guard('crm')->user()->image : 'CRM/Capital7-1.0.0/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
			</div>

			<div class="pull-left info">
				<p>{{ Auth::guard('crm')->user()->firstname }} {{ Auth::guard('crm')->user()->lastname }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>

		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MENU</li>
			@usercan('view', 'crm.dashboard.index')
				<li class="@isset($dashboard_menu) active @endisset">
					<a href="/admin/dashboard">
						<i class="fa fa-dashboard" aria-hidden="true"></i>
						<span>Dashboard</span>
					</a>
				</li>
			@endusercan

			@usercan('view', 'accounts.index')
				<li class="@isset($accounts_menu) active @endisset">
					<a href="/admin/accounts">
						<i class="fa fa-user" aria-hidden="true"></i>
						<span>Accounts</span>
					</a>
				</li>
			@endusercan

			<li class="treeview @isset($wallet_menu) active menu-open @endisset" id="wallet-treeview">
				<a href="javascript:void(0);">
					<i class="fa fa-id-card-o" aria-hidden="true"></i>
					<span>Wallet</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					@usercan('view', 'currencies.index')
					<li class=" @isset($currency_menu) active @endisset ">
						<a href="/admin/wallet/currencies">
							<i class="fa fa-usd" aria-hidden="true"></i>
							<span>Currency</span>
						</a>
					</li>
					@endusercan

					@usercan('view', 'wallet.transfer.transfer')
					<li class=" @isset($deposit_menu) active @endisset ">
						<a href="/admin/wallet/transfer?type=Deposit">
							<i class="fa fa-share-square-o" aria-hidden="true"></i>
							<span>Deposit</span>
						</a>
					</li>
					@endusercan

					@usercan('view', 'wallet.transfer.transfer')
					<li class=" @isset($withdraw_menu) active @endisset ">
						<a href="/admin/wallet/transfer?type=Withdraw">
							<i class="fa fa-share-square-o" aria-hidden="true"></i>
							<span>Withdraw</span>
						</a>
					</li>
					@endusercan

				</ul>
			</li>

			{{--
			@usercan('view', 'status.index')
				<li class="@isset($status_menu) active @endisset temp-hide">
					<a href="/admin/status">
						<i class="fa fa-check" aria-hidden="true"></i>
						<span>Status</span>
					</a>
				</li>
			@endusercan
			--}}

			{{--@if(Auth::guard('crm')->user()->canAccess('view', 'roles.index') || Auth::guard('crm')->user()->canAccess('view', 'permissions.index') || Auth::guard('crm')->user()->canAccess('view', 'assignuser.index'))--}}
				{{--<li class="treeview @isset($security_menu) active menu-open @endisset" id="security-treeview">--}}
					{{--<a href="javascript:void(0);">--}}
						{{--<i class="fa fa-lock" aria-hidden="true"></i>--}}
						{{--<span>Security</span>--}}
						{{--<i class="fa fa-angle-left pull-right"></i>--}}
					{{--</a>--}}
					{{--<ul class="treeview-menu">--}}
						{{--@usercan('view', 'roles.index')--}}
						{{--<li class="@isset($roles_menu) active @endisset">--}}
							{{--<a href="/admin/security/roles">--}}
								{{--<i class="fa fa-cogs" aria-hidden="true"></i>--}}
								{{--<span>Modules</span>--}}
							{{--</a>--}}
						{{--</li>--}}
						{{--@endusercan--}}

			{{--
            <li class="treeview @isset($wallet_menu) active menu-open @endisset" id="wallet-treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-id-card-o" aria-hidden="true"></i>
                    <span>Wallet</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    @usercan('view', 'wallet.transfer.transfer')
                    <li class=" @isset($deposit_menu) active @endisset ">
                        <a href="/admin/wallet/transfer?type=Deposit">
                            <i class="fa fa-share-square-o" aria-hidden="true"></i>
                            <span>Deposit</span>
                        </a>
                    </li>
                    @endusercan

					@usercan('view', 'wallet.transfer.transfer')
					<li class=" @isset($withdraw_menu) active @endisset ">
						<a href="/admin/wallet/transfer?type=Withdraw">
							<i class="fa fa-share-square-o" aria-hidden="true"></i>
							<span>Withdraw</span>
						</a>
					</li>
					@endusercan

					@usercan('view', 'wallet.transfer.transfer')
					<li class=" @isset($redeem_menu) active @endisset ">
						<a href="#/admin/wallet/transfer?type=Redeem">
							<i class="fa fa-share-square-o" aria-hidden="true"></i>
							<span>Redeem</span>
						</a>
					</li>
					@endusercan

                </ul>
            </li>
            --}}

            {{--@if(Auth::guard('crm')->user()->canAccess('view', 'roles.index') || Auth::guard('crm')->user()->canAccess('view', 'permissions.index') || Auth::guard('crm')->user()->canAccess('view', 'assignuser.index'))--}}
                {{--<li class="treeview @isset($security_menu) active menu-open @endisset" id="security-treeview">--}}
                    {{--<a href="javascript:void(0);">--}}
                        {{--<i class="fa fa-lock" aria-hidden="true"></i>--}}
                        {{--<span>Security</span>--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--@usercan('view', 'roles.index')--}}
                        {{--<li class="@isset($roles_menu) active @endisset">--}}
                            {{--<a href="/admin/security/roles">--}}
                                {{--<i class="fa fa-cogs" aria-hidden="true"></i>--}}
                                {{--<span>Modules</span>--}}
                            {{--</a>--}}
                        {{--</li>--}}
                        {{--@endusercan--}}

						{{--@usercan('view', 'permissions.index')--}}
						{{--<li class="@isset($permission_menu) active @endisset">--}}
							{{--<a href="/admin/security/permissions">--}}
								{{--<i class="fa fa-user-secret" aria-hidden="true"></i>--}}
								{{--<span>Roles</span>--}}
							{{--</a>--}}
						{{--</li>--}}
						{{--@endusercan--}}

						{{--@usercan('view', 'assignuser.index')--}}
						{{--<li class="@isset($assignuser_menu) active @endisset">--}}
							{{--<a href="/admin/security/assignuser">--}}
								{{--<i class="fa fa-user-o" aria-hidden="true"></i>--}}
								{{--<span>Users</span>--}}
							{{--</a>--}}
						{{--</li>--}}
						{{--@endusercan--}}
					{{--</ul>--}}
				{{--</li>--}}
			{{--@endif--}}

			{{--
			<li class="treeview @isset($cms_menu) active menu-open @endisset" id="cms-treeview">
                <a href="javascript:void(0);">
                    <i class="fa fa-id-card-o" aria-hidden="true"></i>
                    <span>CMS</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
					@usercan('view', 'crm.products.index')
						<li class="@isset($products_menu) active @endisset">
							<a href="{{ route('crm.products.index') }}">
								<i class="ion ion-bag" aria-hidden="true"></i>
								<span>Products</span>
							</a>
						</li>
					@endusercan

					@usercan('view', 'crm.promotions.index')
						<li class="@isset($promotions_menu) active @endisset">
							<a href="{{ route('crm.promotions.index') }}">
								<i class="ion ion-speakerphone" aria-hidden="true"></i>
								<span>Promotions</span>
							</a>
						</li>
					@endusercan
				</ul>
			</li>
			--}}
		</ul>
	</section>
</aside>
