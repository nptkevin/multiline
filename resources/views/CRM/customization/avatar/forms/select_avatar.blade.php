<div class="modal fade" id="modal-select-avatar" data-backdrop="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-4 form-inline">
                        <span>Show</span>
                        <select name="per_page" id="avatars-per-page" class="form-control input-sm">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>entries</span>
                        <input type="hidden" id="current-url">
                    </div>
                    <div class="col-md-8 form-inline text-right">
                        <span>Search: </span>
                        <input type="text" name="search" id="avatars-search" class="form-control input-sm">
                    </div>
                    <input type="hidden" id="current-url">
                </div>
            </div>
            <div class="modal-body">
                <div class="row avatars"></div>
            </div>
            <div class="modal-footer">
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                <div class="col-md-6 text-right avatar-links"></div>
            </div>
        </div>
    </div>
</div>