@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Customization</li>
        <li class="active">Avatar</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                @usercan('add', 'avatars.store')
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-6 form-inline">
                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                    data-target="#modal-add-avatar" title="Add">
                                <i class="fa fa-plus"></i>
                            </button>

                            <span class="ml">Show</span>
                            <select name="per_page" id="avatars-per-page" class="form-control input-sm">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>entries</span>

                            <span class="ml">Type: </span>
                            <select name="type" id="avatars-type" class="form-control input-sm">
                                <option value="" selected>All</option>
                                @foreach($types as $type)
                                    <option value="{{ $type }}">{{ $type }}</option>
                                @endforeach
                            </select>

                            <span class="ml">Status: </span>
                            <select name="type" id="avatars-status-id" class="form-control input-sm">
                                <option value="" selected>All</option>
                                @foreach($statuses as $status)
                                    <option value="{{ $status->status_id }}">{{ $status->status_name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" id="current-url">
                        </div>
                        <div class="col-md-6 form-inline text-right">
                            <span>Search: </span>
                            <input type="text" name="search" id="avatars-search" class="form-control input-sm">
                        </div>
                    </div>
                </div>
                @endusercan
                <div class="box-body">
                    <div class="row avatars"></div>
                </div>
                <div class="box-footer avatar-links text-center"></div>
            </div>
        </div>
    </div>

    @include('CRM.customization.avatar.forms.add')
    @include('CRM.customization.avatar.forms.edit')
@endsection

@section('style')
    @parent

    <style>
        .ml {
            padding-left: 10px;
        }

        .avatars {
            padding: 20px 20px 0px 20px;
        }

        .avatar-links {
            padding-top: 0;
            padding-bottom: 0;
        }

        .avatars .caption p {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }

        #avatars-per-page, #avatars-type {
            width: auto;
        }
    </style>
@endsection

@section('script')
    @parent

    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/js/params.js") }}"></script>

    <script>
        function editAvatar(src) {
            var a = $(src);
            var form = $('#form-edit-avatar');
            form.attr('data-avatar-id', a.data('avatar-id'));
            form.find('input[name=name]').val(a.data('name'));
            form.find('select[name=type]').val(a.data('type')).trigger('change');
            form.find('select[name=application_id]').val(a.data('application-id')).trigger('change');
            form.find('select[name=status_id]').val(a.data('status-id')).trigger('change');
            $('#modal-edit-avatar').modal('show');
        }

        function getAvatars(url) {
            $('#current-url').val(url);

            $.getJSON(url, function () {
                $('.avatars').html('<p class="text-center">' + '{!! trans('loading.processing') !!}' + '</p>');
            }).done(function (response) {
                $('.avatars').html(response.data.avatars.html);
                $('.avatar-links').html(response.data.avatars.links);
            });
        }

        $(function () {
            getAvatars('{{ route('customization.avatars.json') }}?per_page=' + $('#avatars-per-page').val() + '&readonly=false');

            $('.avatar-links').on('click', '.pagination a', function (e) {
                e.preventDefault();
                var url_lvl1 = setParam($(this).prop('href'), 'per_page', $('#avatars-per-page').val());
                var url_lvl2 = setParam(url_lvl1, 'type', $('#avatars-type').val());
                var url_lvl3 = setParam(url_lvl2, 'status_id', $('#avatars-status-id').val());
                getAvatars(url_lvl3);
            });

            $("#avatars-per-page").on("change", function () {
                var url_lvl1 = setParam($('#current-url').val(), 'per_page', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $("#avatars-status-id").on("change", function () {
                var url_lvl1 = setParam($('#current-url').val(), 'status_id', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $("#avatars-type").on("change", function () {
                var url_lvl1 = setParam($('#current-url').val(), 'type', $(this).val());
                var url_lvl2 = setParam(url_lvl1, 'page', 1);
                getAvatars(url_lvl2);
            });

            $("#avatars-search").on("keyup", function () {
                getAvatars('{{ route('customization.avatars.json') }}?search=' + $(this).val() + '&per_page=' + $('#avatars-per-page').val() + "&type=" + $('#avatars-type').val() + "&status_id=" + $('#avatars-status-id').val() + '&readonly=false');
            });

            $("#modal-add-avatar").on("hidden.bs.modal", function () {
                var form = $('#form-add-avatar');
                form.trigger('reset');
                clearErr(form);
            });

            $("#modal-edit-avatar").on("hidden.bs.modal", function () {
                var form = $('#form-edit-avatar');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-avatar').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.data('temp-action') + '/' + form.data('avatar-id');

                $.ajax({
                    type: form.prop('method'),
                    url: url,
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        } else if (response.status == '{{ config('response.type.fail') }}') {
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('#form-add-avatar').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        } else if (response.status == '{{ config('response.type.fail') }}') {
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>
@endsection

