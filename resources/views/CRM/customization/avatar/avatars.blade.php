@forelse($avatars as $avatar)
    @if(!$readonly)
        <div class="col-xs-2 col-md-1">
    @else
        <div class="col-xs-4 col-sm-3 col-md-2">
    @endif
        <div class="thumbnail avatar-entry" data-avatar-id="{{ $avatar->avatar_id }}"
             data-name="{{ $avatar->name }}"
             data-avatar="{{ str_replace('1024x1024', '100x100', $avatar->avatar) }}">
            <a href="{{ $avatar->avatar }}">
                <img src="{{ str_replace('1024x1024', '100x100', $avatar->avatar) }}"
                     class="img-responsive"
                     alt="{{ $avatar->name }}">
            </a>
            <div class="caption">
                <p title="{{ $avatar->name }}">{{ $avatar->name }}</p>

                @if(!$readonly)
                    @usercan('edit', 'avatars.update')
                    <a href="#" class="btn btn-warning btn-xs"
                       onclick="editAvatar(this);" data-avatar-id="{{ $avatar->avatar_id }}"
                       data-name="{{ $avatar->name }}"
                       data-type="{{ $avatar->type }}"
                       data-application-id="{{ $avatar->application_id }}"
                       data-status-id="{{ $avatar->status_id }}"
                       role="button" title="Edit"><i class="fa fa-edit"></i></a>
                    @endusercan

                    @if($avatar->status->status_name == 'Active')
                        <span class="text-success pull-right">
                            <i class="fa fa-check-circle" title="Active"></i>
                        </span>
                    @else
                        <span class="text-danger pull-right">
                            <i class="fa fa-times-circle" title="Inactive"></i>
                        </span>
                    @endif
                @endif
            </div>
        </div>
    </div>
@empty
    @if(!$readonly)
        <p class="text-muted">{{ trans('validation.custom.avatar.empty') }}</p>
    @else
        <p class="text-muted text-center">{{ trans('validation.custom.avatar.empty') }}</p>
    @endif
@endforelse