@extends('CRM.layouts.dashboard')

@section('style')
    <style>
        .tb-ck {
            width: 7%;
            text-align: center;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Security</li>
        <li class="active">Roles</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-6'>
            <!-- Box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registered Modules</h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">

                    <table id="permission_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Role</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr data-role-id="{{ $permission->role_id }}">
                                <td>{{ $permission->role_name }}</td>
                                <td>{{ $permission->description }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('add', 'permissions.add')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                            data-target="#modal-add-module" title="Add"><i
                                class="fa fa-plus"></i></button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->

        <div class='col-md-6'>
            <!-- Box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Customize Permission: <span id="role_name"></span></h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <table id="module_table" class="table table-hover table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="tb-ck">
                                {{--<input type="checkbox" class="flat-green">--}}
                            </th>
                            <th>Module Name</th>
                            <th class="tb-ck">View</th>
                            <th class="tb-ck">Add</th>
                            <th class="tb-ck">Edit</th>
                            <th class="tb-ck">Delete</th>
                            <th class="tb-ck">Admin</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $role)
                            <tr id="module_tr_{{ $role->module_id }}">
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" class="flat-green module_selector"
                                                   data-moduleid="{{ $role->module_id }}"
                                                   id="module_select_{{ $role->module_id }}">
                                        </label>
                                    </div>
                                </td>
                                <td>{{ $role->module_name }}/{{ $role->controller_name }}</td>
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" class="flat-blue module-perm"
                                                   id="module_view_{{ $role->module_id }}" value="on">
                                        </label>
                                    </div>
                                </td>
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" id="module_add_{{ $role->module_id }}"
                                                   class="flat-blue module-perm" value="on">
                                        </label>
                                    </div>
                                </td>
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" id="module_edit_{{ $role->module_id }}"
                                                   class="flat-blue module-perm" value="on">
                                        </label>
                                    </div>
                                </td>
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" id="module_del_{{ $role->module_id }}"
                                                   class="flat-blue module-perm" value="on">
                                        </label>
                                    </div>
                                </td>
                                <td class="tb-ck">
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" class="flat-blue module-admin" value="on"
                                                   id="module_admin_{{ $role->module_id }}">
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('edit', 'permissions.edit')
                <div class="box-footer">
                    <button type="button" class="btn btn-primary" id="save_permissions"
                            data-loading-text='{{ trans('loading.please_wait') }}'>Save
                    </button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.security.permissions.forms.add')
@endsection

@section('styles')
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/plugins/iCheck/all.css") }}">

    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/plugins/iCheck/icheck.min.js") }}"></script>

    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>
    <script src="{{ asset("CRM/Capital7-1.0.0/plugins/jquery/jquery.query-object.js") }}"></script>

    <script>
        $(function () {
            var permission_table = $('#permission_table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                "order": [
                    [0, "asc"]
                ]
            });

            $('#module_table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                "columnDefs": [
                    {
                        "targets": [0, 2, 3, 4, 5, 6],
                        "orderable": false
                    }
                ],
                "order": [
                    [1, "asc"]
                ]
            });

            $('input[type="checkbox"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
            });

            $('input[type="checkbox"].flat-green').iCheck({
                checkboxClass: 'icheckbox_flat-green',
            });

            $('input[type="checkbox"].flat-blue').on('ifChecked', function () {
                var tr = $(this).closest('tr');
                tr.find('.module_selector').iCheck("check");

                if (tr.find('.module-perm').filter(':checked').length == 4) {
                    tr.find('.module-admin').iCheck("check");
                }
            });

            $('input[type="checkbox"].flat-blue').on('ifUnchecked', function () {
                var tr = $(this).closest('tr');

                if (tr.find('.module-perm').filter(':checked').length == 0) {
                    tr.find('.module_selector').iCheck("uncheck");
                } else {
                    tr.find('.module_selector').iCheck("check");
                }

                if (tr.find('.module-perm').filter(':checked').length < 4) {
                    tr.find('.module-admin').iCheck("uncheck");
                }
            });

            $('.module-admin').on('ifChecked', function () {
                var tr = $(this).closest('tr');
                var module_id = tr.find('.module_selector').data('moduleid');
                tr.find('#module_add_' + module_id).iCheck("check");
                tr.find('#module_edit_' + module_id).iCheck("check");
                tr.find('#module_del_' + module_id).iCheck("check");
            });

            $('.module_selector').on('ifChecked', function () {
                var tr = $(this).closest('tr');
                tr.addClass('selected');
                var module_id = $(this).data('moduleid');
                $('#module_view_' + module_id).iCheck("check");
            });

            $('.module_selector').on('ifUnchecked', function () {
                var tr = $(this).closest('tr');
                tr.removeClass('selected');
                tr.find('input[type="checkbox"].flat-blue').iCheck("uncheck");
            });

            $('#permission_table').on('click', 'tr', function () {
                permission_table.$('tr').removeClass('active');
                $(this).addClass('active');
                var role_id = $(this).data('role-id');
                loadPermissions(role_id);
            });

            if (permission_table.rows().count()) {
                var index = $.query.get('role_index') ? $.query.get('role_index') : 0;
                $('#permission_table tbody tr:eq(' + index + ')').trigger('click');
            }

            function loadPermissions(role_id) {
                $('#role_name').html('{!! trans('loading.please_wait') !!}...');

                $.ajax({
                    type: 'GET',
                    url: "roles/" + role_id + "/json",
                    dataType: 'json',
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            $('input[type="checkbox"].flat-blue, input[type="checkbox"].flat-green').iCheck('uncheck');

                            $.each(response.data.permissions, function (key, permission) {
                                $('#module_tr_' + permission.module_id).attr('data-role-module-id', permission.role_module_id);
                                $('#module_add_' + permission.module_id).iCheck(permission.can_add ? "check" : "uncheck");
                                $('#module_edit_' + permission.module_id).iCheck(permission.can_edit ? "check" : "uncheck");
                                $('#module_del_' + permission.module_id).iCheck(permission.can_delete ? "check" : "uncheck");
                                $('#module_admin_' + permission.module_id).iCheck(permission.admin ? "check" : "uncheck");
                                $('#module_view_' + permission.module_id).iCheck(permission.readonly ? "check" : "uncheck");
                            });
                        }
                    }
                }).done(function (response) {
                    $('#role_name').html('<strong>' + response.data.belong_role + '</strong>');
                });
            }

            $('#save_permissions').click(function () {

                if (!$('input[type="checkbox"].flat-green').parent('.checked').length) {
                    swal({
                        title: '{{ trans('swal.permission.empty.title') }}',
                        html: '{!! trans('swal.permission.empty.html') !!}',
                        type: '{{ trans('swal.permission.empty.type') }}'
                    });
                    return;
                }
                savePermission(permission_table, true);
            });

            $("#modal-add-module").on("hidden.bs.modal", function () {
                var form = $('#formpermission');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formpermission').on('submit', function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.post(form.prop('action'), form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = '{{ route('permissions.index') }}';
                        return;
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        var savePermission = function (permission_table, warning) {
            var btn = $('#save_permissions').button('loading');
            var permission_id = permission_table.$('tr.active').data('role-id');
            var permissions = [];

            $('input[type="checkbox"].flat-green').each(function () {
                if ($(this).parent('[class*="icheckbox"]').hasClass("checked")) {
                    var module_id = $(this).data('moduleid');
                    var role_module_id = $('#module_tr_' + module_id).data('role-module-id');
                    var can_view = $('#module_view_' + module_id).parent('[class*="icheckbox"]').hasClass("checked");
                    var can_add = $('#module_add_' + module_id).parent('[class*="icheckbox"]').hasClass("checked");
                    var can_edit = $('#module_edit_' + module_id).parent('[class*="icheckbox"]').hasClass("checked");
                    var can_del = $('#module_del_' + module_id).parent('[class*="icheckbox"]').hasClass("checked");
                    var can_admin = $('#module_admin_' + module_id).parent('[class*="icheckbox"]').hasClass("checked");
                    module_attributes = {
                        "role_module_id": role_module_id,
                        "module_id": module_id,
                        "can_add": can_add,
                        "can_edit": can_edit,
                        "can_delete": can_del,
                        "admin": can_admin,
                        "readonly": can_view
                    };
                    permissions.push(module_attributes);
                }
            });

            var data = {
                "warning": warning,
                "permissions": permissions,
                "permission_id": permission_id,
                "_token": "{{ csrf_token() }}"
            };

            $.ajax({
                type: 'PUT',
                url: "{{ route('permissions.edit') }}",
                dataType: 'json',
                data: data,
                success: function (response) {
                    if (response.data.swal.type == 'success') {
                        swal(response.data.swal).then(function () {
                            location.href = '{{ route('permissions.index') }}' + '?role_index=' + permission_table.$('tr.active').index();
                        });
                    } else if (response.status == '{{ config('response.type.fail') }}') {
                        swal(response.data.swal).then(function () {
                            savePermission(permission_table, false)
                        });
                    }
                }
            }).done(function () {
                btn.button('reset');
            });
        }
    </script>
@endsection
