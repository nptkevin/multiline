<div class="modal fade" id="modal-add-module" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- form start -->
            <form class="form-horizontal" name="formpermission" id="formpermission"
                  action="{{ route('permissions.add') }}"
                  method="post">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Permission</h4>
                </div>
                <div class="modal-body">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="role-input" class="col-sm-2 control-label">Role Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="role_name-input" name="role_name"
                                       placeholder="Role name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description-input" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="description-input" name="description"
                                       placeholder="Description">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'permissions.add')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->