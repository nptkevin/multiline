@extends('CRM.layouts.dashboard')

@section('style')
    <style>
        .tb-ck {
            width: 7%;
            text-align: center;
        }

        .state {
            cursor: default;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Security</li>
        <li class="active">Modules</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Registered Modules</h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">

                    <table id="module_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Module Name/Controller</th>
                            <th>Description</th>
                            <th>Parent</th>
                            @if(Auth::guard('crm')->user()->canAccess('delete', 'roles.delete'))
                                <th class="tb-ck">Actions</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($modules as $module)
                            <tr>
                                <td>{{ $module->module_name }}/{{ $module->controller_name }}</td>
                                <td>{{ $module->module_description }}</td>
                                <td>{{ $module->parent_id == 0 || is_null($module->parent_id) ? 'None' : $module->parent()->first()->module_name . '/' . $module->parent()->first()->controller_name }}</td>
                                @if(Auth::guard('crm')->user()->canAccess('delete', 'roles.delete'))
                                    <td>
                                        @usercan('edit', 'roles.edit')
                                        <button type="button" role="button" class="btn-xs btn-warning edit_button"
                                                data-module-id="{{ $module->module_id }}"
                                                data-module-name="{{ $module->module_name }}"
                                                data-controller-name="{{ $module->controller_name }}"
                                                data-module-description="{{ $module->module_description }}"
                                                data-parent-id="{{ $module->parent_id == 0 || is_null($module->parent_id) ? 0 : $module->parent_id }}"
                                                title="Edit" onclick="editModule(this);">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                        @endusercan

                                        @usercan('delete', 'roles.delete')
                                        <button type="button" role="button" class="btn-xs btn-danger delete_button"
                                                title="Delete" onclick="deleteModule({{ $module->module_id }});">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        @endusercan
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('add', 'roles.add')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                            data-target="#modal-add-module" title="Add"><i
                                class="fa fa-plus"></i></button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->

    </div><!-- /.row -->

    @include('CRM.security.roles.forms.add')
    @include('CRM.security.roles.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $('#module_table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                "columnDefs": [
                    {
                        "targets": 3,
                        "orderable": false
                    }
                ],
                "order": [
                    [0, "asc"]
                ]
            });

            $("#modal-add-module").on("hidden.bs.modal", function () {
                var form = $('#formmodule');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formmodule').on('submit', function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.post(form.prop('action'), form.serialize(), function (response) {
                    if (response.status == '{{ config('response.type.success') }}') {
                        location.href = '{{ route('roles.index') }}';
                    } else if (response.status == '{{ config('response.type.error') }}') {
                        assocErr(response.errors, form);
                    } else if (response.status == '{{ config('response.type.fail') }}') {
                        swal(response.data.swal);
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $("#modal-edit-module").on("hidden.bs.modal", function () {
                var form = $('#form-edit-module');
                form.find('select[name=parent_id] option').show();
                form.removeAttr('data-module-id');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-module').on('submit', function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: "PUT",
                    url: "/admin/security/roles/" + $(this).data('module-id'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.href = '{{ route('roles.index') }}';
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        } else if (response.status == '{{ config('response.type.fail') }}') {
                            swal(response.data.swal);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        function deleteModule(id) {
            swal({
                title: '{{ trans('swal.module.delete.confirm.title') }}',
                html: '{{ trans('swal.module.delete.confirm.html') }}',
                type: '{{ trans('swal.module.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.module.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/admin/security/roles/delete/" + id,
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.module.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.module.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.module.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        };

        function editModule(src) {
            var form = $('#form-edit-module');
            form.attr('data-module-id', $(src).data('module-id'));
            form.find('input[name=module_name]').val($(src).data('module-name'));
            form.find('input[name=controller_name]').val($(src).data('controller-name'));
            form.find('input[name=module_description]').val($(src).data('module-description'));
            form.find('select[name=parent_id] option[value=' + $(src).data('module-id') + ']').hide();
            form.find('select[name=parent_id]').val($(src).data('parent-id'));
            $('#modal-edit-module').modal('show');
        };
    </script>
@endsection

