@extends('CRM.layouts.dashboard')

@section('style')
    <style>
        .thumbnail > img.fixed-size {
            height: auto;
            width: 50%;
        }

        a.btn.btn-danger.btn-box-tool {
            color: white;
        }

        .box.box-default {
            border: 1px solid #d2d6de;
        }

        .box.box-success.img-thumbnail {
            height: 170px;
            max-height: 170px;
        }

        .pt-30 {
            padding-top: 30px;
        }
    </style>
@endsection

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Applications</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                @usercan('add', 'applications.store')
                <div class="box-header with-border">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                            data-target="#modal-add-app" title="Add"><i
                                class="fa fa-plus"></i>
                    </button>
                </div>
                @endusercan
                <div class="box-body pt-30">
                    @foreach($applications as $application)
                        <div class="col-sm-3 col-md-3">
                            <div class="box box-success img-thumbnail">
                                <div class="box-header with-border">
                                    <h3 class="box-title">
                                        <strong style="font-size:20px;">{{ $application->name }}</strong>
                                    </h3>
                                    <div class="box-tools pull-right">
                                        @usercan('edit', 'applications.update')
                                        <a href="#" class="btn btn-warning btn-xs btn-edit-application"
                                           role="button"
                                           data-application-id="{{ $application->application_id }}"
                                           data-name="{{ $application->name }}"
                                           data-description="{{ $application->description }}"
                                           data-status-id="{{ $application->status_id }}" title="Edit">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        @endusercan

                                        @usercan('delete', 'applications.destroy')
                                        <button onclick="deleteApplication({{ $application->application_id }});"
                                                class="btn btn-danger btn-xs"
                                                role="button" title="Delete">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        @endusercan
                                    </div>
                                </div>
                                <!-- /.box-tools -->
                                <div class="box-body">
                                    <div class="col-sm-4 pull-left row">
                                        <img src="{{ asset($application->app_image_300_300) }}"
                                             alt="{{ $application->name }}" width="300"
                                             height="300" class="img-thumbnail">
                                    </div>
                                    <div class="col-sm-8">
                                        <label>
                                            {{ $application->description }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.application.forms.add')
    @include('CRM.application.forms.edit')
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $("#modal-add-app").on("hidden.bs.modal", function () {
                var form = $('#form_app');
                form.trigger('reset');
                clearErr(form);
            });

            $("#modal-edit-app").on("hidden.bs.modal", function () {
                var form = $('#form_app_edit');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form_app_edit').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');
                var url = form.data('temp-action') + '/' + form.data('application-id');

                $.ajax({
                    type: form.prop('method'),
                    url: url,
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        } else if (response.status == '{{ config('response.type.fail') }}') {
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $('.btn-edit-application').on('click', function () {
                var form = $('#form_app_edit');
                form.attr('data-application-id', $(this).data('application-id'));
                form.find('input[name=name]').val($(this).data('name'));
                form.find('input[name=description]').val($(this).data('description'));
                form.find('select[name=status_id]').val($(this).data('status-id')).trigger('change');
                $('#modal-edit-app').modal('show');
            });

            $('#form_app').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        } else if (response.status == '{{ config('response.type.fail') }}') {
                            swal({
                                title: response.data.swal.title,
                                html: response.data.swal.html,
                                type: response.data.swal.type
                            });
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        function deleteApplication(id) {
            swal({
                title: '{{ trans('swal.application.delete.confirm.title') }}',
                html: '{{ trans('swal.application.delete.confirm.html') }}',
                type: '{{ trans('swal.application.delete.confirm.type') }}',
                showCancelButton: true,
                confirmButtonText: '{{ trans('swal.application.delete.confirm.confirmButtonText') }}'
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "/applications/" + id,
                        data: {'_token': '{{ csrf_token() }}'},
                        success: function (response) {
                            if (response.status == '{{ config('response.type.success') }}') {
                                location.reload();
                            } else if (response.status == '{{ config('response.type.fail') }}') {
                                if (response.data.affected) {
                                    var html = '{{ trans('swal.application.delete.fail.html') }}';

                                    $.each(response.data.affected, function (key, val) {
                                        var str = ' ' + val.count + ' ' + val.relation;
                                        html += str.replace(/_/g, ' ');
                                    });

                                    swal({
                                        title: '{{ trans('swal.application.delete.fail.title') }}',
                                        html: html,
                                        type: '{{ trans('swal.application.delete.fail.type') }}'
                                    });
                                }
                            }
                        }
                    });
                }
            });
            return false;
        }
    </script>
@endsection