<div class="modal fade" id="modal-edit-app" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form name="form_app_edit" id="form_app_edit" data-temp-action="/applications"
                  enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Application</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name-input" class="control-label">Name</label>
                        <input type="text" class="form-control" id="name-input" name="name"
                               placeholder="Name">
                    </div>

                    <div class="form-group">
                        <label for="description-input-input" class="control-label">Description</label>
                        <input type="text" class="form-control" id="description-input"
                               name="description" placeholder="Description">
                    </div>

                    <div class="form-group">
                        <label for="image_landscape" class="control-label">Image <span class="text-muted">(Landscape: 470 x 260)</span></label>
                        <input type="file" class="form-control" id="image_landscape" name="image_landscape">
                    </div>

                    <div class="form-group">
                        <label for="image_square" class="control-label">Image <span class="text-muted">(Square: 300 x 300)</span></label>
                        <input type="file" id="image_square" class="form-control" name="image_square">
                    </div>

                    <div class="form-group">
                        <label for="status-input" class="control-label">Status</label>
                        <select id="status-input" class="form-control" name="status_id">
                            @foreach($statuses as $status)
                                <option value="{{ $status['status_id'] }}">{{ $status['status_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'applications.update')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>