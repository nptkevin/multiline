@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Security Question</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="security_question_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Question</th>
                            @if(Auth::guard('crm')->user()->canAccess('delete', 'security_question.delete') || Auth::guard('crm')->user()->canAccess('edit', 'security_question.update'))
                                <th>Actions</th>
                            @endif
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
                @usercan('add', 'security_question.create')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                            data-target="#modal-add-sec-question" title="Add">
                        <i class="fa fa-plus"></i></button>
                </div><!-- /.box-footer-->
                @endusercan
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    {{-- im not sure if this ok--}}
    <form action="" method="post" id="delete_form">
        {{ csrf_field() }}
    </form>

    @include('CRM.security_question.forms.add')
    @include('CRM.security_question.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        function deleteQuestion(id) {
            if (confirm('Are you sure you want to delete?')) {
                $("#delete_form").attr('action', 'security_question/delete/' + id);
                $("#delete_form").submit();
                return true;
            }
            return false;
        }

        $(function () {
            $('#security_question_table').DataTable({
                "processing": true,
                "serverSide": true,
                "responsive": true,
                "ajax": "/admin/security_question/list",
                "columns": [
                    {"data": "security_question"},
                        @if(Auth::guard('crm')->user()->canAccess('delete', 'security_question.delete') || Auth::guard('crm')->user()->canAccess('edit', 'security_question.update'))
                    {
                        "data": "id",
                        "render": function (data, type, row, meta) {
                            var data_b = '';

                            if (type === 'display') {

                                @if(Auth::guard('crm')->user()->canAccess('edit', 'security_question.update'))
                                    data_b += '<button class="edit-modal btn btn-warning btn-xs" data-toggle="modal" data-target="#modal-edit-sec-question" data-sec_ques="' + row.security_question + '" data-sec_ques_id="' + data + '" title="Edit"><i class="fa fa-edit"></i></button> ';
                                @endif

                                        @if(Auth::guard('crm')->user()->canAccess('delete', 'security_question.delete'))
                                    data_b += ' <button class="delete-question btn btn-danger btn-xs" onclick="javascript:return deleteQuestion(' + data + ');" title="Delete"><i class="fa fa-trash"></i></button>';
                                @endif
                            }
                            return data_b;
                        }
                    }
                    @endif
                ],
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('delete', 'security_question.delete') || Auth::guard('crm')->user()->canAccess('edit', 'security_question.update'))
                    {
                        targets: 1,
                        orderable: false
                    }
                    @endif
                ]
            });
        });

        $('#modal-edit-sec-question').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var security_question = button.data('sec_ques');
            var security_question_id = button.data('sec_ques_id');
            $('#edit_sec_ques').val(security_question);
            $("#security_question_edit_form").attr('action', '/admin/security_question/update/' + security_question_id);
        });
    </script>
@endsection