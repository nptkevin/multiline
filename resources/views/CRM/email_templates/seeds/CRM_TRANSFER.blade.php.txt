Hello Admin,

{{$sender}} has transferred {{$amount}} to {{$receiver}}.

Please login to {{$app_url}} to take action regarding this transaction.

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team