Hello Admin,

{{$name}} is requesting for withdrawal amounting {{$amount}}.

Bank information:

Currency: {{$bank_currency}}

Name: {{$bank_name}}

Code: {{$bank_code}}

Country: {{$bank_country}}

Branch: {{$bank_branch}}

Address Line 1: {{$bank_address}}

Address Line 2: {{$bank_address_2}}

Account Name: {{$bank_account_name}}

Account Number: {{$bank_account_number}}

Swift Code: {{$bank_swift}}

Request notes:

{{$notes}}

Please login to {{$app_url}} to take action regarding this request.

Note: This is auto generated email system. Do not Reply.

Thank you,

{{$app_name}} Team