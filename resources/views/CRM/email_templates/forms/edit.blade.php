<div class="modal fade" id="modal-edit-email-template" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal" name="form-edit-email-template" id="form-edit-email-template" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit <span id="template-id"></span> Template</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <input type="hidden" name="email_type">
                        <div class="form-group">
                            <label for="edit-subject-input" class="col-sm-1 control-label">Subject</label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" id="edit-subject-input" name="subject"
                                       placeholder="Enter Subject">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="content-variables" class="col-sm-1 control-label">Variables</label>

                            <div class="col-sm-11">
                                @foreach(EmailTemplate::$email_variables as $email_variable)
                                    <button type="button" role="button"
                                            id="email-variable-{{ $email_variable["key"] }}"
                                            data-value="{{ $email_variable["value"] }}"
                                            class="btn btn-default email-variable-button">{{ $email_variable["key"] }}</button>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="edit-content-textarea" class="col-sm-1 control-label">Content</label>

                            <div class="col-sm-11">
                                <textarea name="content" id="edit-content-textarea" class="form-control" rows="20" style="resize: none;" placeholder="Enter Content"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>