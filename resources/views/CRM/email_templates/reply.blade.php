@extends('layouts.email')

@section('content')
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p>Hello {{ $username }},</p>
                <p>{{ $content }}</p>
                <br />
                <p>Note: This is auto generated email system. Do not Reply.</p>
                <p>Thank you,</p>
                <p><i>{{ config('app.name') }} Team</i></p>
            </td>
        </tr>
    </table>
@endsection