@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Charges</li>
        <li class="active">Membership</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body">
                    <table id="membership-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>Logo</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Currency</td>
                            <td>Price</td>
                            <td>Type</td>
                            <td>Status</td>
                            <td>Billing Cycle (Days)</td>
                            <td># of Tables</td>
                            <td>Rake (%)</td>
                            @if(Auth::guard('crm')->user()->canAccess('view', 'membership.inclusions.index') || Auth::guard('crm')->user()->canAccess('view', 'membership.avatars.index') || Auth::guard('crm')->user()->canAccess('edit', 'membership.update'))
                            <td>Inclusions</td>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($membership_charges as $membership)
                            <tr>
                                <td title="{{ $membership->name }}">
                                    @if($membership->logo != MembershipCharges::DEFAULT_LOGO)
                                        <a href="{{ $membership->logo }}">
                                            <img src="{{ str_replace('720x720', '100x100', $membership->logo) }}" alt="{{ $membership->name }}" height="25">
                                        </a>
                                    @else
                                        <a href="{{ MembershipCharges::DEFAULT_LOGO_720x720 }}">
                                            <img src="{{ str_replace('720x720', '100x100', MembershipCharges::DEFAULT_LOGO_720x720) }}" alt="{{ $membership->name }}" height="25">
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $membership->name }}</td>
                                <td>{{ $membership->description }}</td>
                                <td>{{ is_null($membership->currency_id) ? 'N/A' : $membership->currency->currency }}</td>
                                <td>{{ number_format($membership->price, 2) }}</td>
                                <td>{{ $membership->membership_type }}</td>
                                <td>{{ $membership->status_name }}</td>
                                <td>{{ $membership->billing_cycle }}</td>
                                <td>{{ $membership->no_of_tables }}</td>
                                <td>{{ $membership->banker_rake }}</td>
                                @if(Auth::guard('crm')->user()->canAccess('view', 'membership.inclusions.index') || Auth::guard('crm')->user()->canAccess('view', 'membership.avatars.index') || Auth::guard('crm')->user()->canAccess('edit', 'membership.update'))
                                <td>
                                    @usercan('edit', 'membership.update')
                                    <button type="button" onclick="editMembership(this);"
                                            data-membership-id="{{ $membership->membership_id }}"
                                            data-logo="{{ $membership->logo }}"
                                            data-name="{{ $membership->name }}"
                                            data-description="{{ $membership->description }}"
                                            data-currency-id="{{ $membership->currency_id }}"
                                            data-price="{{ number_format($membership->price, 2) }}"
                                            data-billing-cycle="{{ $membership->billing_cycle }}"
                                            data-status-id="{{ $membership->status_id }}"
                                            data-membership-type="{{ $membership->membership_type }}"
                                            data-no-of-tables="{{ $membership->no_of_tables }}"
                                            data-banker-rake="{{ $membership->banker_rake }}"
                                            class="btn btn-warning btn-xs"
                                            title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    @endusercan

                                    @usercan('view', 'membership.inclusions.index')
                                    <a href="{{ route('membership.inclusions.index', $membership->membership_id) }}#inclusions"><i class="fa fa-cart-arrow-down"></i> Inclusions</a>
                                    @cant
                                        @usercan('view', 'membership.avatars.index')
                                        <a href="{{ route('membership.avatars.index', $membership->membership_id) }}#avatar"><i class="fa fa-cart-arrow-down"></i> Inclusions</a>
                                        @endusercan
                                    @endusercan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('add', 'membership.add')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                            data-target="#modal-add-membership" title="Add"><i class="fa fa-plus"></i>
                    </button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.charges.membership.forms.add')
    @include('CRM.charges.membership.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $('#membership-table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                    {
                        "targets": 0,
                        "orderable": false
                    }
                        @if(Auth::guard('crm')->user()->canAccess('view', 'membership.inclusions.index') || Auth::guard('crm')->user()->canAccess('view', 'membership.avatars.index') || Auth::guard('crm')->user()->canAccess('edit', 'membership.update'))
                    ,{
                        "targets": 10,
                        orderable: false
                    }
                    @endif
                ]
            });

            $("#modal-add-membership").on("hidden.bs.modal", function () {
                var form = $('#formmembership');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formmembership').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $("#modal-edit-membership").on("hidden.bs.modal", function () {
                var form = $('#form-edit-membership');
                form.removeAttr('data-membership-id');
                form.trigger('reset');
                clearErr(form);
            });

            $('#form-edit-membership').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: '/charges/membership/' + form.data('membership-id'),
                    data: new FormData(form[0]),
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });

        var editMembership = function (src) {
            var form = $('#form-edit-membership');
            form.attr('data-membership-id', $(src).data('membership-id'));
            form.find('input[name=name]').val($(src).data('name'));
            form.find('input[name=description]').val($(src).data('description'));
            form.find('input[name=price]').val($(src).data('price'));
            form.find('input[name=membership_type]').val($(src).data('membership-type'));
            form.find('input[name=no_of_tables]').val($(src).data('no-of-tables'));
            form.find('input[name=banker_rake]').val($(src).data('banker-rake'));
            form.find('input[name=billing_cycle]').val($(src).data('billing-cycle'));
            form.find('select[name=currency_id]').val($(src).data('currency-id')).trigger('change');
            form.find('select[name=status_id]').val($(src).data('status-id')).trigger('change');
            $('#modal-edit-membership').modal('show');
        };
    </script>
@endsection
