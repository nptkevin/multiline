<div class="modal fade" id="modal-edit-charges" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="formeditcharges" id="formeditcharges"
                  action="/charges/regular/edit" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit [<span id="modal-edit-title"></span>] </h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="modal-edit-name" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="modal-edit-name" name="name"
                                       placeholder="Charge Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="modal-edit-description" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="modal-edit-description"
                                       name="description" placeholder="Description">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="modal-edit-currency-id" class="col-sm-2 control-label">Currency</label>

                            <div class="col-sm-10">
                                <select id="modal-edit-currency-id" class="form-control" name="currency_id">
                                    @foreach($global_currencies as $currency)
                                        <option value="{{ $currency->currency_id }}" {{ boolval($currency->default_registration) ? ' selected' : '' }}>{{ $currency->currency }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="modal-edit-price" class="col-sm-2 control-label">Price</label>

                            <div class="col-sm-10">
                                <input type="number" step='0.01' value='0.00' class="form-control"
                                       id="modal-edit-price" name="price" placeholder="Price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="modal-edit-value" class="col-sm-2 control-label">Value</label>

                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="modal-edit-value" name="value"
                                       placeholder="Value" step='0.01' value='0.00'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="modal-edit-status" class="col-sm-2 control-label">Status</label>

                            <div class="col-sm-10">
                                <select id="modal-edit-status" class="form-control" name="status_id">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status['status_id'] }}">{{ $status['status_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('edit', 'regular.edit')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save changes
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>