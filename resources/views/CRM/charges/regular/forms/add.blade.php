<div class="modal fade" id="modal-add-charges" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" name="formcharges" id="formcharges" action="{{ route('regular.add') }}"
                  method="POST">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Regular Charge</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name-input" class="col-sm-2 control-label">Name</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name-input" name="name"
                                       placeholder="Charge Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description-input-input" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="description-input" name="description"
                                       placeholder="Description">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="currency-id-select" class="col-sm-2 control-label">Currency</label>

                            <div class="col-sm-10">
                                <select id="currency-id-select" class="form-control" name="currency_id">
                                    @foreach($global_currencies as $currency)
                                        <option value="{{ $currency->currency_id }}" {{ boolval($currency->default_registration) ? ' selected' : '' }}>{{ $currency->currency }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price-input" class="col-sm-2 control-label">Price</label>

                            <div class="col-sm-10">
                                <input type="number" step='0.01' value='0.00' class="form-control"
                                       id="price-input" name="price" placeholder="Price">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="value-input" class="col-sm-2 control-label">Value</label>

                            <div class="col-sm-10">
                                <input type="number" class="form-control" step='0.01' id="value-input" name="value"
                                       placeholder="Value" value='0.00'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status-input" class="col-sm-2 control-label">Status</label>

                            <div class="col-sm-10">
                                <select id="status-input-edit" class="form-control" name="status_id">
                                    @foreach($statuses as $status)
                                        <option value="{{ $status['status_id'] }}">{{ $status['status_name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    @usercan('add', 'regular.add')
                    <button type="submit" role="button" data-loading-text='{{ trans('loading.please_wait') }}'
                            class="btn btn-primary">
                        Save
                    </button>
                    @endusercan
                </div>
            </form>
        </div>
    </div>
</div>