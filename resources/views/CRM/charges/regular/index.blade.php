@extends('CRM.layouts.dashboard')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Charges</li>
        <li class="active">Regular</li>
    </ol>
@endsection

@section('content')
    <div class='row'>
        <div class='col-xs-12'>
            <!-- Box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ $sub_title }}</h3>
                    <div class="box-tools pull-right"></div>
                </div>
                <div class="box-body">
                    <table id="charge_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Description</td>
                            <td>Currency</td>
                            <td>Price</td>
                            <td>Value</td>
                            <td>Status</td>
                            @if(Auth::guard('crm')->user()->canAccess('edit', 'regular.edit'))
                                <td>Actions</td>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($regular_charges as $regular)
                            <tr>
                                <td>{{ $regular->name }}</td>
                                <td>{{ $regular->description }}</td>
                                <td data-currency-id="{{ is_null($regular->currency_id) ? 0 : $regular->currency->currency_id }}">{{ is_null($regular->currency_id) ? 'N/A' : $regular->currency->currency }}</td>
                                <td>{{ $regular->price }}</td>
                                <td>{{ $regular->charge_details->value }}</td>
                                <td>{{ $regular->status_name }}</td>
                                @if(Auth::guard('crm')->user()->canAccess('edit', 'regular.edit'))
                                <td>
                                    @usercan('edit', 'regular.edit')
                                    <button type="button" data-toggle="modal" data-target="#modal-edit-charges"
                                            data-c_name="{{ $regular->name }}"
                                            data-c_id="{{ $regular->charge_id }}"
                                            data-c_status="{{ $regular->status_id }}"
                                            data-c_descr="{{ $regular->description }}"
                                            data-c_currency_id="{{ is_null($regular->currency_id) ? 0 : $regular->currency->currency_id }}"
                                            data-c_price="{{ $regular->price }}"
                                            data-c_value="{{ $regular->charge_details->value }}"
                                            class="btn-xs btn-warning"
                                            title="Edit">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                    @endusercan
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
                @usercan('add', 'regular.add')
                <div class="box-footer">
                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modal-add-charges" title="Add">
                        <i class="fa fa-plus"></i>
                    </button>
                </div><!-- /.box-footer-->
                @endusercan
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    @include('CRM.charges.regular.forms.add')
    @include('CRM.charges.regular.forms.edit')
@endsection

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css") }}">
@endsection

@section('scripts')
    <!-- DataTables -->
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net/js/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") }}"></script>
@endsection

@section('script')
    <!-- ClubNine -->
    <script src="{{ asset("CRM/Capital7-1.0.0/js/form-validation.js") }}"></script>

    <script>
        $(function () {
            $('#charge_table').DataTable({
                "paging": false,
                "ordering": true,
                "searching": true,
                "responsive": true,
                columnDefs: [
                        @if(Auth::guard('crm')->user()->canAccess('edit', 'regular.edit') || Auth::guard('crm')->user()->canAccess('delete', 'regular.delete'))
                    {
                        targets: 6,
                        orderable: false
                    }
                    @endif
                ]
            });

            $("#modal-edit-price").on("keyup mouseup paste", function () {
                $("#modal-edit-value").val(this.value);
            });

            $("#price-input").on("keyup mouseup paste", function () {
                $("#value-input").val(this.value);
            });

            $('#modal-edit-charges').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);

                var charge_id = button.data('c_id');
                var charge_name = button.data('c_name');
                var charge_description = button.data('c_descr');
                var charge_price = button.data('c_price');
                var charge_value = button.data('c_value');
                var currency_id = button.data('c_currency_id');

                var charge_status = button.data('c_status');
                $('#modal-edit-title').text(charge_name);
                $('#modal-edit-name').val(charge_name);
                $('#modal-edit-description').val(charge_description);
                $('#modal-edit-currency-id').val(currency_id);
                $('#modal-edit-price').val(charge_price);
                $('#modal-edit-value').val(charge_value);

                $("#formeditcharges").attr("action", "/charges/regular/edit/" + charge_id);
                $('#modal-edit-status option[value=' + charge_status + ']').attr('selected', true);
            });

            $("#modal-add-charges").on("hidden.bs.modal", function () {
                var form = $('#formcharges');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formcharges').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });

            $("#modal-edit-charges").on("hidden.bs.modal", function () {
                var form = $('#formeditcharges');
                form.trigger('reset');
                clearErr(form);
            });

            $('#formeditcharges').submit(function (e) {
                e.preventDefault();
                var form = $(this);
                var btn = form.find(':submit').button('loading');

                $.ajax({
                    type: form.prop('method'),
                    url: form.prop('action'),
                    data: form.serialize(),
                    success: function (response) {
                        if (response.status == '{{ config('response.type.success') }}') {
                            location.reload();
                            return;
                        } else if (response.status == '{{ config('response.type.error') }}') {
                            assocErr(response.errors, form);
                        }
                    }
                }).done(function () {
                    btn.button('reset');
                });
            });
        });
    </script>
@endsection
