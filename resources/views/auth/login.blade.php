@extends('layouts.app')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
@endsection

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-7">
				<p align="center">
					<img src="{{ asset('images/brand.png') }}" class="img-brand">
				</p>

				<div class="card card-auth">
					<div class="card-body card-auth-body">
						<form method="POST" action="{{ route('web.login') }}" aria-label="{{ __('Login') }}">
							@csrf

							<div class="form-group row" align="center">
								<div class="col-md-8 offset-md-2">
									<label for="username" class="label-float auth-label"><i class="far fa-user"></i> &nbsp; {{ __('Username') }}</label>

									<textarea id="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" spellcheck="false" rows="1" required autofocus>{{ old('username') }}</textarea>

									@if ($errors->has('username'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('username') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row" align="center">
								<div class="col-md-8 offset-md-2">
									<label for="password" class="label-float auth-label"><i class="fas fa-key"></i> &nbsp; {{ __('Password') }}</label>

									<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

									@if ($errors->has('password'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-8 offset-md-2">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

										<label class="form-check-label auth-label" for="remember">
											{{ __('Remember Me') }}
										</label>
									</div>
								</div>
							</div>

							<div class="form-group row card-auth-btn">
								<div class="col-md-8 offset-md-2">
									<button type="submit" class="btn btn-pill btn-block btn-orange btn-auth-submit">{{ __('Login') }}</button>
									<a class="btn btn-pill btn-block btn-link" href="{{ route('password.request') }}">{{ __('Forgot Password') }}</a>
									<hr />
									<p align="center"><small class="auth-label">Don't have an account yet?</small></p>
									<a class="btn btn-pill btn-block btn-link" href="{{ route('web.register.form') }}">{{ __('Register') }}</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/addons/label-float.js') }}"></script>
@endsection