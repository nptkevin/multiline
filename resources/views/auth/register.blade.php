@extends('layouts.app')

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/auth.css') }}">
@endsection

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-7">
				<p align="center">
					<img src="{{ asset('images/brand.png') }}" class="img-brand">
				</p>

				<div class="card card-auth">
					<div class="card-body card-auth-body">
						<form method="POST" action="{{ route('web.register') }}" aria-label="{{ __('Register') }}">
							@csrf

							<div class="form-group row">
								<div class="col-md-8 offset-md-2">
									<label for="name" class="label-float auth-label">{{ __('Name') }}</label>

									<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

									@if ($errors->has('name'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-8 offset-md-2">
									<label for="email" class="label-float auth-label">{{ __('E-Mail Address') }}</label>
									<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

									@if ($errors->has('email'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-8 offset-md-2">
									<label for="password" class="label-float auth-label">{{ __('Password') }}</label>
									<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

									@if ($errors->has('password'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('password') }}</strong>
										</span>
									@endif
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-8 offset-md-2">
									<label for="password-confirm" class="label-float auth-label">{{ __('Confirm Password') }}</label>
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
								</div>
							</div>

							<div class="form-group row card-auth-btn">
								<div class="col-md-8 offset-md-2">
									<button type="submit" class="btn btn-pill btn-block btn-orange btn-auth-submit">{{ __('Register') }}</button>
									<hr />
									<p align="center"><small class="auth-label">Already have an account?</small></p>
									<a class="btn btn-pill btn-block btn-link" href="{{ route('web.login.form') }}">{{ __('Login') }}</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/addons/label-float.js') }}"></script>
@endsection