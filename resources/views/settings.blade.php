@extends('layouts.app')

@section('styles')
	<style type="text/css">
		body{
			position: relative;
		}

		.control-label{
			padding: 0.5rem 0rem;
			vertical-align: middle;
		}

		.fa-hash{
			color: #f6993f;
			text-shadow: -3px 3px 6px #44444444;
		}

		hr{
			margin-top: 5rem;
			margin-bottom: 5rem;

			border-top: 1px solid rgba(0, 0, 0, .05);
		}

		.list-group-item.active{
			background: #f6993f;
			border-color: #f6993f;
		}

		.settings-panel{
			margin-bottom: 3rem;

			font-family: Raleway, sans-serif;
		}
			.settings-panel small{
				font-size: 12px;
			}

		.settings-sidebar{
			position: fixed;
			width: 15%;
		}

		.ss-settings::-webkit-scrollbar{
			width: 5px;
		}
		.ss-settings::-moz-scrollbar{
			width: 5px;
		}
		.ss-settings::-webkit-scrollbar-track{
			background: transparent;
		}
		.ss-settings::-moz-scrollbar-track{
			background: transparent;
		}
		.ss-settings::-webkit-scrollbar-thumb{
			background: #ffbd7c;
			border-radius: 5px;
		}
		.ss-settings::-moz-scrollbar-thumb{
			background: #ffbd7c;
			border-radius: 5px;
		}
		.ss-settings::-webkit-scrollbar-thumb:hover{
			background: #f6993f;
		}
		.ss-settings::-moz-scrollbar-thumb:hover{
			background: #f6993f;
		}

		.ss-settings .row{
			margin-bottom: 10px;
		}
	</style>
@endsection

@section('content')
	<div class="container" style="margin-top: 100px;">
		<form id="trade-settings">
			{{ csrf_field() }}
			<input type="hidden" name="userid" value="{{ session('user')->id }}">

			<div class="row">
				<div class="settings-sidebar col-3">
					<h1>Trade Settings</h1>

					<div id="list-ss-settings" class="list-group">
						<a class="list-group-item list-group-item-action" href="#viewsetup">View Setup</a>
						<a class="list-group-item list-group-item-action" href="#betcolumns">Bet Columns</a>
						<a class="list-group-item list-group-item-action" href="#betslip">Bet Slip</a>
						<a class="list-group-item list-group-item-action" href="#moresettings">More Settings</a>
					</div>

					<div class="row">
						<div class="col-12 text-center">
							<br />
							<a href="#" class="btn btn-lg btn-block btn-orange" id="save-settings">Save Changes</a>
						</div>
					</div>
				</div>

				<div class="offset-3 col-9 ss-settings">
					<h1 class="settings-panel" id="viewsetup">
						<i class="fas fa-hashtag fa-hash"></i> &nbsp; ViewSetup
						<br />
						<small>Choose which bookie style should the Trade page emulate?</small>
					</h1>

					<p>
						<div class="row">
							<div class="col-sm-12 col-md-7">
								<label class="control-label">Odds View Setup</label>
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="viewsetup">
									<option value="Asian" {{ $viewsetup->viewsetup == "Asian" ? "selected" : "" }}>Asian</option>
									<option value="European" {{ $viewsetup->viewsetup == "European" ? "selected" : "" }}>European</option>
								</select>
							</div>
						</div>
					</p>

					<hr />

					<h1 class="settings-panel" id="betcolumns">
						<i class="fas fa-hashtag fa-hash"></i> &nbsp; BetColumns
						<br />
						<small>The types of bet that are shown in the table of the trading screen.</small>
					</h1>

					<p>
						@foreach($football AS $row)
							<div class="row">
								<div class="col-3 col-sm-2" align="center">
									<span class="toggle toggle-orange">
										<input type="checkbox" name="{{ strtolower(str_replace(array(' ', '1H', '/'), array('_', 'fh', ''), $row['toggle'])) }}" id="toggle-{{ strtolower(str_replace(array(' ', '1H', '/'), array('_', 'fh', ''), $row['toggle'])) }}" {{ $betcolumns->{strtolower(str_replace(array(' ', '1H', '/'), array('_', 'fh', ''), $row['toggle']))} ? "checked" : "" }}>
										<label for="toggle-{{ strtolower(str_replace(array(' ', '1H', '/'), array('_', 'fh', ''), $row['toggle'])) }}">Toggle</label>
									</span>
								</div>

								<div class="col-9 col-sm-10">
									{{ $row['toggle'] }}
								</div>
							</div>
						@endforeach
					</p>

					<hr />

					<h1 class="settings-panel" id="betslip">
						<i class="fas fa-hashtag fa-hash"></i> &nbsp; BetSlip
						<br />
						<small>Lorem ipsum dolor amet</small>
					</h1>

					<p>
						@foreach($slip AS $row)
							<div class="row">
								<div class="col-3 col-sm-2" align="center">
									<span class="toggle toggle-orange">
										<input type="checkbox" name="{{ $row['toggle'] }}" id="betslip-{{ strtolower(str_replace(' ', '', $row['label'])) }}" {{ $betslip->{$row['toggle']} ? "checked" : "" }}>
										<label for="betslip-{{ strtolower(str_replace(' ', '', $row['label'])) }}">Toggle</label>
									</span>
								</div>

								<div class="col-9 col-sm-10">
									{{ $row['label'] }}
								</div>
							</div>
						@endforeach

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Multiple Accounts On Betslip
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="betslip_multi_acc">
									<option value="all" {{ $betslip->betslip_multi_acc == "all" ? "selected" : "" }}>Select All</option>
									<option value="random" {{ $betslip->betslip_multi_acc == "random" ? "selected" : "" }}>Select One at Random</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Price on Betslip
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="betslip_price">
									<option value="best" {{ $betslip->betslip_price == "best" ? "selected" : "" }}>Best</option>
									<option value="average" {{ $betslip->betslip_price == "average" ? "selected" : "" }}>Average</option>
									<option value="both" {{ $betslip->betslip_price == "both" ? "selected" : "" }}>Both</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Maximum Bet
							</div>

							<div class="col-sm-12 col-md-5">
								<input type="text" class="form-control" name="max_bet" value="{{ $betslip->max_bet }}">
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Default Stake (B)
							</div>

							<div class="col-sm-12 col-md-5">
								<input type="text" class="form-control" name="stake_def" value="{{ $betslip->stake_def}}">
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Default Price in Betslips
							</div>

							<div class="col-sm-12 col-md-5">
								<input type="text" class="form-control" name="betslip_price_def" value="{{ $betslip->betslip_price_def}}">
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Default Expiry
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="expiry_def">
									<option value="10" {{ $betslip->expiry_def == "10" ? "selected" : "" }}>10 mins</option>
									<option value="20" {{ $betslip->expiry_def == "20" ? "selected" : "" }}>20 mins</option>
									<option value="30" {{ $betslip->expiry_def == "30" ? "selected" : "" }}>30 mins</option>
								</select>
							</div>
						</div>
					</p>

					<hr />

					<h1 class="settings-panel" id="moresettings">
						<i class="fas fa-hashtag fa-hash"></i> &nbsp; MoreSettings
						<br />
						<small>Lorem ipsum dolor amet</small>
					</h1>

					<p>
						<div class="row">
							<div class="col-3 col-sm-2" align="center">
								<span class="toggle toggle-orange">
									<input type="checkbox" name="sounds" id="more-sounds" {{ $moresettings->sounds ? "checked" : "" }}>
									<label for="more-sounds">Toggle</label>
								</span>
							</div>

							<div class="col-9 col-sm-10">
								Play Sounds
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Competition Bookie
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="bookie_comp">
									<option value="0">None</option>
									<option value="1">Abc</option>
									<option value="2">123</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								Price Bookies
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" multiple name="bookie_price[]">
									<option value="10">10 mins</option>
									<option value="20">20 mins</option>
									<option value="30">30 mins</option>
								</select>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 col-md-7">
								<label class="control-label">Account Reselection</label>
							</div>

							<div class="col-sm-12 col-md-5">
								<select class="form-control select2" name="reselect_acc">
									<option value="never" {{ $moresettings->reselect_acc == "never" ? "selected" : "" }}>Never Reselect</option>
									<option value="btr" {{ $moresettings->reselect_acc == "btr" ? "selected" : "" }}>Only Bookies at Time of Replacement</option>
									<option value="add_new" {{ $moresettings->reselect_acc == "add_new" ? "selected" : "" }}>Add new Bookies to Order</option>
								</select>
							</div>

							<hr />
						</div>
					</p>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$(function(){
			$('.list-group-item-action').on('click', function(){
				var $target = $(this).attr('href');

				$('html, body').animate({
					scrollTop: $target == "#viewsetup" ? 0 : (parseFloat($($target).offset().top) - 100)
				}, 1500, "easeOutCirc");
			});

			$('#save-settings').on('click', function($e){
				$('html, body').animate({
					scrollTop: 0
				}, 1500, "easeOutCirc");

				$.post("{{ route('settings.trade.save') }}", $('#trade-settings').serialize(), function($response, $status){
					var $obj = $.parseJSON($response);

					console.log($obj);
				});
			});

			$('.select2').select2();
		});
	</script>
@endsection