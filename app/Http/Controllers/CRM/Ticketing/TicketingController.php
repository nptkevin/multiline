<?php

namespace App\Http\Controllers\CRM\Ticketing;

use App\API\Application;
use App\API\Tradeprofile;
use App\common\BankInfo;
use App\common\TradeMarketTicket;
use App\common\TradeMarketTicketDeposit;
use App\common\TradeMarketTicketHistory;
use App\common\TradeMarketTicketTransfer;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\Wallet_ledger AS WalletLedger;
use App\CRM\NinepineModels\WalletTransfer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class TicketingController extends Controller{
	public function index(){
		$data['page_title'] = "Tickets";
		$data['tickets_menu'] = true;

		return view('CRM.ticketing.index')
			->with($data);
	}

	public function datatable(Request $request){
		return dataTable(
			$request,
			TradeMarketTicket::where('inquiry_by', 0)
				->where('trademarket_ticket_parent_id', 0)
				->with(
					'status',
					'subticketcount',
					'trade_profile',
					'trader_account',
					'wallet',
					'wallet.currency'
				)
		);
	}

	public function ticketDetails($id){
		$data['page_title'] = "Ticket Details";
		$data['tickets_menu'] = true;
		$data['ticket'] = TradeMarketTicket::leftJoin('Tradeprofile AS tp', 'TradeMarketTicket.trade_profile_id', '=', 'tp.tradeprofile_id')
			->leftJoin('crmcurrency AS gc', function($join){
				$join->on('tp.globalcurrency_id', '=', 'gc.currency_id')
					->orOn('tp.sellcurrency_id', '=', 'gc.currency_id');
			})
			->leftJoin('crmcurrency AS cc', 'tp.cryptocurrency_id', '=', 'cc.currency_id')
			->where('TradeMarketTicket.trademarket_ticket_id', $id)
			->with('status', 'trader_account', 'inquiry_account')
			->select(
				'*',
				'TradeMarketTicket.amount AS amount',
				'TradeMarketTicket.value AS value',
				'gc.currency_name AS gcname',
				'gc.currency_symbol AS gcsymbol',
				'cc.currency_name AS ccname',
				'cc.currency_symbol AS ccsymbol'
			)
			->first();
		$data['subticket'] = TradeMarketTicket::where('trademarket_ticket_parent_id', $id)
			->orderBy('TradeMarketTicket.ticket_generated_id', 'asc')
			->with('status', 'currency', 'trader_account', 'wallet', 'wallet.currency', 'inquiry_account', 'inquiry_account.wallet', 'trade_profile')
			->select(
				'*',
				'TradeMarketTicket.amount AS amount',
				'TradeMarketTicket.value AS value'
			)
			->get();

		$data['flags'] = array(
			'open' => 'primary',
			'offer' => 'primary',
			'rejected' => 'danger',
			'approved' => 'success',
			'payment made' => 'success',
			'payment received' => 'success',
			'close' => 'dark',
			'approve' => 'success',
			'pending' => 'info',
			'completed' => 'success',
			'dispute' => 'warning',
			'cancelled' => 'danger'
		);

		return view('CRM.ticketing.ticketinfo.index')
			->with($data);
	}

	public function trackTicket(Request $request){
		$data = array();
		$track = TradeMarketTicketHistory::leftJoin('TradeMarketTicket AS t', 't.trademarket_ticket_id', '=', 'TradeMarketTicketHistory.trademarket_ticket_id')
			->leftJoin('users AS u', 't.inquiry_by', '=', 'u.id')
			->leftJoin('crmstatus AS s', 'TradeMarketTicketHistory.ticket_status', '=', 's.status_id')
			->where('TradeMarketTicketHistory.trademarket_ticket_id', $request->id)
			->orderBy('TradeMarketTicketHistory.created_at', 'asc') // SWITCH TO DESC IF NECESSARY
			->select(
				// 'TradeMarketTicketHistory.created_at',
				\DB::raw('TRIM(TO_CHAR("TradeMarketTicketHistory"."created_at", \'MON\')) AS "created_at_month"'),
				\DB::raw('TO_CHAR("TradeMarketTicketHistory"."created_at", \'DD\') AS "created_at_date"'),
				\DB::raw('TO_CHAR("TradeMarketTicketHistory"."created_at", \'YYYY\') AS "created_at_year"'),
				\DB::raw('TO_CHAR("TradeMarketTicketHistory"."created_at", \'HH12:MI AM\') AS "created_at_time"'),
				'u.username',
				's.status_name',
				'TradeMarketTicketHistory.note'
			)
			->get();
		$data['track'] = $track;

		return response()->json($data, 200);
	}

	/**
	 * @version CP-260 from branch `ticket-system`
	 *
	 * CRM Administrator Ticket System Actions.
	 * 		Monitor and avoid `ticket action` to be modified client-side.
	 * 			@category `offer` 				['reward', 'revert']
	 * 			@category `paymentmade` 		['reward']
	 * 			@category `dispute` 			['reward', 'revert']
	 *
	 * 		Conditional Hierarchy
	 * 			@var `$request->ticketaction` 	['reward', 'revert']
	 * 			@var `$request->type` 			['offer', 'paymentmade', 'dispute']
	 * 			@var `$request->tickettype` 	['Buyer', 'Seller']
	 * 			@var `$request->tid` 			['trademarket_ticket_id']
	 *
	 * 		SweetAlert Variables
	 * 			@var `swal` 					['success', 'info', 'error']
	 *
	 * 		@param 	Request $request
	 * 		@return \Illuminate\Http\JsonResponse
	 *
	 * 		References
	 * 			@see `controllers` 				App\Http\Controllers\api\Ticket\TicketController.php
	 * 			@see `models` 					App\common\TradeMarketTicket.php
	 * 											App\common\TradeMarketTicketHistory.php
	 * 											App\CRM\NinepineModels\Status.php
	**/
	public function crmTicketAction(Request $request){
		$data = array();
		$checkticket = TradeMarketTicket::where('trademarket_ticket_id', $request->tid)
			->with(
				'trade_profile',
				'inquiry_account',
				'trader_account'
			);

		if($request->ticketaction == "reward"){
			switch($request->type){
				case "offer":
					$approved_id = Status::getIdOf('TradeMarketTicket', 'Approved');
					$ticket = $checkticket->first();
					$bankinfo_id = BankInfo::where('user_id', $ticket->trader_account->id)
						->where('is_default', true)
						->first()
						->bankinfo_id;
					$ticket->bankinfo_id = $bankinfo_id;
					$ticket->ticket_status = $approved_id;
					$ticket->save();

					$fileurl = "";

					if($request->hasFile('attachments')){
						$file = $request->attachments;
						$depositid = TradeMarketTicketDeposit::orderBy('id', 'desc')->first()->id + 1;
						$dir = "public/attachments/ticket_deposit/$depositid";
						$path = storage_path("app/$dir");
						$filename = $file->getClientOriginalName();

						if(!File::exists($path))
							File::makeDirectory($path, 0775, true);

						$replace = str_replace(storage_path('app/public'), '', $file->move($path, $filename));
						$fileurl = config('app.url') . $replace;
					}

					$deposit = $this->ticketDeposit($ticket, $fileurl, 'Action made by Capital7 CRM Administrator | ' . $request->notes);
					$ticket->sendEmail();
					$this->addToHistory($ticket, $approved_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['ticket'] = $ticket;
					$data['response'] = $this->actionResponse('success', 'Success', 'Accept Offer Action Successfully Sent.', 'success');
					break;

				case "paymentmade":
					$payment_received_id = Status::getIdOf('TradeMarketTicket', 'Payment Received');
					$pending_id = Status::getIdOf('TradeMarketTicketTransfer', 'Pending');
					$ticket = $checkticket->first();
					$ticket->ticket_status = $payment_received_id;
					$ticket->save();

					$this->ticketTransfer($ticket, $pending_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);
					$ticket->sendEmail();
					$this->addToHistory($ticket, $payment_received_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['ticket'] = $ticket;
					$data['response'] = $this->actionResponse('success', 'Success', 'Accept Offer Action Successfully Sent.', 'success');
					break;

				case "paymentreceived":
					$completed_id = Status::getIdOf('TradeMarketTicket', 'Completed');
					$approved_id = Status::getIdOf('TradeMarketTicketTransfer', 'Approve');
					$ticket = $checkticket->first();
					$ticket->ticket_status = $completed_id;
					$ticket->save();

					$offer_balance = Wallet::find($ticket->wallet_id);
					$owner_balance = Wallet::find($ticket->trade_profile->wallet_id);

					if($ticket->trade_profile->type == "Seller"){
						$offer_balance->balance += $ticket->amount;
						$offer_balance->save();
					} else{
						$owner_balance->balance += $ticket->amount;
						$owner_balance->save();
					}

					$source = Source::getIdByName('TradeMarketTicketTransfer');
					$wallet_transfer = $this->walletTransfer($ticket, "Action made by Capital7 CRM Administrator | Marketplace Offer Successful");

					$appid = Application::getIdOf('Capital7');

					if($ticket->trade_profile->type == "Seller"){
						$this->walletLedger($ticket->wallet_id, $source, $wallet_transfer, $ticket->amount, 0, $appid, $offer_balance->balance);
					} else{
						$this->walletLedger($ticket->trade_profile->wallet_id, $source, $wallet_transfer, $ticket->amount, 0, $appid, $owner_balance->balance);
					}

					$this->ticketTransfer($ticket, $approved_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);
					$ticket->sendEmail();
					$this->addToHistory($ticket, $completed_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['response'] = $this->actionResponse('success', 'Success', 'Payment Confirm Action Successfully Sent.', 'success');
					break;

				case "dispute":
					$completed_id = Status::getIdOf('TradeMarketTicket', 'Completed');
					$approved_id = Status::getIdOf('TradeMarketTicketTransfer', 'Approve');
					$ticket = $checkticket->first();
					$ticket->ticket_status = $completed_id;
					$ticket->save();

					$offer_balance = Wallet::find($ticket->wallet_id);
					$owner_balance = Wallet::find($ticket->trade_profile->wallet_id);

					if($ticket->trade_profile->type == "Seller"){
						$offer_balance->balance += $ticket->amount;
						$offer_balance->save();
					} else{
						$owner_balance->balance += $ticket->amount;
						$owner_balance->save();
					}

					$source = Source::getIdByName('TradeMarketTicketTransfer');
					$wallet_transfer = $this->walletTransfer($ticket, "Action made by Capital7 CRM Administrator | Marketplace Offer Successful");

					$appid = Application::getIdOf('Capital7');

					if($ticket->trade_profile->type == "Seller"){
						$this->walletLedger($ticket->wallet_id, $source, $wallet_transfer, $ticket->amount, 0, $appid, $offer_balance->balance);
					} else{
						$this->walletLedger($ticket->trade_profile->wallet_id, $source, $wallet_transfer, $ticket->amount, 0, $appid, $owner_balance->balance);
					}

					$this->ticketTransfer($ticket, $approved_id, 'Dispute action made by Capital7 CRM Administrator | ' . $request->notes);

					$ticket->sendEmail();

					$parent = TradeMarketTicket::find($ticket->trademarket_ticket_parent_id);
					$parent->hasBalance();
					$this->addToHistory($ticket, $completed_id, 'Dispute action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['ticket'] = $ticket;
					$data['response'] = $this->actionResponse('success', 'Success', 'Reward Dispute Action Successfully Sent.', 'success');
					break;

				default:
					$data['response'] = $this->actionResponse('invalid', 'Invalid Request', 'Action not allowed in the application.', 'error');
					break;
			}
		} elseif($request->ticketaction == "revert"){
			switch($request->type){
				case "offer":
					$rejected_id = Status::getIdOf('TradeMarketTicket', 'Rejected');
					$ticket = $checkticket->first();
					$ticket->ticket_status = $rejected_id;
					$ticket->save();

					$ticket->sendEmail();
					$this->addToHistory($ticket, $rejected_id, 'Action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['ticket'] = $ticket;
					$data['response'] = $this->actionResponse('success', 'Success', 'Reject Offer Action Successfully Sent.', 'success');
					break;

				case "paymentmade":
					$data['response'] = $this->actionResponse('invalid', 'Invalid Request', 'Action not allowed in the application.', 'error');
					break;

				case "paymentreceived":
					$data['response'] = $this->actionResponse('invalid', 'Invalid Request', 'Action not allowed in the application.', 'error');
					break;

				case "dispute":
					$rejected_id = Status::getIdOf('TradeMarketTicket', 'Rejected');
					$ticket = $checkticket->first();
					$ticket->ticket_status = $rejected_id;
					$ticket->save();

					// if($request->tickettype == "Seller"){
						$parentticket = Tradeprofile::find($ticket->trade_profile_id);
						$parentticket->amount += $ticket->amount;
						$parentticket->save();

						$parentticket2 = TradeMarketTicket::find($ticket->trademarket_ticket_parent_id);
						$parentticket2->amount += $ticket->amount;
						$parentticket2->save();
					// }

					$ticket->sendEmail();
					$this->addToHistory($ticket, $rejected_id, 'Dispute action made by Capital7 CRM Administrator | ' . $request->notes);

					$data['ticket'] = $ticket;
					$data['response'] = $this->actionResponse('success', 'Success', 'Reject Dispute Action Successfully Sent.', 'success');
					break;

				default:
					$data['response'] = $this->actionResponse('invalid', 'Invalid Request', 'Action not allowed in the application.', 'error');
					break;
			}
		} else{
			$data['response'] = $this->actionResponse('invalid', 'Invalid Request', 'Action not allowed in the application.', 'error');
		}

		return response()->json($data, 200);
	}

	/**
	 * @version CP-260
	 *
	 * Multi-dimensional array for response,
	 *
	 * @param 	$result 		`string` 		['success', 'invalid']
	 * 			$title 			`string`
	 * 			$text 			`string`
	 * 			$type 			`string` 		['success', 'info', 'error']
	 *
	 * @return 	array()
	**/
	private static function actionResponse($result, $title, $text, $type){
		$result = $result == "success" ? "success" : "invalid";

		return [
			config('response.result') => config('response.' . $result),
			config('response.message') => $text,
			"swal" => $type,
			"title" => $title,
		];
	}

	/**
	 * @version CP-260
	 *
	 * Log action made to `TradeMarketTicketHistory` table
	 *
	 * @param 	$ticket 		`array`
	 * 			$ticket_status	`int`
	 * 			$note 			`string`
	**/
	private static function addToHistory($ticket = [], $ticket_status, $note){
		return TradeMarketTicketHistory::create([
			'trademarket_ticket_id' => $ticket->trademarket_ticket_id,
			'ticket_status' => $ticket_status,
			'amount' => $ticket->amount,
			'note' => $note
		]);
	}

	/**
	 * @version CP-260
	 *
	 * Log to `TradeMarketTicketDeposit` table
	 *
	 * @param 	$ticket 		`array`
	 * 			$url 			`string`
	 * 			$note 			`string`
	**/
	private static function ticketDeposit($ticket = [], $url, $note){
		return TradeMarketTicketDeposit::create([
			'trademarket_ticket_id' => $ticket->trademarket_ticket_id,
			'sender_id' => $ticket->trader_account->id,
			'receiver_id' => $ticket->inquiry_account->id,
			'bank_id' => $ticket->bankinfo_id,
			'attachments' => $url,
			'notes' => $note,
			'type' => 'Bank',
		]);
	}

	/**
	 * @version CP_260
	 *
	 * Log to `TradeMarketTicketTransfer` table
	 *
	 * @param 	$ticket 		`array`
	 * 			$status 		`int`
	 * 			$note 			`string`
	**/
	private static function ticketTransfer($ticket = [], $status, $note){
		return TradeMarketTicketTransfer::create([
			'trade_market_ticket_id' => $ticket->trademarket_ticket_id,
			'sender_wallet_id' => $ticket->wallet_id,
			'receiver_wallet_id' => $ticket->trade_profile->wallet_id,
			'status_id' => $status,
			'note' => $note,
		]);
	}

	/**
	 * @version CP-260
	 *
	 * Log to `WalletTransfer` table
	 *
	 * @param 	$ticket 		`array`
	 * 			$note 			`string`
	 * 			$status 		`int` NULLABLE
	 *
	 * @return 	last_insert_id 	`int`
	**/
	private static function walletTransfer($ticket = [], $note, $status = null){
		$wallet_transfer = WalletTransfer::create([
			'sender_id' => $ticket->inquiry_account->id,
			'receiver_id' => $ticket->trader_account->id,
			'amount_transfer' => $ticket->amount,
			'reason' => $note,
			'currency_id' => $ticket->trade_profile->cryptocurrency_id,
			'sender_wallet_id' => $ticket->trade_profile->type == "Seller" ? $ticket->trade_profile->wallet_id : $ticket->wallet_id,
			'receiver_wallet_id' => $ticket->trade_profile->type == "Seller" ? $ticket->wallet_id : $ticket->trade_profile->wallet_id,
			'status_id' => $status,
		]);

		return $wallet_transfer->wallet_transfer_id;
	}

	/**
	 * @version CP-260
	 *
	 * Log to `TradeMarketTicketDeposit` table
	 *
	 * @param 	$ticket 		`array`
	 * 			$url 			`string`
	 * 			$note 			`string`
	**/
	private static function walletLedger($walletid, $source, $wallet_transfer, $debit, $credit, $appid, $balance){
		return WalletLedger::create([
			'wallet_id' => $walletid,
			'source_id' => $source,
			'resource_id' => $wallet_transfer,
			'debit' => $debit,
			'credit' => $credit,
			'application_id' => $appid,
			'balance' => $balance,
		]);
	}
}