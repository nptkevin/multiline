<?php

namespace App\Http\Controllers\CRM\Baccarat;

use App\CRM\baccarat\Accounts;
use App\Http\Controllers\CRM\NinePineController;

class DashboardController extends NinePineController
{
    //
    
    public function index() {
        $page_title ='Dashboard';
        $TotalAccounts = Accounts::count();
         return view('CRM.dashboard/index', compact('page_title', 'TotalAccounts'));
    }

    
}
