<?php

namespace App\Http\Controllers\CRM\Charts;

use App\CRM\baccarat\Accounts;
use App\Http\Controllers\Controller;

class RegisteredUsersController extends Controller
{
    public function registered_users_range($from, $to)
    {
        $registered_data = Accounts::getRegisteredDataFromRange($from, $to);

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => compact('registered_data')
        ]);
    }
}
