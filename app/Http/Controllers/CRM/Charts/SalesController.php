<?php

namespace App\Http\Controllers\CRM\Charts;

use App\CRM\baccarat\Transaction;
use App\Http\Controllers\CRM\NinePineController;

class SalesController extends NinePineController
{
    public function sales_range($from, $to)
    {
        $sales_data = Transaction::getSalesDataFromRange($from, $to);

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => compact('sales_data')
        ]);
    }
}
