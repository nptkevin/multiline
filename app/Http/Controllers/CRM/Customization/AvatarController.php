<?php

namespace App\Http\Controllers\CRM\Customization;

use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\AvatarRequest;
use App\CRM\NinepineModels\Avatar;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;

class AvatarController extends NinePineController
{
    public function json(Request $request)
    {
        $search = $request->query('search');
        $type = $request->query('type');
        $readonly = $request->query('readonly');
        $status_id = $request->query('status_id');
        $per_page = $request->query('per_page');
        $query = Avatar::latest('updated_at')->search($search);
        $except = json_decode($request->query('except'));

        if ($request->has('type')) {
            $query->where('type', $type);
        }

        if ($request->has('status_id')) {
            $query->where('status_id', $status_id);
        }

        if ($request->has('except')) {
            $query->whereNotIn('avatar_id', $except);
        }
        $avatars = $query->paginate($per_page)->appends([
            'search' => $search,
            'per_page' => $per_page,
            'readonly' => $readonly,
            'status_id' => $status_id,
            'type' => $type,
            'except' => $except,
        ]);
        $avatars = [
            'html' => (string)view('customization.avatar.avatars')->with([
                'avatars' => $avatars,
                'readonly' => $readonly == 'true' ? true : false
            ]),
            'links' => (string)$avatars->links()
        ];

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => compact('avatars'),
        ]);
    }

    public function index()
    {
        $data['customization_menu'] = true;
        $data['avatar_menu'] = true;
        $data['page_title'] = 'Avatars';
        $data['applications'] = Applications::all();
        $data['statuses'] = Status::where('status_type', 'Avatar')->get();
        $data['types'] = Avatar::$types;
        return view('CRM.customization.avatar.index')->with($data);
    }

    public function store(AvatarRequest $request)
    {
        $avatar = Avatar::create([
            'name' => $request->input('name'),
            'application_id' => $request->input('application_id'),
            'type' => $request->input('type'),
            'avatar' => Avatar::DEFAULT_AVATAR,
            'status_id' => $request->input('status_id')
        ]);

        if (!$request->hasFile('avatar') || !$avatar->makeAvatar($request->file('avatar'))) {
            $swal = trans('swal.avatar.create.image.error');

            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('swal')
            ]);
        }
        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(AvatarRequest $request, Avatar $avatar)
    {
        $avatar->update([
            'name' => $request->input('name'),
            'application_id' => $request->input('application_id'),
            'type' => $request->input('type'),
            'status_id' => $request->input('status_id')
        ]);

        if ($request->hasFile('avatar')) {
            if (!$avatar->makeAvatar($request->file('avatar'))) {
                $swal = trans('swal.avatar.update.image.error');

                return response()->json([
                    config('response.status') => config('response.type.fail'),
                    config('response.data') => compact('swal')
                ]);
            }
        }
        $avatar->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}
