<?php

namespace App\Http\Controllers\CRM\Accounts;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\BankInfo;
use App\CRM\baccarat\Status;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\AccountsRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;


class AccountsController extends NinePineController
{
    public function __construct()
    {
        $this->middleware('auth:crm');
    }

    public function index()
    {
        $data['page_title'] = "Accounts";
        $data['accounts_menu'] = true;
//        $data['statuses'] = Status::of("Account");
        return view('CRM.accounts.index')->with($data);
    }

    public function dataTable(Request $request)
    {
        return dataTable($request, Accounts::withoutGuest());
    }

    public function update(AccountsRequests $request, Accounts $account){
        $account->update($request->except(['password']));
        $account->birth_date = $request->birth_date;

        if($request->password){
            $account->password = Hash::make($request->password);
        }
        $account->save();

        $swal = trans('swal.account.update.success');

        return redirect()->back()->with(compact('swal'));
    }

    public function details(Request $request, Accounts $account)
    {
        $data['page_title'] = 'Account Information';
        $data['account'] = $account;
        // $data['account_membership'] = $account->memberships()->first();
        $data['statuses'] = Status::of("Account");
        // $banks = $account->bank_info;
        // $bankinfo_id = $request->query('bankinfo_id');

        // if ($bankinfo_id) {
        //     $banks = $account->bank_info()->where('bankinfo_id', $bankinfo_id)->get();
        // }
        // $data['banks'] = $banks;
        $data['accounts_menu'] = true;
        $data['genders'] = Accounts::$genders;
        return view('CRM.accounts.details.index')->with($data);
    }

    public function generateUsername()
    {
        $faker = \Faker\Factory::create();
        $gen_username = $faker->userName(9);

        while( (strlen($gen_username) < 9) || (Accounts::where('username', $gen_username)->count() != 0)) {
            $gen_username = $faker->userName;
        }

        // remove dot and exactly 8 char
        $gen_username = substr(str_replace('.', '', $gen_username), 0, 8);

        return $gen_username;
    }

    public function addAccount(AccountsRequests $request)
    {

        $status_id = Status::where('status_type', 'Account')
            ->where('status_name', 'Active')
            ->first()
            ->status_id;

        Accounts::create([
            //set empty for now
            'name' => '',
            'email' => $request->username.'@ninepinetech.com',

            'username' => $request->username,
            'password' => Hash::make($request->password),
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'status_id' => $status_id
        ]);

        return redirect(route('accounts.index'));
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            "password" => ["required", "min:6", "confirmed", function ($attribute, $value, $fail) {

                if(!preg_match('/[A-Z]/', $value)){
                    $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                }

                if(!preg_match('/[a-z]/', $value)){
                    $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                }

                if(!preg_match('/[0-9]/', $value)){
                    $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                }

                if(!preg_match('/[&@!#+]/', $value)){
                    $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                }

            }
            ]
        ]);

        $account = Accounts::where('id', $request->user_id)->get();
        $account->password = Hash::make($request->password);
        $account->save();

        return response()->json([
            'status' => 'success'
        ]);

    }
}
