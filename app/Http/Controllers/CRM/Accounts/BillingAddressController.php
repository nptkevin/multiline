<?php

namespace App\Http\Controllers\CRM\Accounts;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\BillingAddress;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\BillingAddressRequest;

class BillingAddressController extends NinePineController
{
    private $prefix = 'ba_';

    public function index()
    {
        // For billing address isolation
    }

    public function store(BillingAddressRequest $request, Accounts $account)
    {
        $create = removeArrKeyPrefix($request->all(), $this->prefix);
        $account->billing_address()->create($create);
        return $this->back();
    }

    public function update(BillingAddressRequest $request, Accounts $account, BillingAddress $billing_address)
    {
        $update = removeArrKeyPrefix($request->all(), $this->prefix);
        $billing_address->update($update);
        $billing_address->save();
        return $this->back();
    }

    private function back()
    {
        $swal = trans('swal.account.update.success');
        return redirect()->back()->with(compact('swal'));
    }
}
