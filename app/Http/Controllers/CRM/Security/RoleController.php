<?php

namespace App\Http\Controllers\CRM\Security;

use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\ModuleRequest;
use App\CRM\NinepineModels\Module;
use App\CRM\NinepineModels\Role;
use App\CRM\NinepineModels\RoleModule;

class RoleController extends NinePineController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['modules'] = Module::orderBy('module_name', 'asc')->get();
        $data['page_title'] = 'Module Management';
        $data['security_menu'] = true;
        $data['roles_menu'] = true;
        return view('CRM.security.roles.index')->with($data);
    }

    public function add(ModuleRequest $request)
    {
        $module_name = $request->input('module_name');
        $controller_name = $request->input('controller_name');

        $module = Module::where('module_name', $module_name)
            ->where('controller_name', $controller_name)
            ->first();

        if (!is_null($module)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => swal(
                    trans('swal.module.create.module_taken.title'),
                    trans('swal.module.create.module_taken.html', [
                        'module' => $module_name . '/' . $controller_name
                    ]),
                    trans('swal.module.create.module_taken.type')
                )
            ]);
        }

        if (!moduleExists($module_name, $controller_name)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => swal(
                    trans('swal.module.create.module_not_exists.title'),
                    trans('swal.module.create.module_not_exists.html', [
                        'module' => $module_name . '/' . $controller_name
                    ]),
                    trans('swal.module.create.module_not_exists.type')
                )
            ]);
        }

        Module::create([
            'controller_name' => $controller_name,
            'module_name' => $module_name,
            'module_description' => $request->input('module_description'),
            'can_add' => true,
            'can_edit' => true,
            'can_delete' => true,
            'admin' => true,
            'readonly' => true,
            'parent_id' => $request->input('parent_id')
        ]);

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function edit(ModuleRequest $request, Module $module)
    {
        $module_name = $request->input('module_name');
        $controller_name = $request->input('controller_name');

        $_module = Module::where('module_name', $module_name)
            ->where('controller_name', $controller_name)
            ->where('module_id', '!=', $module->module_id)
            ->first();

        if (!is_null($_module)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => swal(
                    trans('swal.module.update.module_taken.title'),
                    trans('swal.module.update.module_taken.html', [
                        'module' => $module_name . '/' . $controller_name
                    ]),
                    trans('swal.module.update.module_taken.type')
                )
            ]);
        }

        if (!moduleExists($module_name, $controller_name)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => swal(
                    trans('swal.module.update.module_not_exists.title'),
                    trans('swal.module.update.module_not_exists.html', [
                        'module' => $module_name . '/' . $controller_name
                    ]),
                    trans('swal.module.update.module_not_exists.type')
                )
            ]);
        }
        $module->update([
            'controller_name' => $controller_name,
            'module_name' => $module_name,
            'module_description' => $request->input('module_description'),
            'parent_id' => $request->input('parent_id')
        ]);
        $module->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function delete(Module $module)
    {
        $affected = $module->getAffectedRelations();

        if (count($affected)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('affected')
            ]);
        }
        $module->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function json(Role $role)
    {
        $role_modules = RoleModule::where('role_id', $role->role_id)->get();
        $modules['permissions'] = $role_modules->toArray();
        $modules['belong_role'] = $role->role_name;

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => $modules,
        ]);
    }
}
