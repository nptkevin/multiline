<?php

namespace App\Http\Controllers\CRM\News;

use App\CRM\NinepineModels\News;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\Controller;
use App\Http\Requests\CRM\NewsRequests;
use App\Http\Requests\CRM\PromotionsRequests;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;

use Request AS URIRequest;

class NewsController extends Controller{
	private $content_type;

	public function __construct(){
		$this->middleware('auth:crm');
		$this->content_type = URIRequest::segment(2);
	}

	public function index(){
		$data['cms_menu'] = true;
		$data['content_type'] = strtolower($this->content_type);
		$data[$this->content_type . '_menu'] = true;
		$data['page_title'] = ucfirst($this->content_type);

		return view('CRM.cms.content.index')
			->with($data);
	}

	public function dataTable(Request $request){
		return dataTable($request, News::where('content_type', ucfirst($this->content_type))->with('status'));
	}

	public function create(){
		$data['cms_menu'] = true;
		$data['content_type'] = strtolower($this->content_type);
		$data[$this->content_type . '_menu'] = true;
		$data['page_title'] = 'Add ' . ucfirst($this->content_type);
		$data['statuses'] = Status::where('status_type', 'News')->get();
		$data['content_types'] = News::$content_types;
		$data['dir'] = 'u_' . time();

		return view('CRM.cms.content.create')
			->with($data);
	}

	public function store(PromotionsRequests $request){
		$request_values = $request->all();
		$dir = $request->get('dir');
		$content_type = $request->get('content_type');
		$url = News::makeImage($request->file('coverphoto'), public_path("uploads/$content_type/$dir"));
		$request_values['coverphoto'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . "/uploads/$content_type/$dir/$url";
		$request_values['link'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->title));
		$news = News::create($request_values);

		return redirect()->route('crm.' . $this->content_type . '.index');
	}

	public function upload(NewsRequests $request){
		$url = News::makeImage($request->file('content_image'), public_path("uploads/" . $this->content_type . "/" . $request->get('dir')));

		return response()->json([
			config('response.status') => config('response.type.success'),
			config('response.data') => "uploads/" . $this->content_type . "/" . $request->get('dir') . "/" . $url
		]);
	}

	public function getImage($image){
		$file = public_path("uploads/news/$image");

		if(!File::exists($file))
			abort(404);

		return response()->file($file);
	}

	public function formedit(News $id){
		$data['cms_menu'] = true;
		$data['content_type'] = strtolower($this->content_type);
		$data[$this->content_type . '_menu'] = true;
		$data['news'] = $id;
		$data['page_title'] = 'Edit ' . ucfirst($this->content_type);
		$data['statuses'] = Status::where('status_type', 'News')->get();
		$data['content_types'] = News::$content_types;
		$data['dir'] = basename(dirname($id->coverphoto));

		return view('CRM.cms.content.edit')
			->with($data);
	}

	public function update(PromotionsRequests $request){
		$request_values = $request->all();
		$news = News::find($request->news_id);

		if(!empty($request->file('coverphoto'))){
			$dir = $request->get('dir');
			$content_type = $request->get('content_type');
			$url = News::makeImage($request->file('coverphoto'), public_path("uploads/$content_type/$dir"));
			$request_values['coverphoto'] = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on" ? "https://" : "http://" . $_SERVER['HTTP_HOST'] . "/uploads/" . $request->get('content_type') . "/$dir/$url";
		}

		$request_values['link'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->title));
		$news->update($request_values);
		$news->save();

		return redirect()->back();
	}

	public function destroy(News $id){
		$dir = storage_path("app/public/$id->content_type/$id->news_id");

		if(File::exists($dir))
			deleteDir($dir);

		$id->delete();

		return response()->json([
			config('response.status') => config('response.type.success')
		]);
	}
}