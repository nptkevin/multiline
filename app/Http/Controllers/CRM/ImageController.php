<?php

namespace App\Http\Controllers\CRM;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function see(Request $request, $image)
    {
        $file = storage_path("app/public/images/$image");

        if(!File::exists($file)) {
            abort(404);
        }
        return response()->file($file);
    }
}
