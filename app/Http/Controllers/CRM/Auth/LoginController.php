<?php

namespace App\Http\Controllers\CRM\Auth;

use App\Http\Controllers\Controller;
use App\CRM\NinepineModels\Status;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Auth;
class LoginController extends Controller{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';
    protected $guard = 'crm';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $prev = url()->previous();

        session([
            'url.intended' => $prev == config('app.url') ? $this->redirectTo : $prev
        ]);
        $this->redirectTo = session()->get('url.intended');
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {
         \Log::debug('crm action login');
          $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        // Attempt to log the user in
        if (Auth::guard('crm')->attempt(['email' => $request->email, 'password' => $request->password])) {
            // if successful, then redirect to their intended location
             
//             \Log::debug('success===user');
            //Session::put('user', auth()->user()->toArray());
            return redirect('/admin/dashboard');
//            return redirect()->intended(route('crm.dashboard.index'));

        } else {
            return $this->sendFailedLoginResponse($request);
//            return redirect()->back()->withInput($request->only('email', 'remember'));
//            \Log::debug('failed===user');
        }
    }

    public function username(){
        return 'email';
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request){
        // Get the `status_id` of Active user
        $active_status_id = Status::where('status_type', 'User')
            ->where('status_name', 'Active')->first();

        return [
            'email' => $request->{$this->username()},
            'password' => $request->password,
            'status_id' => $active_status_id->status_id
        ];
    }

    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        session(['url.intended' => ""]);
        Session::forget('url.intended');

        return redirect('/admin/login');
    }

    public function showLoginForm()
    {
        return view('CRM.auth.login');
    }
}
