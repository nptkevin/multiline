<?php

namespace App\Http\Controllers\CRM\EmailTemplate;

use App\CRM\NinepineModels\EmailTemplate;
use App\Http\Requests\CRM\EmailTemplateRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailTemplateController extends Controller
{
    public function index()
    {
        $data['page_title'] = 'Email Templates';
        $data['email_templates_menu'] = true;
        return view('CRM.email_templates.index')->with($data);
    }

    public function update(EmailTemplateRequest $request, EmailTemplate $email_template)
    {
        $variables = collect(EmailTemplate::$email_types)
            ->where("name", $request->get("email_type"))
            ->first()["variables"];

        $content_errors = [];
        $invalid_variables = [];
        preg_match_all("/\\{{(.*?)\\}}/", $request->get("content"), $matches);

        foreach ($matches[0] as $match) {

            if (!collect(EmailTemplate::$email_variables)->where("value", $match)->count()) {
                array_push($invalid_variables, $match);
            }
        }

        if (!empty($invalid_variables)) {
            array_push($content_errors, "The variable " . implode(', ', $invalid_variables) . " is invalid.");
        }

        $required_variables = [];

        foreach ($variables as $variable) {
            $needle = collect(EmailTemplate::$email_variables)->where("key", $variable)->first()["value"];

            if (!str_contains($request->get("content"), $needle)) {
                array_push($required_variables, $needle);
            }
        }

        if (!empty($required_variables)) {
            array_push($content_errors, "The variable " . implode(', ', $required_variables) . " is required.");
        }

        if (!empty($content_errors)) {
            return response()->json([
                config('response.status') => config('response.type.error'),
                config('response.errors') => ['content' => $content_errors]
            ]);
        }

        $email_template->update($request->all());
        $email_template->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}
