<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 3/6/2018
 * Time: 2:19 PM
 */

namespace App\Http\Controllers\CRM\Wallet;


use App\common\WalletDeposit;
use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Status;
use App\CRM\NinepineModels\Wallet;

use App\Http\Controllers\CRM\Accounts\AccountsController;
use App\Http\Controllers\CRM\NinePineController;
use App\Mail\WebDepositEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class WalletDepositController extends NinePineController
{


    public function index(Request $request)
    {
        $data['wallet_menu'] = true;
        $data['deposit_menu'] = true;
        $data['page_title'] = 'Deposit';
        $query = WalletDeposit::with('status', 'currency', 'account.wallet', 'account', 'depositinfo');

        if ($request->query('wallet_deposit_id')) {
            $data['walletDeposit'] =  $query->where('wallet_deposit_id', $request->query('wallet_deposit_id'))->get();
        } else {
            $data['walletDeposit'] = $query->get();
        }
        $data['in_app_currencies'] = Currency::where('app_currency', true)->get();
        $data['global_currencies'] = Currency::where('app_currency', false)
            ->orWhere('app_currency', null)->get();
        $data['pending'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Pending')->first();
        $data['approve'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Approve')->first();
        $data['deny'] = Status::where('status_type', 'WalletWithdrawal')->where('status_name', 'Denied')->first();

        $data['statuses'] = Status::where('status_type','=', 'WalletDeposit')
            ->where('status_name','<>', 'Pending')
            ->get(['status_id', 'status_name']);
        return view('CRM.wallet.deposit.index')->with($data);
    }


    public function updateCrmNotes(Request $request){
        $note = $request->input('deposit-note');
        $deposit_id  = $request->input('deposit_id');
        $walletDeposit = WalletDeposit::find($deposit_id);
        $walletDeposit->update(['crm_note' => $note]);
        $walletDeposit->save();
        $swal = trans('swal.wallet.deposit.success');


        return response()->json(compact('swal'));

    }
    public function processApproval(Request $request){
        $password = $request->input('password');
        if  (!\Hash::check($password, Auth::user()->password)) {
            //$response = array("error"=>"Invalid Password");
            $response = array("error", config('response.information') =>"Invalid password");
            return $response;
        }
        $approvalDescription = $request->input('approvalStatus');
        $deposit_id = $request->input('wallet_deposit_id');
        $user_id = $request->input('user_id');
        $note = $request->input('crm-note');
        $status = Status::where('status_type','=', 'WalletDeposit')
            ->where('status_name','=', $approvalDescription)
            ->first()->status_id;

        $walletDeposit = WalletDeposit::find($deposit_id);

        if($walletDeposit){
            $walletDeposit->update([
                'status_id' => $status,
                'crm_note' => $note,
                'updated_at' => Carbon::now()
            ]);
            $walletDeposit->save();
            
            $account = Accounts::find($user_id);
            $amount = $walletDeposit->amount;
            $currency = Currency::find($walletDeposit->currency_id);
            $source = Source::where('source_name', "WalletDeposit")->first();
           
            $resource_id = $deposit_id;
            $application = Applications::where('name', 'Capital7')->first();
            $type = Wallet::TYPE_CHARGE;
            if ($approvalDescription == config('DepositState.Denied'))
            {
                $_amount = $currency->currency_symbol . ' ' . number_format($amount, 8);
                Mail::send(new WebDepositEmail($account, $_amount, $note, $approvalDescription));

                $response = array("success", config('response.information') =>"Success!");
                    return $response;
            }    
            if($application){
                $wallet_ledger = Wallet::makeTransaction($account, $amount, $currency, $source, $resource_id, $application, $type, $walletDeposit->wallet->wallet_account_number);
                if($wallet_ledger){

                    $_amount = $currency->currency_symbol . ' ' . number_format($amount, 8);
                    Mail::send(new WebDepositEmail($account, $_amount, $note, $approvalDescription));

                    $response = array("success", config('response.information') =>"Success!");
                    return $response;
                }
                else{
                    $response = array("error", config('response.information') =>"Error in saving the wallet ledger! Please contact the admin.");
                    return $response;
                }
            }

        }else{
            $response = array("error", config('response.information') =>"Error in updating approval status. Please contact the admin!");
            return $response;

        }

        return;



    }


}