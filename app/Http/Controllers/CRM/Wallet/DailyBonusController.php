<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\Http\Controllers\Controller;
use App\Http\Requests\CRM\DailyBonusRequest;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\DailyBonus;
use Illuminate\Http\Request;

class DailyBonusController extends Controller
{
    public function dataTable(Request $request)
    {
        return dataTable($request, DailyBonus::query());
    }

    public function index()
    {
        $data['wallet_menu'] = true;
        $data['daily_bonus_menu'] = true;
        $data['page_title'] = 'Daily Bonus';
        $data['daily_bonuses'] = DailyBonus::all();
        $data['in_app_currencies'] = Currency::where('app_currency', true)->get();
        $data['types'] = DailyBonus::$types;
        return view('CRM.wallet.daily_bonus.index')->with($data);
    }

    public function store(DailyBonusRequest $request)
    {
        $daily_bonus = DailyBonus::where('currency_id', $request->get('currency_id'))
            ->where('type', $request->get('type'))
            ->first();

        if (!is_null($daily_bonus)) {
            return response()->json([
                config('response.status') => config('response.type.error'),
                config('response.errors') => [
                    'type' => [trans('validation.custom.type.currency_duplicate')]
                ]
            ]);
        }
        DailyBonus::create($request->all());

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(DailyBonusRequest $request, DailyBonus $daily_bonus)
    {
        $_daily_bonus = DailyBonus::where('currency_id', $request->get('currency_id'))
            ->where('type', $request->get('type'))
            ->where('daily_bonus_id', '!=', $daily_bonus->daily_bonus_id)
            ->first();

        if (!is_null($_daily_bonus)) {
            return response()->json([
                config('response.status') => config('response.type.error'),
                config('response.errors') => [
                    'type' => [trans('validation.custom.type.currency_duplicate')]
                ]
            ]);
        }
        $daily_bonus->update($request->all());
        $daily_bonus->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}
