<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\RegistrationRequest;
use App\CRM\NinepineModels\Avatar;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Registration;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;

class RegistrationController extends NinePineController
{
    public function dataTable(Request $request)
    {
        return dataTable($request, Registration::query());
    }

    public function index()
    {
        $data['wallet_menu'] = true;
        $data['registration_menu'] = true;
        $data['page_title'] = 'Registration';
        $data['registrations'] = Registration::all();
        $data['in_app_currencies'] = Currency::where('app_currency', true)->get();
        $data['types'] = Registration::getTypes();
        $data['avatars'] = Avatar::orderBy('updated_at', 'desc')->get();
        $data['active_id'] = Status::where('status_type', 'Avatar')
            ->where('status_name', 'Active')
            ->first()->status_id;
        $data['excluded_avatar_ids'] = Registration::pluck('avatar_id');
        return view('CRM.wallet.registration.index')->with($data);
    }

    public function store(RegistrationRequest $request)
    {
        $application_id = Applications::where('name', 'Baccarat')->first()->application_id;
        $request->request->add(compact('application_id'));
        Registration::create($request->all());

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(RegistrationRequest $request, Registration $registration)
    {
        $registration->update($request->all());
        $registration->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}
