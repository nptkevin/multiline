<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\TransferRequest;
//use App\Mail\WebTransferFromCrmEmail;
use App\CRM\NinepineModels\CrmTransfer;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class TransferController extends NinePineController
{
    public function __construct()
    {
        $this->middleware('auth:crm');
    }

    public function index(Request $request)
    {
        if(!in_array($request->type, ['Deposit', 'Withdraw', 'Redeem']))  $request->type = 'Deposit';

        $data['wallet_menu'] = true;
        $data[strtolower($request->type).'_menu'] = true;
        $data['page_title'] = $request->type;
        $data['in_app_currencies'] = Currency::all();
        $data['global_currencies'] = Currency::all();
        return view('CRM.wallet.transfer.index')->with($data);
    }

    public function dataTable(Request $request)
    {
        return dataTable($request, Accounts::withoutGuest(), ['first_name', 'last_name', 'country', 'birth_date']);
    }

    public function get_currencies(Request $request)
    {
        $account = Accounts::find($request->get('user_id'));

        return response()->view('CRM.wallet.transfer.forms.currency_options', [
            'crypto' => $account->wallet()
                ->whereIn('currency_id', Currency::crypto()->pluck('currency_id'))
                ->orderBy('Wallet.currency_id', 'asc')
                ->orderBy('Wallet.wallet_account_number', 'asc')
                ->get(),
            'global' => $account->wallet()
                ->whereIn('currency_id', Currency::crypto(false)->pluck('currency_id'))
                ->orderBy('Wallet.currency_id', 'asc')
                ->orderBy('Wallet.wallet_account_number', 'asc')
                ->get()
        ]);
    }

    public function transfer(TransferRequest $request)
    {
        $sender = Auth::guard('crm')->user();
        $mode = $request->mode;

        try {
            $receiver = Accounts::findOrFail($request->user_id);
            $currency = Currency::findOrFail($request->currency_id);
        } catch (\Exception $e) {
            return response()->json(swal(
                trans('swal.exception.title'),
                $e->getMessage(),
                trans('swal.exception.type')
            ));
        }

        if($mode == 'add') {
            $transfer_amount = $request->transfer_amount;
            $charge_type = Wallet::TYPE_CHARGE;
            $mode_title = 'Deposit';
            $mode_text = 'added';
        } else {
            $transfer_amount = -$request->transfer_amount;
            $charge_type = Wallet::TYPE_DISCHARGE;
            $mode_title = 'Withdraw';
            $mode_text = 'deducted';
        }

        try {
            DB::beginTransaction();

            $crm_transfer = CrmTransfer::create([
                'transfer_amount' => $transfer_amount,
                'currency_id' => $currency->currency_id,
                'crm_user_id' => $sender->id,
                'reason' => $request->reason,
                'user_id' => $receiver->id
            ]);

            //$application = Applications::where('name', 'Baccarat')->first();
            $source = Source::where('source_name', 'CRM')->first();
            $resource_id = $crm_transfer->crm_transfer_id;

            $ledger = Wallet::makeTransaction($receiver, $request->transfer_amount, $currency, $source, $resource_id, /* $application, */ $charge_type);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
                return response()->json([
                    "errors" => [
                        "transfer_amount" => ["Can't deduct wallet anymore." . $e->getMessage()]
                    ]
                ], 400);
        }

        $amount =  number_format($request->transfer_amount, 2) . ' ' . $currency->currency_symbol;

        if (!is_null($receiver->email)) {
            //Mail::send(new CrmTransferMail($receiver, $amount));
        }

        return response()->json(swal(
            trans('swal.transfer.success.title', [
                'mode_title' => $mode_title
            ]),
            trans('swal.transfer.success.html', [
                'mode' => $mode_text,
                'amount' => $amount,
                'link_to_route' => link_to_route('accounts.details', $receiver->getDisplayName(), [
                    $receiver, '#wallet'
                ])
            ]),
            trans('swal.transfer.success.type')
        ));
    }
}