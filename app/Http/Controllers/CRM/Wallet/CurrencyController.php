<?php

namespace App\Http\Controllers\CRM\Wallet;

use App\Http\Controllers\CRM\NinePineController;
use App\Http\Requests\CRM\CurrencyRequest;
use App\CRM\NinepineModels\Currency;
use Illuminate\Http\Request;

class CurrencyController extends NinePineController
{
    public function dataTable(Request $request)
    {
        return dataTable($request, Currency::query());
    }

    public function index()
    {
        $data['wallet_menu'] = true;
        $data['currency_menu'] = true;
        $data['page_title'] = 'Currency';
        return view('CRM.wallet.currency.index')->with($data);
    }

    public function store(CurrencyRequest $request)
    {
        Currency::resetDefaultRegistration($request->default_registration);
        Currency::create([
            'currency_name' => $request->currency_name,
            'currency_symbol' => $request->currency_symbol,
            'currency' => $request->currency,
            'default_registration' => $this->setDefaultRegistration($request->default_registration, $request->app_currency),
            'app_currency' => $this->setAppCurrency($request->app_currency)
        ]);

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(CurrencyRequest $request, Currency $currency)
    {
        Currency::resetDefaultRegistration($request->default_registration);
        $currency->update([
            'currency_name' => $request->currency_name,
            'currency_symbol' => $request->currency_symbol,
            'currency' => $request->currency,
            'default_registration' => $this->setDefaultRegistration($request->default_registration, $request->app_currency),
            'app_currency' => $this->setAppCurrency($request->app_currency)
        ]);
        $currency->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function setDefaultRegistration($boolA, $boolB)
    {
        $default = $boolA === '1' ? 1 : 0;
        return $this->setAppCurrency($boolB) ? 0 : $default;
    }

    public function setAppCurrency($bool)
    {
        return $bool === '1' ? 1 : 0;
    }
}
