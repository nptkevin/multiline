<?php

namespace App\Http\Controllers\CRM\CMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\CRM\CategoryRequest;
use App\CRM\NinepineModels\Category;
use App\CRM\NinepineModels\Status;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function dataTable(Request $request)
    {
        return dataTable($request, Category::with('parent', 'status'));
    }

    public function index()
    {
        $data['cms_menu'] = true;
        $data['category_menu'] = true;
        $data['page_title'] = 'Categories';
        $data['categories'] = Category::all();
        $data['statuses'] = Status::where('status_type', 'Category')->get();
        return view('CRM.cms.category.index')->with($data);
    }

    public function json(Request $request)
    {
        return response()->json(Category::toTree());
    }

    public function positions($parent)
    {
        $positions = Category::where('parent_category_id', $parent)
            ->orderBy('sort', 'asc')
            ->pluck('sort');

        return response()->json([
            config('response.status') => config('response.type.success'),
            config('response.data') => compact('positions')
        ]);
    }

    public function store(CategoryRequest $request)
    {
        $categories = Category::where('parent_category_id', $request->input('parent_category_id'))
            ->orderBy('sort', 'asc')
            ->get();

        foreach ($categories as $category) {
            if ($category->sort >= $request->input('sort')) {
                $category->sort = $category->sort + 1;
            }
            $category->save();
        }
        Category::create($request->all());

        return response()->json([
            config('response.status') => config('response.type.success')
        ], 201);
    }

    public function update(CategoryRequest $request, Category $category)
    {
        Category::reorder($request->input('parent_category_id'), $category->sort, $request->input('sort'));
        $category->update($request->all());
        $category->save();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }

    public function destroy(Category $category)
    {
        $affected = $category->getAffectedRelations();

        if (count($affected)) {
            return response()->json([
                config('response.status') => config('response.type.fail'),
                config('response.data') => compact('affected')
            ]);
        }
        $category->delete();

        return response()->json([
            config('response.status') => config('response.type.success')
        ]);
    }
}
