<?php

namespace App\Http\Controllers\web;

use App\common\LeagueInfo;
use App\common\LeagueName;
use App\common\Schedule;
use App\common\Scrape;
use App\common\Watchlist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TradeController extends Controller{
	public function __construct(){
		//
	}

	public function index(){
		$leagueinfo = LeagueInfo::all();
		$leaguename = LeagueName::orderBy('json_league_id', 'desc')
			->first();

		$fetched = [];
		$data['inplay'] = $data['today'] = $data['early'] = [];
		$watchlist = Watchlist::where(\DB::raw('to_char("created_at", \'YYYY-MM-DD\')'), date('Y-m-d'))
			->get();

		foreach($watchlist AS $row):
			$fetched[] = Scrape::where('league_name', $row->leaguename)
				->where('group_game_id', $row->group_game_id)
				->pluck('id')
				->toArray();
		endforeach;

		$fid = "";

		for($i = 0; $i < count($fetched); $i++):
			$fid .= $i > 0 ? "," : "";
			$fid .= implode(',', $fetched[$i]);
		endfor;

		$fid = $fid == "" ? [0] : explode(',', $fid);

		$lid = Watchlist::where(\DB::raw('to_char("created_at", \'YYYY-MM-DD\')'), date('Y-m-d'))
			->pluck('leagueinfo_id')
			->toArray();

		if(!empty($leaguename)):
			switch(strtoupper($leaguename->game_schedule)):
				case 'IN-PLAY':
					for($i = 0; $i < count($leaguename['leagueids']); $i++):
						if(!in_array($leaguename['leagueids'][$i], $lid))
							$data['inplay'][] = LeagueInfo::where('leagueinfo_id', $leaguename['leagueids'][$i])
								->whereNotIn('leagueinfo_id', $fid)
								->get();
					endfor;

					$leaguename = LeagueName::getBySchedule('today')
						->orderBy('json_league_id', 'desc')
						->first();

					if(!is_null($leaguename)):
						for($i = 0; $i < count($leaguename['leagueids']); $i++):
							if(!in_array($leaguename['leagueids'][$i], $lid))
								$data['today'][] = LeagueInfo::where('leagueinfo_id', $leaguename['leagueids'][$i])
									->whereNotIn('leagueinfo_id', $fid)
									->get();
						endfor;
					endif;
				break;

				case 'EARLY':
					for($i = 0; $i < count($leaguename['leagueids']); $i++):
						if(!in_array($leaguename['leagueids'][$i], $lid))
							$data['early'][] = LeagueInfo::where('leagueinfo_id', $leaguename['leagueids'][$i])
								->whereNotIn('leagueinfo_id', $fid)
								->get();
					endfor;
				break;

				default:
					for($i = 0; $i < count($leaguename['leagueids']); $i++):
						if(!in_array($leaguename['leagueids'][$i], $lid))
							$data['today'][] = LeagueInfo::where('leagueinfo_id', $leaguename['leagueids'][$i])
								->whereNotIn('leagueinfo_id', $fid)
								->get();
					endfor;
				break;
			endswitch;
		endif;

		$grpwatchlist = Scrape::whereIn('id', $fid)
			->orderBy('id', 'asc')
			->get();

		$arr = [];

		foreach($grpwatchlist AS $key => $row):
			$arr[$row->league_name][$row->group_game_id][] = $row;
			$arr[$row->league_name]['ggid'] = Scrape::where('league_name', $row->league_name)
				->where('group_game_id', $row->group_game_id)
				->orderBy('id', 'asc')
				->first()
				->group_game_id;
			$arr[$row->league_name]['lid'] = LeagueInfo::where('leaguename', $row->league_name)
				->orderBy('leagueinfo_id', 'desc')
				->first()
				->leagueinfo_id;
		endforeach;

		$data['watchlist'] = $arr;

		return view('trade')
			->with($data);
	}

	public function getData(){
		$data = [];

		return json_encode($data);
	}

	public function getLeagueData(Request $request){
		$data = [];
		$data['lid'] = $request->lid;
		$data['gs'] = $request->gs;
		$data['leaguetitle'] = LeagueInfo::where('leg_league_id', 'ILIKE', '%' . $request->lid)
			->where('game_schedule', strtoupper($request->gs))
			->first()
			->leaguename;

		$watchlist = Watchlist::where(\DB::raw('to_char("created_at", \'YYYY-MM-DD\')'), date('Y-m-d'))
			->get();
		$disregard = [];

		foreach($watchlist AS $row):
			$disregard = Scrape::where('league_name', $row->leaguename)
				->where('group_game_id', $row->group_game_id)
				->where('game_schedule', strtoupper($row->game_schedule))
				->pluck('id')
				->toArray();
		endforeach;

		$data['linfo'] = Scrape::where('league_name', 'ILIKE', $data['leaguetitle'])
			->whereNotIn('id', $disregard)
			->orderBy('id', 'asc')
			->get();

		return json_encode($data);
	}

	public function saveWatchlist(Request $request){
		$data = [];
		$leaguename = LeagueInfo::find($request->lid)
			->leaguename;

		$data['game_schedule'] = Scrape::where('league_name', $leaguename)
			->where('group_game_id', $request->ggid)
			->where('game_schedule', strtoupper($request->gs))
			->orderBy('id', 'asc')
			->first()
			->schedule;
		$data['game_schedule'] = str_replace('LIVE', '', $data['game_schedule']);

		$data['rowdata'] = [
			'leagueinfo_id' => $request->lid,
			'leaguename' => $leaguename,
			'group_game_id' => $request->ggid,
			'game_schedule' => $request->gs,
		];

		Watchlist::create($data['rowdata']);

		return json_encode($data);
	}

	public function removeWatchlist(Request $request){
		$data = [];
		$leaguename = LeagueInfo::find($request->lid)
			->leaguename;

		$data['rowdata'] = [
			'leagueinfo_id' => $request->lid,
			'leaguename' => $leaguename,
			'group_game_id' => $request->ggid,
			'game_schedule' => strtolower($request->gs),
		];

		Watchlist::where('leagueinfo_id', $request->lid)
			->where('leaguename', $leaguename)
			->where('group_game_id', $request->ggid)
			->where('game_schedule', strtolower($request->gs))
			->delete();

		return json_encode($data);
	}
}