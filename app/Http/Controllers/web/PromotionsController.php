<?php

namespace App\Http\Controllers\web;

use App\CRM\NinepineModels\News;
use App\CRM\NinepineModels\Status;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PromotionsController extends Controller{
	public function index(){
		$active_id = Status::where('status_name', 'Active')
			->where('status_type', 'News')
			->first()
			->status_id;

		$data['promotions'] = News::where('status_id', $active_id)
			->where('content_type', 'Promotions')
			->orderBy('created_at', 'desc')
			->get();

		return view('promotions')
			->with($data);
	}

	public function getProducts(){
		$active_id = Status::where('status_name', 'Active')
			->where('status_type', 'News')
			->first()
			->status_id;

		$data['products'] = News::where('status_id', $active_id)
			->where('content_type', 'Products')
			->orderBy('created_at', 'desc')
			->get();

		return view('products')
			->with($data);
	}

	public function postContent(Request $request){
		$data = News::where('news_id', $request->pid)
			->first();

		return $data;
	}
}