<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $search_term = $request->q;

        $data['search_term'] = $search_term;
        $data['title'] = 'Search Results for';
        return view('search')
            ->with($data);
    }
}
