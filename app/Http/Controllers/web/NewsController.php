<?php

namespace App\Http\Controllers\web;

use App\CRM\baccarat\Status;
use App\CRM\NinepineModels\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class NewsController extends Controller{
	public function getImage($image){
		$file = "uploads/news$image";

		if(!File::exists($file))
			abort(404);

		return response()->file($file);
	}

	public function imageUpload(Request $request){
		\Log::info(json_encode('asd'));

		try{
			if($request->hasFile('content_image')){
				$_dir = $request->get('dir');
				$dir = "public/news/$_dir";
				$image = $request->file('content_image');

				if($image->isValid()){
					$image_path = storage_path("app/$dir/") . $image->getClientOriginalName();

					if(!File::exists(dirname($image_path)))
						File::makeDirectory(dirname($image_path), 0775, true);

					$image->move(dirname($image_path), basename($image_path));
					$content_image_path = str_replace(storage_path('app/public'), '', $image_path);
					$url = $request->root() . $content_image_path;

					return response()->json([
						config('response.result') => config('response.success'),
						config('response.data') => compact('url')
					]);
				}
			}
			return response()->json([
				config('response.result') => config('response.failed'),
				config('response.information') => 'File is invalid or not found.'
			]);
		} catch(\Exception $ex){
			return response()->json([
				config('response.result') => config('response.failed'),
				config('response.information') => $ex->getMessage()
			]);
		}
	}

	public function renameDir(Request $request){
		$dir = $request->get('dir');
		$content_id = $request->get('content_id');
		$_dir = storage_path("app/public/news/$dir");

		if(File::exists($_dir))
			rename($_dir, storage_path("app/public/news/$content_id"));

		return response()->json([
			config('response.result') => config('response.success')
		]);
	}

	public function showAllNews(){
		$active = Status::where('status_type', 'News')
			->where('status_name', 'Active')
			->first()
			->status_id;
		$news = News::where('status_id', $active)->get();
		$data['news'] = $news;

		return view('news.index')->with($data);
	}

	public function getNews($filename){
		$active = Status::where('status_type', 'News')
			->where('status_name', 'Active')
			->first()
			->status_id;
		$link = $filename;
		$news = News::where('link', $link)
			->where('status_id', $active)
			->first();

		if(!$news)
			abort(404);

		$data['news'] = $news;

		$recentnews = News::where('news_id', '!=', $news->news_id)
			->where('status_id', $active)
			->get();
		$data['recentnews'] = $recentnews;

		return view('news.news_content')
			->with($data);
	}

}