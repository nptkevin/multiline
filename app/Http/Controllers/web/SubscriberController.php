<?php

namespace App\Http\Controllers\web;

use App\CRM\NinepineModels\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriberController extends Controller
{
    public function subscribe(Request $request)
    {
        if(Subscriber::where('email', $request->email)->count() > 0){
                return response()->json([
                    'result' => 'failed',
                    'message' => 'Email already exist.',
                    'data' => []
                ]);
        }

        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->is_opt_out = false;
        $subscriber->save();

        return response()->json([
            'result' => 'success',
            'message' => 'You have subscribed to offers.',
            'data' => []
        ]);

    }
}
