<?php

namespace App\Http\Controllers\web;

use App\common\Settings\Trade\BetColumns;
use App\common\Settings\Trade\BetSlip;
use App\common\Settings\Trade\MoreSettings;
use App\common\Settings\Trade\ViewSetup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsController extends Controller{
	public function index(){
		if(!session('user'))
			return redirect('/login');

		$data = [
			'football' => [
				[
					'toggle' => 'FT 1X2',
					'switch' => true,
				],
				[
					'toggle' => 'FT Handicap',
					'switch' => true,
				],
				[
					'toggle' => 'FT O/U',
					'switch' => true,
				],
				[
					'toggle' => 'FT O/E',
					'switch' => true,
				],
				[
					'toggle' => '1H 1X2',
					'switch' => true,
				],
				[
					'toggle' => '1H Handicap',
					'switch' => true,
				],
				[
					'toggle' => '1H O/U',
					'switch' => true,
				],
			],
			'slip' => [
				[
					'label' => 'Confirm Bet Placement',
					'toggle' => 'conf_betplacement',
					'switch' => false,
				],
				[
					'label' => 'Show Betslip Agent',
					'toggle' => 'show_betslip_agent',
					'switch' => true,
				],
				[
					'label' => 'Use Equivalent Bets',
					'toggle' => 'use_equiv_bets',
					'switch' => false,
				],
				[
					'label' => 'Allow Putting Offers Up On Exchanges',
					'toggle' => 'allow_offer_exchange',
					'switch' => true,
				],
				[
					'label' => 'Betslip: Auto-Insert Thousands Separator',
					'toggle' => 'autoinsert_thou_separator',
					'switch' => false,
				],
			],
		];

		$settings_id = ViewSetup::where('userid', session('user')->id)
			->first();
		$data['viewsetup'] = ViewSetup::where('userid', is_null($settings_id) ? 0 : $settings_id->userid)
			->first();
		$data['betcolumns'] = BetColumns::where('userid', is_null($settings_id) ? 0 : $settings_id->userid)
			->first();
		$data['betslip'] = BetSlip::where('userid', is_null($settings_id) ? 0 : $settings_id->userid)
			->first();
		$data['moresettings'] = MoreSettings::where('userid', is_null($settings_id) ? 0 : $settings_id->userid)
			->first();

		return view('settings')
			->with($data);
	}

	public function saveTradeSettings(Request $request){
		if(!session('user'))
			return json_encode([
				'status' => 401,
				'message' => 'Your session might have expired. Please log in.'
			]);

		$radios = [
			'ft_1x2',
			'ft_handicap',
			'ft_ou',
			'ft_oe',
			'fh_1x2',
			'fh_handicap',
			'fh_ou',
			'show_betslip_agent',
			'allow_offer_exchange',
			'conf_betplacement',
			'use_equiv_bets',
			'autoinsert_thou_separator',
			'sounds',
		];

		foreach($radios AS $row):
			$request->{$row} = is_null($request->{$row}) ? 0 : 1;
		endforeach;

		$request->bookie_price = json_encode($request->bookie_price);
		$ifExists = ViewSetup::where('userid', session('user')->id)
			->first();

		try{
			if(is_null($ifExists)){
				$var_viewsetup = new ViewSetup;
				$var_viewsetup = $var_viewsetup->getFillable();
				$array = [];

				foreach($var_viewsetup AS $row){
					$array[$row] = $request->{$row};
				}

				ViewSetup::create($array);

				$var_betcolumns = new BetColumns;
				$var_betcolumns = $var_betcolumns->getFillable();
				$array = [];

				foreach($var_betcolumns AS $row){
					$array[$row] = $request->{$row};
				}

				BetColumns::create($array);

				$var_betslip = new BetSlip;
				$var_betslip = $var_betslip->getFillable();
				$array = [];

				foreach($var_betslip AS $row){
					$array[$row] = $request->{$row};
				}

				BetSlip::create($array);

				$var_moresettings = new MoreSettings;
				$var_moresettings = $var_moresettings->getFillable();
				$array = [];

				foreach($var_moresettings AS $row){
					$array[$row] = $request->{$row};
				}

				MoreSettings::create($array);

				return json_encode([
					'status' => 200,
					'message' => "Settings Saved Successfully"
				]);
			} else{
				$var_viewsetup = ViewSetup::where('userid', $ifExists->userid)->first();
				$var_viewsetup = $var_viewsetup->getFillable();
				$array = [];

				foreach($var_viewsetup AS $row){
					$array[$row] = $request->{$row};
				}

				ViewSetup::where('userid', session('user')->id)->update($array);

				$var_betcolumns = BetColumns::where('userid', $ifExists->userid)->first();
				$var_betcolumns = $var_betcolumns->getFillable();
				$array = [];

				foreach($var_betcolumns AS $row){
					$array[$row] = $request->{$row};
				}

				BetColumns::where('userid', session('user')->id)->update($array);

				$var_betslip = BetSlip::where('userid', $ifExists->userid)->first();
				$var_betslip = $var_betslip->getFillable();
				$array = [];

				foreach($var_betslip AS $row){
					$array[$row] = $request->{$row};
				}

				BetSlip::where('userid', session('user')->id)->update($array);

				$var_moresettings = MoreSettings::where('userid', $ifExists->userid)->first();
				$var_moresettings = $var_moresettings->getFillable();
				$array = [];

				foreach($var_moresettings AS $row){
					$array[$row] = $request->{$row};
				}

				MoreSettings::where('userid', session('user')->id)->update($array);

				return json_encode([
					'status' => 200,
					'message' => "Settings Saved Successfully!"
				]);
			}
		} catch(Exception $e){
			return json_encode([
				'status' => 400,
				'message' => "Oops! Something went wrong."
			]);
		}
	}
}