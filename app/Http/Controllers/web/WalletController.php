<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Session;

class WalletController extends Controller
{
    public function index()
    {
        $data['title'] = 'Wallet';
        $data['wallet'] = array(
            'data' => array()
        );

        return view('wallet')
            ->with($data);
    }
}
