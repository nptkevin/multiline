<?php

namespace App\Http\Controllers\web;

use App\CRM\baccarat\Accounts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller{
	public function index(){
		if(!session()->has('user'))
        	return redirect('/');

		$data['user'] = Accounts::with('wallet')
			->where('id', \Auth::user()->id)
			->first();
		$data['shop'] = [
			'row1' => [
				[
					'img' => 'sp-ferrari',
					'label' => 'Ferrari',
					'pts' => 576431,
				],
				[
					'img' => 'sp-porsche',
					'label' => 'Porsche',
					'pts' => 343455,
				],
				[
					'img' => 'sp-ducati',
					'label' => 'Ducati',
					'pts' => 246785,
				],
			],
			'row2' => [
				[
					'img' => 'sp-p20pro',
					'label' => 'Huawei P20 Pro',
					'pts' => 3421,
				],
				[
					'img' => 'sp-samsungnote9',
					'label' => 'Samsung Note 9',
					'pts' => 5345,
				],
				[
					'img' => 'sp-iphonexsmax',
					'label' => 'iPhone XS Max',
					'pts' => 6104,
				],
			],
			'row3' => [
				[
					'img' => 'sp-lenovo',
					'label' => 'Lenovo Laptop',
					'pts' => 1502,
				],
				[
					'img' => 'sp-macbookpro',
					'label' => 'Macbook Pro',
					'pts' => 16374,
				],
				[
					'img' => 'sp-mssurfacepro',
					'label' => 'MS Surface Pro',
					'pts' => 8634,
				],
			],
			'row4' => [
				[
					'img' => 'sp-fifa',
					'label' => 'FIFA 2018',
					'video' => '<iframe src="https://www.youtube.com/embed/G7Y1M2532BI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="width: 100%; height: 30vw; max-height: 400px;"></iframe>',
				],
				[
					'img' => 'sp-nba',
					'label' => 'NBA 2018',
					'video' => '<iframe src="https://www.youtube.com/embed/YN6QK_GaaCM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="width: 100%; height: 30vw; max-height: 400px;"></iframe>',
				],
				[
					'img' => 'sp-nfl',
					'label' => 'NFL 2018',
					'video' => '<iframe src="https://www.youtube.com/embed/AZfCQ04n_rU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="width: 100%; height: 30vw; max-height: 400px;"></iframe>'
				],
			],
			'row5' => [
				[
					'img' => 'sp-spin',
					'label' => 'SPIN2WIN',
				],
			],
		];

		return view('dashboard')
			->with($data);
	}
}