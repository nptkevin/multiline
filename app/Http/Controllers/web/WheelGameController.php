<?php

namespace App\Http\Controllers\web;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\Currency;
use App\CRM\NinepineModels\GameResultTransfer;
use App\CRM\NinepineModels\Source;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WheelGameController extends Controller
{
    /**
     * Spins the wheel and award points
     *  (will upgrade to server logic random spin)
     */
    public function spin(Request $request)
    {
        if(!session()->has('user'))  return redirect('/');

        $game_name = $request->game_name;
        $points = (double) $request->points;


        try {
            $receiver = Accounts::findOrFail(Auth::user()->id);
            $currency = Currency::getDefault();
        } catch (\Exception $e) {
            return response()->json(swal(
                trans('swal.exception.title'),
                $e->getMessage(),
                trans('swal.exception.type')
            ));
        }

        $wheel_game_controller = GameResultTransfer::create([
            'user_id' => $receiver->id,
            'currency_id' => $currency->currency_id,
            'game_name' => $request->game_name,
            'transfer_amount' => $request->points,
            'result' => $request->result,
            'notes' => 'From Spin2Win'
        ]);

        $source = Source::where('source_name', 'GameResult')->first();
        $resource_id = $wheel_game_controller->game_result_transfer_id;

        if($request->result == 'win') {
            $charge_type = Wallet::TYPE_CHARGE;
        } else {
            $charge_type = Wallet::TYPE_DISCHARGE;
        }


        try {
            $wallet_ledger = Wallet::makeTransaction($receiver, $request->points, $currency, $source, $resource_id, /* $application, */ $charge_type);
            $balance = $wallet_ledger->balance;
        } catch (\Exception $e) {
            return response()->json([
                "errors" => [
                    "transfer_amount" => ["Can't deduct wallet anymore."]
                ]
            ], 400);
        }

        return response()->json([
            'result' => 'success',
            'data' => [
                'new_balance' => $balance
            ]
        ]);



    }
}
