<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\web\LoginRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;
use Redirect;


class UserController extends Controller{
	public function loginProcess(LoginRequests $request){
		$api = api(env('API_URL') . config('api.endpoints.user.login'), 'POST', $request->all());

		if(array_key_exists('error', $api)){
			$response = [
				'result' => 'error',
				'message' => $api['message']
			];
		} else{

			Session::put('access_token', $api['access_token']);
			Session::put('refresh_token', $api['refresh_token']);

			$profile = api(env('API_URL') . config('api.endpoints.user.profile'), 'GET');
			Session::put('user', $profile);

			$response = [
				'result' => 'success',
				'message' => 'Login Successful'
			];
		}

		return json_encode($response);
	}

	public function userLogout(){
//		$logout = api(env('API_URL') . config('api.endpoints.user.logout'), 'POST');
		Session::put('access_token', '');
		Session::put('refresh_token', '');
		Session::put('user', '');

		return Redirect('/');
	}

	public function registerProcess(Request $request){
		$data = [
			'email' => $request->email,
			'birth_date' => $request->birthdate,
			'password' => $request->password,
			'repeat_password' => $request->repeat_password,
		];

		$response = Curl::to(env('API_URL') . config('api.endpoints.user.register'))
			->withHeader('Accept: application/json')
			->withData($data)
			->returnResponseObject()
			->post();
		$response = json_decode($response->content, true);

		if(array_key_exists('error', $response)){
			$errors = array();
			$errors['result'] = "error";
			$error = $response['error'];

			foreach($error as $key => $value){
				$errors['fields'][] = $key;
				$errors['errmsg'][] = $value[0];
			}

			return json_encode($errors);
		}

		$response = [
			'result' => 'success',
			'message' => 'Account Successfully Registered'
		];

		return json_encode($response);
	}

	public function profile()
    {
        $data['title'] = 'Personal info';
//        $data['user_profile'] = Session::get('user');
        return view('myinfo')
            ->with($data);
    }

    public function profileSave(Request $request)
    {

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $gender = $request->input('gender');
        $country = $request->input('country');
        $birth_date = $request->input('year') . '-' . $request->input('month') . '-' . $request->input('day');

        $user = Auth::user();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->name = $first_name . ' ' . $last_name;
        $user->gender = $gender;
        $user->country = $country;
        $user->birth_date = $birth_date;
        $user->save();

        Session::put('user', auth()->user()->toArray());

        return redirect('/user/myinfo');
    }
}