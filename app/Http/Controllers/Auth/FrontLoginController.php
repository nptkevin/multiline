<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;
use App\common\User;
use Auth;

class FrontLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function showLoginForm()
    {
        return redirect('/');
    }

    public function login(Request $request)
    {
      \Log::debug('actionlogin');

        // Validate the form data
        $validator = $this->validate($request, [
            'username'   => 'required',
            'password' => 'required|min:8'
        ],
        [
            'username.required' => 'Username is required',
            'password.required' => 'Password is required',
            'password.min' => 'Minimum length is 8'
        ]);

        $user = User::where('username', $request->username)->count();

        // Attempt to log the user in
        if(Auth::guard('web')->attempt(['username' => $request->username, 'password' => $request->password])){
            Session::put('user', auth()->user());

            // if successful, then redirect to their intended location
            // return [
            //     'result' => 'success',
            //     'message' => 'Login Successful'
            // ];

            return redirect('/trade');
            //return redirect()->intended(route('user.profile'));
        } else{
            $result = $user <= 0 ? "failed-1" : "failed-2";
            $message = $user <= 0 ? "Account does not exist" : "Invalid credentials";

            return [
                'result' => $result,
                'message' => $message
            ];

            return response('', 401);
        }

//        dd(auth()->user());
        // if unsuccessful, then redirect back to the login with the form data
        return redirect()->back()->withInput($request->only('username', 'remember'));
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/');
    }
}
