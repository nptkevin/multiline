<?php

namespace App\Http\Controllers\api;

use App\common\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Client;
use Response;

class UserController extends Controller{
	private $client;

	public function __construct(){
		$this->client = Client::find(3);
	}

	public function loginProcess(Request $request){
		try{
			$data = User::where('email', $request->email)->first();

			if(is_null($data))
				return array(
					'result' => 'error',
					'message' => 'E-mail not found'
				);

			if(!\Hash::check($request->password, $data->password))
				return array(
					'result' => 'error',
					'message' => 'Incorrect Password'
				);

			return $response = array(
				'result' => 'success',
				'message' => 'Login Successful',
				'data' => $data
			);
		} catch(Exception $e){
			return $response = array(
				'result' => 'error',
				'message' => 'Login Failed',
				'error-line' => $e->getLine(),
				'error-message' => $e->getMessage()
			);
		}
	}
}