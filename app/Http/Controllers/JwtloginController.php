<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;
class JwtloginController extends Controller
{
    //
    public function index(Request $request) {
        try {
            $token = $request->token;

            $validation = Curl::to(env('API_URL')."ValidateSSO?token=".$token."&application=".$request->application)
            ->returnResponseObject()
            ->get();
            $result = json_decode($validation->content,true);
        
            if ($result['result']=='success')
            {
                $data = json_decode($result['data'],true);
               $bearer = "Bearer ". $data['token'];
              
	           Session::put('access_token', $data['token']);
                $profile = api(endpoint('user.profile'), 'GET');
                Session::put('user', $profile);
                return Redirect('/');
                    
                        
            } else {
                return response()->json($result,200);
            }
            
         
        } catch (\Exception $e) {
                echo $e->getMessage();
        }
    }
}
