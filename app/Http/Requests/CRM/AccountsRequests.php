<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AccountsRequests extends FormRequest{
	public function authorize(){
		return Auth::guard('crm')->check();
	}

	public function rules(){

        switch($this->method())
        {
            case 'POST':
            {
                return [
                    "first_name" => "max:100",
                    "last_name" => "max:100",
//			"status_id" => "required",
                    "username" => "required|regex:/^[a-zA-Z0-9]+$/u|size:8|unique:bit.users",
                    "password" => ["required", "min:6", function ($attribute, $value, $fail) {

                        if(!preg_match('/[A-Z]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[a-z]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[0-9]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[&@!#+]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                    }
                    ],
                ];
            }
            case 'PUT':
            {
                return [
                    "first_name" => "max:100",
                    "last_name" => "max:100",
                    "password" => ["nullable", "min:6", function ($attribute, $value, $fail) {

                        if(!preg_match('/[A-Z]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[a-z]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[0-9]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                        if(!preg_match('/[!@#$%^&*]/', $value)){
                            $fail($attribute . ' must contain 1 uppercase, 1 lowercase, 1 special character.');
                        }

                    }
                    ],
                    "email" => "max:100|email",
                ];
            }
            default:break;
        }




	}
}