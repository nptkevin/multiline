<?php

namespace App\Http\Requests\CRM;

use App\CRM\NinepineModels\News;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PromotionsRequests extends FormRequest{
	public function authorize(){
		return true;
	}

	public function rules(){
		$title = $this->title;
		$content_type = $this->content_type;
		$duplicate = News::where('content_type', $content_type)
				->where('title', $title)
				->count();

		switch($this->method()){
			case "POST":
				$required = "required|";
				$unique = $duplicate >= 1 ? "|unique:bit_crm.News,title" : "";
				break;
			case "PUT":
				$required = "";
				$unique = $duplicate >= 1 ? "|unique:bit_crm.News,title," . $this->news_id . ",news_id" : "";
				break;
		}

		\Log::info(json_encode($unique));

		return [
			'title' => 'required' . $unique,
			'content_type' => 'required',
			'status_id' => 'required',
			'content' => 'required',
			'coverphoto' => $required . "image|max:2048|mimes:png,PNG,jpg,JPG,jpeg,JPEG,gif,GIF|dimensions:max_width=1170,max_height=383"
		];
	}

	public function messages(){
		return [
			'title.required' => 'Title is required.',
			'title.unique' => 'Title already exists.',
			'content_type.required' => 'Content Type is required.',
			'status_id.required' => 'Status is required.',
			'content.required' => 'Content is required.',
			'coverphoto.required' => 'Cover Photo is required.',
			'coverphoto.image' => 'Invalid image file.',
			'coverphoto.dimensions' => 'Image Dimensions exceeded maximum limit: 1170 x 383'
		];
	}
}