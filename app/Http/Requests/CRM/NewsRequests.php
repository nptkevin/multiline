<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequests extends FormRequest{
	public function authorize(){
		return true;
	}

	public function rules(){
		return [
			'content_image' => "image|max:2048|mimes:png,PNG,jpg,JPG,jpeg,JPEG,gif,GIF|dimensions:max_width=1170,max_height=383",
		];
	}

	public function response(array $errors){
		return response()->json([
			config('response.status') => config('response.type.error'),
			config('response.errors') => $errors
		]);
	}
}