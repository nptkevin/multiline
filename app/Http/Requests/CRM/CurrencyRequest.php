<?php

namespace App\Http\Requests\CRM;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'currency_name' => 'required|min:2|max:100',
                    'currency_symbol' => 'required|max:6',
                    'currency' => 'required|max:16|unique:bit_crm.Currency',
                    'default_registration' => '',
                    'confirm' => 'required_if:default_registration,1'
                ];
            }
            case 'PUT': {
                $currency = $this->route('currency');
                $currency_id = $currency->currency_id;
                $default_registration = intval($currency->default_registration);

                return [
                    'currency_name' => 'required|min:2|max:100',
                    'currency_symbol' => 'required|max:6',
                    'currency' => "required|max:16|unique:bit_crm.Currency,currency,$currency_id,currency_id",
                    'default_registration' => '',
                    'confirm' => "required_unless:default_registration,$default_registration"
                ];
            }
            default:
                break;
        }
    }

    public function response(array $errors)
    {
        return response()->json([
            config('response.status') => config('response.type.error'),
            config('response.errors') => $errors
        ]);
    }
}
