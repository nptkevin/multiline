<?php

namespace App\common\Settings\Trade;

use Illuminate\Database\Eloquent\Model;

class BetSlip extends Model{
	protected $connection = 'bit';
	protected $table = 'settings_trade_betslip';
	protected $primaryKey = 'id';

	protected $fillable = [
		'userid',
		'conf_betplacement',
		'show_betslip_agent',
		'use_equiv_bets',
		'allow_offer_exchange',
		'autoinsert_thou_separator',
		'betslip_multi_acc',
		'betslip_price',
		'max_bet',
		'stake_def',
		'betslip_price_def',
		'expiry_def',
	];
}