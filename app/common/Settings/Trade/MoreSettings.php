<?php

namespace App\common\Settings\Trade;

use Illuminate\Database\Eloquent\Model;

class MoreSettings extends Model{
	protected $connection = 'bit';
	protected $table = 'settings_trade_moresettings';
	protected $primaryKey = 'id';

	protected $fillable = [
		'userid',
		'sounds',
		'bookie_comp',
		'bookie_price',
		'reselect_acc',
	];
}