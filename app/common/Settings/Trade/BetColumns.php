<?php

namespace App\common\Settings\Trade;

use Illuminate\Database\Eloquent\Model;

class BetColumns extends Model{
	protected $connection = 'bit';
	protected $table = 'settings_trade_betcolumns';
	protected $primaryKey = 'id';

	protected $fillable = [
		'userid',
		'ft_1x2',
		'ft_handicap',
		'ft_ou',
		'ft_oe',
		'fh_1x2',
		'fh_handicap',
		'fh_ou',
	];
}