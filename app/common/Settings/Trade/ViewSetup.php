<?php

namespace App\common\Settings\Trade;

use Illuminate\Database\Eloquent\Model;

class ViewSetup extends Model{
	protected $connection = 'bit';
	protected $table = 'settings_trade_viewsetup';
	protected $primaryKey = 'id';

	protected $fillable = [
		'userid',
		'viewsetup',
	];
}