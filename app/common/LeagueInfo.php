<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class LeagueInfo extends Model{
	protected $connection = 'bit';
	protected $table = 'leagueinfo';
	protected $primaryKey = 'leagueinfo_id';

	public static function getBySchedule($schedule){
		$schedule = strtoupper($schedule);

		return self::where('game_schedule', $schedule)
			->orderBy('leaguename', 'asc');
	}
}