<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class LeagueName extends Model{
	protected $connection = 'bit';
	protected $table = 'json_league_name';
	protected $primaryKey = 'json_league_id';

	protected $casts = [
		'leagueids' => 'array'
	];

	public static function getBySchedule($schedule){
		$schedule = strtoupper($schedule);

		return self::where('game_schedule', $schedule);
	}
}