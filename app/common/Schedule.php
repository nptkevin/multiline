<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model{
	protected $connection = 'bit';
	protected $table = 'json_team_schedule';
	protected $primaryKey = 'id';

	protected $casts = [
		'schedule_team_ids' => 'array'
	];

	public static function getBySchedule($schedule){
		$schedule = strtoupper($schedule);

		return self::where('game_schedule', $schedule);
	}
}