<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class Scrape extends Model{
	protected $connection = 'bit';
	protected $table = 'soccer_scraped';
	protected $primaryKey = 'id';
}