<?php

namespace App\common;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable{
	use Notifiable;

	/**
	 * Name of the Model Table.
	 *
	 * @var 	$table 			string
	**/
	protected $table = 'users';

	/**
	 * Primary Key of the Model Table.
	 *
	 * @var 	$primaryKey 	string
	**/
	protected $primaryKey = 'id';

	/**
	 * The Model Table attributes that are mass assignable.
	 *
	 * @var 	$fillable 		array
	**/
	protected $fillable = [
		'name',
		'username',
		'email',
		'password',
		'gender',
		'birth_date',
		'status_id',
	];

	/**
	 * The Model Table attributes that should be hidden for arrays.
	 *
	 * @var 	$hidden 		array
	**/
	protected $hidden = [
		'password',
		'remember_token'
	];
}