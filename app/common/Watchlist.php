<?php

namespace App\common;

use Illuminate\Database\Eloquent\Model;

class Watchlist extends Model{
	protected $connection = 'bit';
	protected $table = 'watchlist';
	protected $primaryKey = 'id';
	protected $fillable = [
		'leagueinfo_id',
		'leaguename',
		'group_game_id',
		'game_schedule',
	];

	public static function getWatchlistIdBySchedule($schedule, $ggid){
		return self::where('game_schedule', $schedule)
			->where('group_game_id', $ggid)
			->orderBy('leagueinfo_id', 'asc')
			->get();
	}
}