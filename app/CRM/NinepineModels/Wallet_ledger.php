<?php

namespace App\CRM\NinepineModels;

use App\API\Tradeprofile;
use App\common\WalletWithdrawal;
use App\common\WalletDeposit;
use App\common\TradeMarketTicketTransfer;
use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use App\CRM\baccarat\MembershipCharges;
use App\CRM\baccarat\RegularCharges;
use App\CRM\baccarat\Transaction;
use Illuminate\Database\Eloquent\Model;

class Wallet_ledger extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Wallet_ledger';
    protected $primaryKey = 'wallet_ledger_id';

    protected $fillable = [
        'wallet_id',
        'source_id',
        'resource_id',
        'debit',
        'credit',
        'application_id',
        'balance',
        'json'
    ];

    protected $casts = [
        'balance' => 'double',
        // 'debit' => 'double',
        // 'credit' => 'double',
    ];

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('wallet_id', $this->fillable),
                array_search('source_id', $this->fillable),
                array_search('resource_id', $this->fillable),
                array_search('application_id', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->whereRaw('"' . $this->table . '"."' . $value . '"::text ILIKE \'%' . $search . '%\'');
            }
            $query->orWhereRaw('"' . $this->table . '"."' . $value . '"::text ILIKE \'%' . $search . '%\'');
        }
        return $query;
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id');
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

    public function crm_transfer_resource()
    {
        return $this->belongsTo(CrmTransfer::class, 'resource_id', 'crm_transfer_id');
    }

    public function game_result_resource()
    {
        return $this->belongsTo(GameResultTransfer::class, 'resource_id', 'game_result_transfer_id');
    }

    public function wallet_transfer_resource()
    {
        return $this->belongsTo(WalletTransfer::class, 'resource_id', 'wallet_transfer_id');
    }

    public function charge_resource()
    {
        return $this->belongsTo(RegularCharges::class, 'resource_id', 'charge_id');
    }

    public function transaction_resource()
    {
        return $this->belongsTo(Transaction::class, 'resource_id', 'transaction_id');
    }

    public function membership_resource()
    {
        return $this->belongsTo(MembershipCharges::class, 'resource_id', 'membership_id');
    }

    public function daily_bonus_resource()
    {
        return $this->belongsTo(DailyBonus::class, 'resource_id', 'daily_bonus_id');
    }

    public function application()
    {
        return $this->belongsTo(Applications::class, 'application_id');
    }
}
