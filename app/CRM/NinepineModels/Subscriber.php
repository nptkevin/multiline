<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Subscribers';
    protected $primaryKey = 'subscriber_id';


}
