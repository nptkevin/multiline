<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class BankInformation extends Model
{
    protected $connection = 'bit_crm';
	protected $table = 'Depositinfo';
	protected $primaryKey = 'depositinfo_id';
	protected $fillable =[
		'information',
		'currency_id',
        'status_id',
        'default_account'
	];
	public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('currency_id', $this->fillable),
                array_search('status_id', $this->fillable),
                array_search('default_account', $this->fillable)
               
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }
}
