<?php

namespace App\CRM\NinepineModels;

use App\CRM\baccarat\Applications;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class Avatar extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Avatar';
    protected $primaryKey = 'avatar_id';

    protected $fillable = [
        'name',
        'avatar',
        'application_id',
        'type',
        'status_id'
    ];

    public static $types = [
        'Register',
        'Guest',
        'Membership',
        'Market',
        'Promotion'
    ];

    public static $size = [
        'width' => 1024,
        'height' => 1024
    ];

    const DEFAULT_AVATAR = 'DEFAULT';

    public function application()
    {
        return $this->belongsTo(Applications::class, 'application_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function registration()
    {
        return $this->hasMany(Registration::class);
    }

    public function scopeSearch($query, $search)
    {
        return $query->where('name', 'ILIKE', "%$search%");
    }

    public function membership_avatar()
    {
        return $this->hasMany(MembershipAvatar::class);
    }

    public function makeAvatar(UploadedFile $image)
    {
        if ($image->isValid()) {
            $dir = "public/avatars/$this->avatar_id";
            $original_path = storage_path("app/") . $image->store("$dir/original");
            $filename = basename($original_path);
            $width = self::$size['width'];
            $height = self::$size['height'];

            $_1024x1024_path = storage_path("app/$dir/$width" . "x$height/") . $filename;

            if (!File::exists(dirname($_1024x1024_path))) {
                File::makeDirectory(dirname($_1024x1024_path), 0775, true);
            }
            $_1024x1024 = Image::make($original_path)
                ->resize($width, $height)
                ->save($_1024x1024_path);

            $_100x100_path = storage_path("app/$dir/100x100/") . $filename;

            if (!File::exists(dirname($_100x100_path))) {
                File::makeDirectory(dirname($_100x100_path), 0775, true);
            }
            $_100x100 = Image::make($original_path)
                ->resize(100, 100)
                ->save($_100x100_path);

            $response = Curl::to(config('api.url') . "avatars/upload")
                ->withData([
                    'avatar_id' => $this->avatar_id
                ])
                ->withFile('avatar_1024x1024', $_1024x1024_path, $_1024x1024->mime(), $filename)
                ->withFile('avatar_100x100', $_100x100_path, $_100x100->mime(), $filename)
                ->returnResponseObject()
                ->post();
            $content = collect(json_decode($response->content, true));

            if ($content->get('result') == "success") {
                deleteDir(storage_path("app/$dir"));
                $this->avatar = $content->get('data')['url'];
                return $this->save();
            }
            Log::error('API: ' . title_case($content->get('result')) . ': ' . $content->get('message'));
        }
        return false;
    }
}
