<?php

namespace App\CRM\NinepineModels;

use App\common\BankInfo;
use App\common\TradeMarketTicketTransfer;
use App\common\WalletDeposit;
use App\common\WalletWithdrawal;
use App\CRM\baccarat\Accounts;
use App\CRM\baccarat\Applications;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Wallet';
    protected $primaryKey = 'wallet_id';

    protected $fillable = [
        'balance',
        'currency_id',
        'source_id',
        'user_id'
    ];

    protected $casts = [
        'balance' => 'double',
    ];

    const TYPE_CHARGE = 'CHARGE';
    const TYPE_DISCHARGE = 'DISCHARGE';

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id')->select([
            "id",
            "name",
            "email",
            "username",
            "account_image"
        ]);
    }

    public function source()
    {
        return $this->belongsTo(Source::class, 'source_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'wallet_id');
    }


    public static function makeTransaction(Accounts $receiver, $amount, Currency $currency, Source $source, $resource_id, /* Applications $application, */ $type, $wallet_account_number = null)
    {
        $wallet = null;
        $debit = doubleval(0);
        $credit = doubleval(0);

        if (!$receiver->wallet()->count()) {

            if($type == self::TYPE_DISCHARGE) {
                // no account yet but already deducted
                throw new \Exception('ERR_NEW_WALLET_DEDUCT');
            }

            $wallet = $receiver->wallet()->create([
                'balance' => $amount,
                'currency_id' => $currency->currency_id
            ]);
        } else {
            $wallet = $receiver->wallet()->where('currency_id', $currency->currency_id)->first();

            if (is_null($wallet)) {
                $wallet = $receiver->wallet()->create([
                    'balance' => $amount,
                    'currency_id' => $currency->currency_id
                ]);
            } else {
                if ($type == self::TYPE_CHARGE) {
                    $wallet->balance += $amount;
                } else {
                    if($wallet->balance < $amount){
                        throw new \Exception('ERR_WALLET_DEDUCT_EXCEED');
                    }
                    $wallet->balance -= $amount;
                }
                $wallet->save();
            }
        }

        if ($type == self::TYPE_CHARGE) {
            $debit = $amount;
        } else {
            $credit = $amount;
        }

        $mode = $type==self::TYPE_CHARGE?'Deposited':'Withdrawn';
        $json_text = [
            $mode => $amount,
            'time' => Carbon::now()->format('d/m/Y H:iA')
        ];
        $json_text = json_encode($json_text);

        return Wallet_ledger::create([
            'wallet_id' => $wallet->wallet_id,
            'source_id' => $source->source_id,
            'resource_id' => $resource_id,
            'debit' => $debit,
            'credit' => $credit,
            'json_data_output' => $json_text,
            // 'application_id' => $application->application_id,
            'balance' => $wallet->balance
        ]);

    }

}
