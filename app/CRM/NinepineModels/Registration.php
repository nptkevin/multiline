<?php

namespace App\CRM\NinepineModels;

use App\CRM\baccarat\Applications;
use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'RegisterWallet';
    protected $primaryKey = 'registration_id';

    protected $fillable = [
        'currency_id',
        'avatar_id',
        'amount',
        'type',
        'application_id'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function application()
    {
        return $this->belongsTo(Applications::class, 'application_id');
    }

    public function avatar()
    {
        return $this->belongsTo(Avatar::class, 'avatar_id');
    }

    public static function getTypes()
    {
        $types = [];
        $_types = self::pluck('type')->all();

        if (!in_array( 'Register', $_types)) {
            array_push($types, 'Register');
        }

        if (!in_array( 'Guest', $_types)) {
            array_push($types, 'Guest');
        }
        return $types;
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'registration_id');
    }
}
