<?php

namespace App\CRM\NinepineModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MarketplaceAvatar extends Model{
    protected $connection = 'bit_crm';
    protected $table = 'MarketplaceAvatar';
    protected $primaryKey ='marketplace_avatar_id';
    protected $fillable = [
		'avatar_id',
		'avatar_amount',
		'avatar_currency',
		'valid_date',
		'status_id'
	];

	public function currencies(){
		return $this->belongsTo('App\NinepineModels\Currency', 'avatar_currency');
	}

	public function status(){
		return $this->belongsTo('App\NinepineModels\Status', 'status_id');
	}

	public function avatars(){
		return $this->belongsTo('App\NinepineModels\Avatar', 'avatar_id');
	}

	public function getValidDateAttribute($value){
		return ($value) ? Carbon::parse($value)->format('F d, Y') : "";
	}

	public function scopeSearch($query, $search){
		return $query->where('avatar', 'ILIKE', "%$search%");
	}
}
