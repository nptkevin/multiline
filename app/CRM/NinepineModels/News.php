<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class News extends Model{
	protected $connection = 'bit_crm';
	protected $table = 'News';
	protected $primaryKey = 'news_id';

	protected $fillable = [
		'title',
		'content',
		'status_id',
		'coverphoto',
		'link',
		'content_type',
	];

	public static $content_types = [
		'Products',
		'Promotions'
	];

	public function status(){
		return $this->belongsTo(Status::class, 'status_id');
	}

	public function scopeSearch($query, $search, $cols = null){
		if (is_null($cols)){
			$except = [
				array_search('status_id', $this->fillable)
			];

			$cols = array_except($this->fillable, $except);
		}

		foreach($cols as $key => $value){
			if($key == 0){
				$query->where($value, 'ILIKE', "%$search%");
			}

			$query->orWhere($value, 'ILIKE', "%$search%");
		}

		return $query;
	}

	public static function makeImage(UploadedFile $image, $dir){
		if($image->isValid()){
			$path = "$dir/" . $image->getClientOriginalName();

			if(!File::exists(dirname($path))){
				File::makeDirectory(dirname($path), 0777, true);
			}

			$image->move(dirname($path), basename($path));
			$_image = Image::make($path)->save($path);

			return $image->getClientOriginalName();
		}

		return null;
	}
}