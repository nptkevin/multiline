<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class DepositInfo extends Model{
	protected $connection = 'bit_crm';
	protected $table = 'Depositinfo';
	protected $primaryKey = 'depositinfo_id';
}