<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class GameResultTransfer extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'GameResultTransfer';
    protected $primaryKey = 'game_result_transfer_id';

    protected $fillable = [
        'user_id',
        'game_id',
        'game_name',
        'currency_id',
        'transfer_amount',
        'result',
        'notes',
        'status_id',
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

}
