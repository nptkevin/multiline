<?php

namespace App\CRM\NinepineModels;

use App\API\Tradeprofile;
use App\common\TradeMarketTicket;
use App\common\TradeMarketTicketHistory;
use App\common\TradeMarketTicketTransfer;
use App\common\WalletDeposit;
use App\common\WalletWithdrawal;
use App\CRM\User;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Statuses';
    protected $primaryKey = 'status_id';
    protected $fillable = ['status_type', 'status_name'];

    public function ListByType()
    {
        return $this->groupBy('status_type')
            ->pluck('status_type');
    }

    public function getStatusOf($type){
        $data = $this->where('status_type', '=', $type)
            ->get(array('status_id', 'status_name'));

        return $data;
    }

    public function user()
    {
        return $this->hasMany(User::class, 'status_id');
    }

    public function trade_market_ticket_transfer()
    {
        return $this->hasMany(TradeMarketTicketTransfer::class, 'status_id');
    }

    public function avatar()
    {
        return $this->hasMany(Avatar::class, 'status_id');
    }

    public function trade_profile()
    {
        return $this->hasMany(Tradeprofile::class, 'status_id');
    }

    public function category()
    {
        return $this->hasMany(Category::class, 'status_id');
    }

    public function trade_market_ticket()
    {
        return $this->hasMany(TradeMarketTicket::class, 'ticket_status', 'status_id');
    }

    public function trade_market_ticket_history()
    {
        return $this->hasMany(TradeMarketTicketHistory::class, 'ticket_status', 'status_id');
    }

    public function content()
    {
        return $this->hasMany(Content::class, 'status_id');
    }

    public function wallet_deposit()
    {
        return $this->hasMany(WalletDeposit::class, 'status_id');
    }

    public function wallet_withdrawal()
    {
        return $this->hasMany(WalletWithdrawal::class, 'status_id');
    }

    public function getAffectedRelations()
    {
        $affected = [];

        if ($this->user()->count()) {
            array_push($affected, [
                'relation' => 'user',
                'count' => $this->user()->count()
            ]);
        }

        if ($this->avatar()->count()) {
            array_push($affected, [
                'relation' => 'avatar',
                'count' => $this->avatar()->count()
            ]);
        }
        return $affected;
    }

    public static function getIdOf($type, $name)
    {
        return self::where("status_type", $type)
            ->where("status_name", $name)
            ->first()
            ->status_id;
    }
}
