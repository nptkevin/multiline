<?php

namespace App\CRM\NinepineModels;

use App\CRM\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Roles';
    protected $primaryKey = 'role_id';

    protected $fillable = [
        'role_name',
        'description',
        'created_by'
    ];

    public function user_role()
    {
        return $this->hasOne(UserRole::class, 'role_id');
    }

    public function role_module()
    {
        return $this->hasOne(RoleModule::class, 'role_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
