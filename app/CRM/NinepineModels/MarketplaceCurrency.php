<?php

namespace App\CRM\NinepineModels;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Ixudra\Curl\Facades\Curl;

class MarketplaceCurrency extends Model{
    protected $connection = 'bit_crm';
    protected $table = 'MarketplaceCurrency';
    protected $primaryKey ='marketplace_currency_id';
    protected $fillable = [
        'image',
		'item_amount',
		'item_currency',
		'purchase_amount',
		'purchase_currency',
		'application_id',
		'expiration_date',
		'status_id'
	];

    public static $size = [
        'width' => 1024,
        'height' => 1024
    ];

    const DEFAULT_IMAGE_1024x1024 = '/CRM/Capital7-1.0.0/img/marketplace/currency/default/1024x1024/otuojRapL1OkHzGUzxQwJyqTIWaFuVVQyORfOoSS.png';

    public function status(){
        return $this->belongsTo('App\NinepineModels\Status', 'status_id');
    }

    public function applications(){
        return $this->belongsTo('App\baccarat\Applications', 'application_id');
    }

    public function itemcurrencies(){
        return $this->belongsTo('App\NinepineModels\Currency', 'item_currency');
    }

    public function purchasecurrencies(){
        return $this->belongsTo('App\NinepineModels\Currency', 'purchase_currency');
    }

    public function getExpirationDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('F d, Y h:i A') : "";
    }

    public function scopeSearch($query, $search, $cols = null){
        foreach($cols as $key => $value){
            if($key == 0){
                $query->where($value, 'ILIKE', "%$search%");
            }

            $query->orWhere($value, 'ILIKE', "%$search%");
        }

        return $query;
    }

    public function makeImage(UploadedFile $image)
    {
        if ($image->isValid()) {
            $dir = "public/marketplace/currency/$this->marketplace_currency_id";
            $original_path = storage_path("app/") . $image->store("$dir/original");
            $filename = basename($original_path);
            $width = self::$size['width'];
            $height = self::$size['height'];

            $_1024x1024_path = storage_path("app/$dir/$width" . "x$height/") . $filename;

            if (!File::exists(dirname($_1024x1024_path))) {
                File::makeDirectory(dirname($_1024x1024_path), 0775, true);
            }
            $_1024x1024 = Image::make($original_path)
                ->resize($width, $height)
                ->save($_1024x1024_path);

            $_100x100_path = storage_path("app/$dir/100x100/") . $filename;

            if (!File::exists(dirname($_100x100_path))) {
                File::makeDirectory(dirname($_100x100_path), 0775, true);
            }
            $_100x100 = Image::make($original_path)
                ->resize(100, 100)
                ->save($_100x100_path);

            $response = Curl::to(config('api.url') . "marketplace/currency/upload")
                ->withData([
                    'marketplace_currency_id' => $this->marketplace_currency_id
                ])
                ->withFile('image_1024x1024', $_1024x1024_path, $_1024x1024->mime(), $filename)
                ->withFile('image_100x100', $_100x100_path, $_100x100->mime(), $filename)
                ->returnResponseObject()
                ->post();
            $content = collect(json_decode($response->content, true));

            if ($content->get('result') == "success") {
                deleteDir(storage_path("app/$dir"));
                $this->image = $content->get('data')['url'];
                return $this->save();
            }
            Log::error('API: ' . title_case($content->get('result')) . ': ' . $content->get('message'));
        }
        return false;
    }
}
