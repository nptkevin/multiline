<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Sources';
    protected $primaryKey = 'source_id';

    protected $fillable = [
        'source_name'
    ];

    public function wallet()
    {
        return $this->hasMany(Wallet::class);
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class);
    }

    public static function getIdByName($source_name)
    {
        return self::where('source_name', $source_name)->first()->source_id;
    }
}
