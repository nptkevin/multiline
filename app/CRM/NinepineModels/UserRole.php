<?php

namespace App\CRM\NinepineModels;

use App\CRM\User;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $fillable = [
        'role_id', 'user_id'
    ];

    protected $connection = 'bit_crm';
    protected $table = 'UserRoles';
    protected $primaryKey = 'user_role_id';

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
