<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class DailyBonus extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'DailyBonuses';
    protected $primaryKey = 'daily_bonus_id';

    protected $fillable = [
        'points',
        'currency_id',
        'type'
    ];

    public static $types = [
        'Register',
        'Guest'
    ];

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'daily_bonus_id');
    }
}
