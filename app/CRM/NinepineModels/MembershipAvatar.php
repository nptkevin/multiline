<?php

namespace App\CRM\NinepineModels;

use App\CRM\baccarat\MembershipCharges;
use Illuminate\Database\Eloquent\Model;

class MembershipAvatar extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'MembershipAvatar';
    protected $primaryKey = 'membership_avatar_id';

    protected $fillable = [
        'description',
        'avatar_id',
        'membership_id',
        'membership_avatar_id'
    ];

    public function membership_charges()
    {
        return $this->belongsTo(MembershipCharges::class, 'membership_id');
    }

    public function avatar()
    {
        return $this->belongsTo(Avatar::class, 'avatar_id');
    }
}
