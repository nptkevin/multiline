<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $connection = 'bit_crm';
    protected $table = 'Modules';
    protected $primaryKey = 'module_id';

    protected $fillable = [
        'controller_name',
        'module_name',
        'module_description',
        'can_add',
        'can_edit',
        'can_delete',
        'admin',
        'parent_id',
        'readonly'
    ];

    public function role_module()
    {
        return $this->hasOne(RoleModule::class, 'module_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id', 'module_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'module_id');
    }

    public function getAffectedRelations()
    {
        $affected = [];

        if ($this->role_module()->count()) {
            array_push($affected, [
                'relation' => 'role_module',
                'count' => $this->role_module()->count()
            ]);
        }

        if ($this->children()->count()) {
            array_push($affected, [
                'relation' => 'children',
                'count' => $this->children()->count()
            ]);
        }
        return $affected;
    }

    public static function checkParentRelationships(array $modules) {
        foreach ($modules as $module) {
            $module_from_model = Module::find($module['module_id']);
            $parent = $module_from_model->parent()->first();

            if (is_null($parent)) {
                continue;
            }
            $module_from_array = collect($modules)
                ->where('module_id', $module_from_model->parent_id)
                ->first();

            if ($module_from_array['readonly'] == 'false' || is_null($module_from_array)) {
                return [
                    'module' => $module_from_model->module_name . '/' . $module_from_model->controller_name,
                    'parent' => $parent->module_name . '/' . $parent->controller_name
                ];
            }
        }
        return null;
    }
}
