<?php

namespace App\CRM\NinepineModels;

use Illuminate\Database\Eloquent\Model;

class CRMDepositInfo extends Model{
	protected $connection = 'bit';
	protected $table = 'crmdepositinfo';
	protected $primaryKey = 'depositinfo_id';

    public function wallet_deposit()
    {
        return $this->hasMany(WalletDeposit::class, 'depositinfo_id');
    }
}