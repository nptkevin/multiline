<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    protected $connection = 'bit';
    protected $table = 'SecurityQuestions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'security_question',
        'status_id'
    ];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('status_id', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }
}
