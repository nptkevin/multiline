<?php

namespace App\CRM\baccarat;

use App\API\Tradeprofile;
use App\common\BankInfo;
use App\common\TradeMarketTicket;
use App\common\WalletDeposit;
use App\common\WalletWithdrawal;
use App\CRM\NinepineModels\CrmTransfer;
use App\CRM\NinepineModels\Wallet;
use App\CRM\NinepineModels\WalletTransfer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Accounts extends Model
{
    protected $connection = 'bit';
    protected $table = 'users';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'email',
        'first_name',
        'last_name',
        'country',
        'address',
        'username',
        'mobile',
        'password',
        'gender',
        'birth_date',
        'status_id'
    ];

    protected $casts = [
        'amount' => 'double'
    ];

    public static $genders = [
        'Male',
        'Female',
        'Other'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function bankInfo()
    {
        return $this->belongsTo(BankInfo::class, 'user_id', 'id');
    }

    public function wallet()
    {
        return $this->hasMany(Wallet::class, 'user_id', 'id');
    }

    public function trader_account()
    {
        return $this->hasMany(TradeMarketTicket::class, 'user_id', 'id');
    }

    public function inquiry_account()
    {
        return $this->hasMany(TradeMarketTicket::class, 'inquiry_by', 'id');
    }

    public function bank_info()
    {
        return $this->hasMany(BankInfo::class, 'user_id', 'id');
    }

    public function wallet_deposit()
    {
        return $this->hasMany(WalletDeposit::class, 'user_id', 'id');
    }

    public function trade_profile()
    {
        return $this->hasMany(Tradeprofile::class, 'user_id', 'id');
    }

    public function wallet_withdrawal()
    {
        return $this->hasMany(WalletWithdrawal::class, 'user_id', 'id');
    }

    public function crm_transfer()
    {
        return $this->hasMany(CrmTransfer::class, 'user_id', 'id');
    }

    public function wallet_transfer_sender()
    {
        return $this->hasMany(WalletTransfer::class, 'sender_id', 'id');
    }

    public function wallet_transfer_receiver()
    {
        return $this->hasMany(WalletTransfer::class, 'receiver_id', 'id');
    }

    public function trade_market_ticket_inquiry()
    {
        return $this->hasMany(TradeMarketTicket::class, 'inquiry_by', 'id');
    }

    public function trade_market_ticket_trader()
    {
        return $this->hasMany(TradeMarketTicket::class, 'user_id', 'id');
    }

    public function scopeSearch($query, $search, $cols = null)
    {
        if (is_null($cols)) {
            $except = [
                array_search('status_id', $this->fillable),
                array_search('gender', $this->fillable),
                array_search('password', $this->fillable),
                array_search('birth_date', $this->fillable)
            ];

            $cols = array_except($this->fillable, $except);
        }

        foreach ($cols as $key => $value) {
            if ($key == 0) {
                $query->where($value, 'ILIKE', "%$search%");
            }
            $query->orWhere($value, 'ILIKE', "%$search%");
        }
        return $query;
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    public function billing_address()
    {
        return $this->hasMany(BillingAddress::class, 'user_id');
    }

    public function payment_infos()
    {
        return $this->hasMany(PaymentInfo::class);
    }

    public function memberships()
    {
        return $this->belongsToMany(MembershipCharges::class, 'UserMemberships', 'user_id', 'membership_id');
    }

    public function getMembershipCountAttribute()
    {
        if (!array_key_exists('membershipCount', $this->relations)) {
            $this->load('membershipCount');
        }
        $related = $this->getRelation('membershipCount')->first();
        return ($related) ? $related->aggregate : 0;
    }

    public function user_membership_history()
    {
        return $this->hasMany(UserMembershipHistory::class, 'user_id', 'id');
    }

    public function user_membership()
    {
        return $this->hasMany(UserMembership::class, 'user_id', 'id');
    }

    public function user_application()
    {
        return $this->hasMany(UserApplication::class, 'user_id', 'id');
    }

    public function applications()
    {
        return $this->belongsToMany(Applications::class, 'UserApplications', 'user_id', 'application_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('F d, Y');
    }

    public function getBirthDateAttribute($value)
    {
        $return = ($value) ? Carbon::parse($value)->format('F d, Y') : "";

        return $return;
    }

    public function getDisplayName()
    {
        if (!is_null($this->username))
            return $this->username;
        else if (!is_null($this->email)) {
            return $this->email;
        } else if (!is_null($this->first_name) && !is_null($this->last_name)) {
            return $this->first_name . ' ' . $this->last_name;
        } else if (!is_null($this->mobile)) {
            return $this->mobile;
        }
        return null;
    }

    public function getTransactionActivity()
    {
        return $this->transactions()
            ->join('TransactionLogs', 'Transactions.transaction_id', '=', 'TransactionLogs.transaction_id')
            ->join('TransactionStatuses', 'TransactionLogs.transaction_status_id', '=', 'TransactionStatuses.transaction_status_id')
            ->join('TransactionDetails', 'Transactions.transaction_id', '=', 'TransactionDetails.transaction_detail_id')
            ->join('Charges', 'TransactionDetails.charge_id', '=', 'Charges.charge_id')
            ->join('ChargeTypes', 'Charges.charge_type_id', '=', 'ChargeTypes.charge_type_id')
            ->select([
                'TransactionLogs.transaction_log_id as transaction_log_id',
                DB::raw('to_char("TransactionLogs"."created_at", \'Month DD, YYYY/HH24:MI:SS\') as "transaction_log_created_at"'),
                'ChargeTypes.name as charge_type_name',
                'Charges.description as charge_description',
                'Charges.price as charge_price',
                'TransactionStatuses.description as transaction_status_description'
            ]);
    }

    public static function withoutGuest()
    {
        return self::where('email', '!=', 'guest@ninepinetech.com');
    }

    public static function getRegisteredToday()
    {
        return self::withoutGuest()->whereDate('created_at', '=', date('Y-m-d'));
    }

    public static function getRegisteredFromDate($date) // Y-m-d
    {
        return self::withoutGuest()->whereDate('created_at', '=', $date);
    }

    public static function getRegisteredFromWeek($day)
    {
        $from = date("Y-m-d", strtotime('monday this week'));
        $to = date("Y-m-d", strtotime("$day this week"));
        return self::getRegisteredFromRange($from, $to);
    }

    public static function getRegisteredFromYear($year)
    {
        return self::withoutGuest()->whereYear('created_at', '=', $year);
    }

    public static function getRegisteredFromRange($from, $to)
    {
        return self::withoutGuest()->whereBetween(DB::raw('date("created_at")'), [
            Carbon::createFromFormat('Y-m-d', $from)->subDay()->format('Y-m-d'),
            Carbon::createFromFormat('Y-m-d', $to)->addDay()->format('Y-m-d')
        ])->groupBy(DB::raw('date("created_at")'))
            ->orderBy(DB::raw('date("created_at")'), 'asc')
            ->select([
                DB::raw('count("id") as "count"'),
                DB::raw('date("created_at") as "dates"')
            ]);
    }

    public static function getRegisteredFromMonth($year_month)
    {
        $created_at = '"created_at"';
        return self::withoutGuest()->whereRaw("to_char($created_at, 'YYYY-MM') = '$year_month'");
    }

    public static function getRegisteredDataFromRange($from, $to)
    {
        $data = self::getRegisteredFromRange($from, $to);

        return [
            'count' => $data->pluck('count'),
            'dates' => $data->pluck('dates')
        ];
    }



    public static function getUserChargeDataFromDateRange($from, $to)
    {
        $data = self::withoutGuest()
            ->join('Transactions', 'Transactions.user_id', '=', 'users.id')
            ->join('TransactionLogs', 'Transactions.transaction_id', '=', 'TransactionLogs.transaction_id')
            ->join('TransactionStatuses', 'TransactionLogs.transaction_status_id', '=', 'TransactionStatuses.transaction_status_id')
            ->join('TransactionDetails', 'Transactions.transaction_id', '=', 'TransactionDetails.transaction_detail_id')
            ->join('Charges', 'TransactionDetails.charge_id', '=', 'Charges.charge_id')
            ->join('ChargeTypes', 'Charges.charge_type_id', '=', 'ChargeTypes.charge_type_id')
            ->whereBetween(DB::raw('date("TransactionLogs"."created_at")'), [
                Carbon::createFromFormat('Y-m-d', $from)->subDay()->format('Y-m-d'),
                Carbon::createFromFormat('Y-m-d', $to)->addDay()->format('Y-m-d')
            ])->groupBy('dates')
            ->groupBy('users')
            ->orderBy('dates', 'asc')
            ->select([
                DB::raw('sum("Charges"."price") as "amount"'),
                DB::raw('"users"."id" as "users"'),
                DB::raw('date("TransactionLogs"."created_at") as "dates"')
            ]);

        return [
            'amount' => $data->pluck('amount'),
            'users' => $data->pluck('users'),
            'dates' => $data->pluck('dates'),
        ];
    }
}
