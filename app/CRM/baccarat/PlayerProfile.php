<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class PlayerProfile extends Model
{
    //
    protected $connection = 'bit';
    protected $table = 'users';
    protected $primaryKey ='id';
}
