<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    protected $connection = 'bit';
    protected $table = 'BillingAddress';
    protected $primaryKey ='billing_address_id';

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'country',
        'city',
        'billing_address',
        'billing_address_second',
        'postalcode',
        'phone'
    ];

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }
}
