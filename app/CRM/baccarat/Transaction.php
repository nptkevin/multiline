<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Wallet_ledger;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaction extends Model
{
    protected $connection = 'bit';
    protected $table = 'Transactions';
    protected $primaryKey = 'transaction_id';
    protected $casts = [
        'sales' => 'double'
    ];

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function payment_info()
    {
        return $this->belongsTo(PaymentInfo::class, 'payment_info_id');
    }

    public function transaction_detail()
    {
        return $this->hasOne(TransactionDetail::class, 'transaction_id');
    }

    public function transaction_logs()
    {
        return $this->hasMany(TransactionLog::class, 'transaction_id');
    }

    public function wallet_ledger()
    {
        return $this->hasMany(Wallet_ledger::class, 'resource_id', 'transaction_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->whereRaw('"TransactionLogs"."transaction_log_id"::text LIKE \'%' . $search . '%\'')
            ->orWhereRaw('"TransactionLogs"."created_at"::text LIKE \'%' . $search . '%\'')
            ->orWhere('ChargeTypes.name', 'ILIKE', "%$search%")
            ->orWhere('Charges.description', 'ILIKE', "%$search%")
            ->orWhereRaw('"Charges"."price"::text LIKE \'%' . $search . '%\'')
            ->orWhere('TransactionStatuses.name', 'ILIKE', "%$search%");
    }

    public static function salesQuery()
    {
        return self::join('TransactionLogs', 'Transactions.transaction_id', '=', 'TransactionLogs.transaction_id')
            ->join('TransactionStatuses', 'TransactionLogs.transaction_status_id', '=', 'TransactionStatuses.transaction_status_id')
            ->join('TransactionDetails', 'Transactions.transaction_id', '=', 'TransactionDetails.transaction_detail_id')
            ->join('Charges', 'TransactionDetails.charge_id', '=', 'Charges.charge_id')
            ->join('ChargeTypes', 'Charges.charge_type_id', '=', 'ChargeTypes.charge_type_id');
    }

    public static function getAllSales()
    {
        $sales = self::salesQuery()->select([
            DB::raw('sum("TransactionDetails"."price") as "sales"')
        ])->first()->sales;
        return doubleval(is_null($sales) ? 0 : $sales);
    }

    public static function getSalesFromDate($date) // Y-m-d
    {
        $sales = self::salesQuery()->select([
            DB::raw('sum("TransactionDetails"."price") as "sales"')
        ])->whereDate('TransactionLogs.created_at', '=', $date)->first()->sales;
        return doubleval(is_null($sales) ? 0 : $sales);
    }


    public static function getSalesFromWeek($day)
    {
        $from = date("Y-m-d", strtotime('monday this week'));
        $to = date("Y-m-d", strtotime("$day this week"));
        return self::getSalesFromRange($from, $to);
    }

    public static function getSalesFromYear($year)
    {
        $sales = self::salesQuery()->select([
            DB::raw('sum("TransactionDetails"."price") as "sales"')
        ])->whereYear('TransactionLogs.created_at', '=', $year)->first()->sales;
        return doubleval(is_null($sales) ? 0 : $sales);
    }

    public static function getSalesFromRange($from, $to)
    {
        $sales = self::salesQuery()->select([
            DB::raw('sum("TransactionDetails"."price") as "sales"')
        ])->whereBetween(DB::raw('date("TransactionLogs"."created_at")'), [
            Carbon::createFromFormat('Y-m-d', $from)->subDay()->format('Y-m-d'),
            Carbon::createFromFormat('Y-m-d', $to)->addDay()->format('Y-m-d')
        ])->first()->sales;
        return doubleval(is_null($sales) ? 0 : $sales);
    }

    public static function getSalesFromMonth($year_month)
    {
        $created_at = '"TransactionLogs"."created_at"';
        $sales = self::salesQuery()->select([
            DB::raw('sum("TransactionDetails"."price") as "sales"')
        ])->whereRaw("to_char($created_at, 'YYYY-MM') = '$year_month'")
            ->first()
            ->sales;
        return doubleval(is_null($sales) ? 0 : $sales);
    }

    public static function getSalesDataFromRange($from, $to)
    {
        $data = self::salesQuery()
            ->whereBetween(DB::raw('date("TransactionLogs"."created_at")'), [
                Carbon::createFromFormat('Y-m-d', $from)->subDay()->format('Y-m-d'),
                Carbon::createFromFormat('Y-m-d', $to)->addDay()->format('Y-m-d')
            ])->groupBy('dates')
            ->orderBy('dates', 'asc')
            ->select([
                DB::raw('sum("TransactionDetails"."price") as "sales"'),
                DB::raw('date("TransactionLogs"."created_at") as "dates"')
            ]);

        return [
            'sales' => $data->pluck('sales'),
            'dates' => $data->pluck('dates')
        ];
    }
}
