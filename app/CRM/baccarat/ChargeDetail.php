<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class ChargeDetail extends Model
{
    protected $connection = 'bit';
    protected $table = 'ChargeDetails';
    protected $primaryKey = 'charge_details_id';

    protected $fillable = [
        'name',
        'description',
        'value',
        'charge_id'
    ];

    public function charge()
    {
        return $this->belongsTo(RegularCharges::class);
    }
}
