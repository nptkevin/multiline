<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class UserApplication extends Model
{
    protected $connection = 'bit';
    protected $table = 'UserApplications';
    protected $primaryKey = 'user_application_id';

    public function application()
    {
        return $this->belongsTo(Applications::class, 'application_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function accounts()
    {
        return $this->belongsTo(Accounts::class, 'user_id', 'id');
    }
}
