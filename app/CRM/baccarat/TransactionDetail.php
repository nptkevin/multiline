<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $connection = 'bit';
    protected $table = 'TransactionDetails';
    protected $primaryKey = 'transaction_detail_id';

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function charge()
    {
        return $this->belongsTo(RegularCharges::class, 'charge_id');
    }
}
