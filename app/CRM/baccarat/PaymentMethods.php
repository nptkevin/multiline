<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class PaymentMethods extends Model
{
    //
    protected $connection = 'bit';
    protected $table = 'PaymentMethods';
    protected $primaryKey ='payment_method_id';

    public function payment_info()
    {
        return $this->hasOne(PaymentInfo::class);
    }
}
