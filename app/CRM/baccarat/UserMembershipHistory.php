<?php

namespace App\CRM\baccarat;

use App\CRM\NinepineModels\Currency;
use Illuminate\Database\Eloquent\Model;

class UserMembershipHistory extends Model
{
    protected $connection = 'bit';
    protected $table = 'UserMembershipHistory';
    protected $primaryKey = 'user_membership_history_id';

    public function accounts()
    {
        return $this->belongsTo(Accounts::class, 'user_id', 'id');
    }

    public function membership()
    {
        return $this->belongsTo(MembershipCharges::class, 'membership_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function user_membership_inclusion_history()
    {
        return $this->hasMany(UserMembershipInclusionHistory::class, 'user_membership_history_id');
    }
}
