<?php

namespace App\CRM\baccarat;

use Illuminate\Database\Eloquent\Model;

class PaymentInfo extends Model
{
    protected $connection = 'bit';
    protected $table = 'PaymentInfo';
    protected $primaryKey = 'payment_info_id';

    public function account()
    {
        return $this->belongsTo(Accounts::class, 'user_id');
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethods::class, 'payment_method_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }
}
