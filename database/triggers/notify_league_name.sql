CREATE OR REPLACE FUNCTION notify_league_name()
RETURNS trigger AS
$BODY$
BEGIN
PERFORM pg_notify('notify_league_name', row_to_json(NEW)::text);
RETURN NULL;
END; 
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER notify_league_name
AFTER INSERT
ON "json_league_name"
FOR EACH ROW
EXECUTE PROCEDURE notify_league_name();