<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsernameUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DB::raw('"users"'), function (Blueprint $table) {
            $table->string('username',50)->nullable();
            $table->string('mobile',50)->nullable();
            $table->string('account_image',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DB::raw('"users"'), function (Blueprint $table) {
            $table->dropColumn(['username','mobile','account_image']);
        });
    }
}
