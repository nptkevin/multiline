<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCrmTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bit_crm')->create('CrmTransfer', function (Blueprint $table) {
            $table->increments('crm_transfer_id');
            $table->integer('user_id');
            $table->double('transfer_amount',10,2)->nullable();
            $table->integer('currency_id')->nullable();
            $table->integer('crm_user_id')->nullable();
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bit_crm')->dropIfExists('CrmTransfer');
    }
}
