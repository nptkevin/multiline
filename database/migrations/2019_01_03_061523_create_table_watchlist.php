<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWatchlist extends Migration{
	protected $tablename = "watchlist";

	public function up(){
		Schema::create($this->tablename, function(Blueprint $table){
			$table->increments('id');
			$table->integer('leagueinfo_id');
			$table->string('leaguename');
			$table->string('group_game_id');
			$table->string('game_schedule');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists($this->tablename);
	}
}