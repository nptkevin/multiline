<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGameTypeToJsonteamschedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('json_team_schedule', function (Blueprint $table) {
            //
               $table->string("game_schedule")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('json_team_schedule', function (Blueprint $table) {
            //
             $table->dropColumn(['game_schedule']);
        });
    }
}
