<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CRMCreateTableUsers extends Migration{
	public function up(){
		Schema::connection('bit_crm')->create('Users', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('firstname');
			$table->string('lastname');
			$table->integer('status_id');
			$table->string('email')->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	public function down(){
		Schema::connection('bit_crm')->dropIfExists('Users');
	}
}