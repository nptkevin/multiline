<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWalletLedger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bit_crm')->create('Wallet_ledger', function (Blueprint $table) {
            $table->increments('wallet_ledger_id');
            $table->integer('wallet_id')->nullable();
            $table->integer('source_id')->nullable();
            $table->integer('resource_id')->nullable();
            $table->double('debit',10,2)->nullable();
            $table->double('credit',10,2)->nullable();
            $table->integer('application_id')->nullable();
            $table->double('balance',10,2)->nullable();
            $table->text('json_data_output')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bit_crm')->dropIfExists('Wallet_ledger');
    }
}
