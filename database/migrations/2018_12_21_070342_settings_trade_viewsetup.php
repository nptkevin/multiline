<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsTradeViewsetup extends Migration{
	protected $tablename = "settings_trade_viewsetup";

	public function up(){
		Schema::create($this->tablename, function(Blueprint $table){
			$table->increments('id');
			$table->integer('userid');
			$table->string('viewsetup', 20);
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists($this->tablename);
	}
}