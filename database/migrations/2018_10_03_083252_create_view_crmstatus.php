<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewCRMStatus extends Migration{
	public function up(){
		DB::statement("CREATE OR REPLACE VIEW crmstatus AS
					SELECT
						st.*
					FROM
						dblink(
							'dbname=" . env('DB_DATABASE_CRM') . "',
							'SELECT
								status_id,
								status_type,
								status_name
							FROM
								public.\"Statuses\"'
						) AS st(
							status_id INT,
							status_type CHARACTER VARYING,
							status_name CHARACTER VARYING
						);");
	}

	public function down(){
		DB::statement('DROP VIEW IF EXISTS crmstatus');
	}
}