<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsTradeBetslip extends Migration{
	protected $tablename = "settings_trade_betslip";

	public function up(){
		Schema::create($this->tablename, function(Blueprint $table){
			$table->increments('id');
			$table->integer('userid');
			$table->boolean('conf_betplacement');
			$table->boolean('show_betslip_agent');
			$table->boolean('use_equiv_bets');
			$table->boolean('allow_offer_exchange');
			$table->boolean('autoinsert_thou_separator');
			$table->string('betslip_multi_acc', 20);
			$table->string('betslip_price', 20);
			$table->double('max_bet', 13, 2);
			$table->double('stake_def', 13, 2);
			$table->double('betslip_price_def', 13, 2);
			$table->integer('expiry_def');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists($this->tablename);
	}
}