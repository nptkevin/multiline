<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bit_crm')->create('Currency', function(Blueprint $table){
            $table->increments('currency_id');
            $table->string('currency_name');
            $table->string('currency_symbol');

            $table->string('currency');
            $table->boolean('default_registration');
            $table->boolean('app_currency');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bit_crm')->dropIfExists('Currency');
    }
}
