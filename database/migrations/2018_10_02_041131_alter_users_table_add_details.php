<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(DB::raw('"users"'), function (Blueprint $table) {
            $table->string('first_name',50)->nullable();
            $table->string('last_name',50)->nullable();
            $table->string('country',50)->nullable();
            $table->string('gender',25)->nullable();
            $table->date('birth_date')->nullable();
            $table->integer('status_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(DB::raw('"users"'), function (Blueprint $table) {
            $table->dropColumn(['first_name','last_name','status_id','country','gender', 'birth_date']);
        });
    }
}
