<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusesTable extends Migration{
	public function up(){
		Schema::connection('bit_crm')->create('Statuses', function(Blueprint $table){
			$table->increments('status_id');
			$table->string('status_type');
			$table->string('status_name');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::connection('bit_crm')->dropIfExists('Statuses');
	}
}