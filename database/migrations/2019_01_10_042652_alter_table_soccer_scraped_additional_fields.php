<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableSoccerScrapedAdditionalFields extends Migration{
	protected $tablename = "soccer_scraped";
	protected $fields = [
		'fh_1x2_id',
		'fh_1x2_onclick',
		'fh_handicap_1',
		'fh_handicap_2',
		'fh_handicap_id',
		'fh_handicap_onclick',
		'fh_ou_1',
		'fh_ou_2',
		'fh_ou_3',
		'fh_ou_id',
		'fh_ou_onclick',
		'ft_1x2_id',
		'ft_1x2_onclick',
		'ft_handicap_1',
		'ft_handicap_2',
		'ft_handicap_id',
		'ft_handicap_onclick',
		'ft_oe_id',
		'ft_oe_onclick',
		'ft_oe1',
		'ft_oe2',
		'ft_ou_id',
		'ft_ou_onclick',
		'ft_ou1',
		'ft_ou2',
		'ft_ou3',
	];

	public function up(){
		Schema::table(DB::raw($this->tablename), function(Blueprint $table){
			foreach($this->fields AS $row):
				$table->string($row)->nullable();
			endforeach;
		});
	}

	public function down(){
		Schema::table(DB::raw($this->tablename), function(Blueprint $table){
			$table->dropColumn($this->fields);
		});
	}
}