<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegfieldLeagueinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leagueinfo', function (Blueprint $table) {
            //
            $table->string('leg_league_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leagueinfo', function (Blueprint $table) {
            $table->dropColumn(['leg_league_id']);
            //
        });
    }
}
