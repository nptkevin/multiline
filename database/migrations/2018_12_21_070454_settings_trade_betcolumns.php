<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsTradeBetcolumns extends Migration{
	protected $tablename = "settings_trade_betcolumns";

	public function up(){
		Schema::create($this->tablename, function(Blueprint $table){
			$table->increments('id');
			$table->integer('userid');
			$table->boolean('ft_1x2');
			$table->boolean('ft_handicap');
			$table->boolean('ft_ou');
			$table->boolean('ft_oe');
			$table->boolean('fh_1x2');
			$table->boolean('fh_handicap');
			$table->boolean('fh_ou');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists($this->tablename);
	}
}