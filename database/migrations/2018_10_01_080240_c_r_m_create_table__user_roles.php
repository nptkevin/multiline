<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CRMCreateTableUserRoles extends Migration{
	public function up(){
		Schema::connection('bit_crm')->create('UserRoles', function (Blueprint $table) {
			$table->increments('user_role_id');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::connection('bit_crm')->drop('UserRoles');
	}
}