<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJsonTeamSchedule extends Migration{
	public function up(){
		Schema::create('json_team_schedule', function(Blueprint $table){
			$table->increments('id');
			$table->json('schedule_team_ids');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists('json_team_schedule');
	}
}