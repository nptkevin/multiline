<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSoccerScraped extends Migration{
	public function up(){
		Schema::create('soccer_scraped', function(Blueprint $table){
			$table->increments('id');
			$table->string('schedule');
			$table->string('team');
			$table->string('ft_1x2');
			$table->string('ft_handicap');
			$table->string('ft_ou');
			$table->string('ft_oe');
			$table->string('fh_1x2');
			$table->string('fh_handicap');
			$table->string('fh_ou');
			$table->string('league_name');
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists('soccer_scraped');
	}
}