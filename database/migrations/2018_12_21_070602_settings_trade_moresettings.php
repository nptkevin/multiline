<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SettingsTradeMoresettings extends Migration{
	protected $tablename = "settings_trade_moresettings";

	public function up(){
		Schema::create($this->tablename, function(Blueprint $table){
			$table->increments('id');
			$table->integer('userid');
			$table->boolean('sounds');
			$table->integer('bookie_comp')->nullable();
			$table->longText('bookie_price')->nullable();
			$table->string('reselect_acc', 50);
			$table->timestamps();
		});
	}

	public function down(){
		Schema::dropIfExists($this->tablename);
	}
}