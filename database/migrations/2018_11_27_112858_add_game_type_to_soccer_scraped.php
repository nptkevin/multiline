<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGameTypeToSoccerScraped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soccer_scraped', function (Blueprint $table) {
            //
            $table->string("game_schedule")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soccer_scraped', function (Blueprint $table) {
            //
              $table->dropColumn(['game_schedule']);
        });
    }
}
