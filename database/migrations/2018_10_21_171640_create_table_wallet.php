<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bit_crm')->create('Wallet', function (Blueprint $table) {
            $table->increments('wallet_id');
            $table->integer('user_id');
            $table->double('balance',10,2);
            $table->integer('currency_id')->nullable();
            $table->integer('source_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bit_crm')->dropIfExists('Wallet');
    }
}
