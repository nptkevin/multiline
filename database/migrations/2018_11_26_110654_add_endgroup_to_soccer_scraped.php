<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEndgroupToSoccerScraped extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soccer_scraped', function (Blueprint $table) {
            //
            $table->string('endgroup')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soccer_scraped', function (Blueprint $table) {
            //
             $table->dropColumn(['endgroup']);
        });
    }
}
