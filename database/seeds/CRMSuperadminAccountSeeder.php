<?php

use App\CRM\NinepineModels\Status;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CRMSuperadminAccountSeeder extends Seeder{
	public function run(){
		DB::connection('bit_crm')->table('Users')->insert([
			'name' => 'Super Admin',
			'firstname' => 'Super',
			'lastname' => 'Admin',
			'status_id' => Status::where('status_type', 'User')->where('status_name', 'Active')->first()->status_id,
			'email' => 'superadmin@ninepinetech.com',
			'password' => bcrypt('9pinesecurity@dmin'),
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);
	}
}