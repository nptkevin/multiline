<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultUserSettingsSeeder extends Seeder{
	public function run(){
		DB::table('settings_trade_viewsetup')->insert([
			'userid' => 0,
			'viewsetup' => 'Asian',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('settings_trade_betcolumns')->insert([
			'userid' => 0,
			'ft_1x2' => true,
			'ft_handicap' => true,
			'ft_ou' => true,
			'ft_oe' => true,
			'fh_1x2' => true,
			'fh_handicap' => true,
			'fh_ou' => true,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('settings_trade_betslip')->insert([
			'userid' => 0,
			'conf_betplacement' => false,
			'show_betslip_agent' => true,
			'use_equiv_bets' => false,
			'allow_offer_exchange' => true,
			'autoinsert_thou_separator' => false,
			'betslip_multi_acc' => 'all',
			'betslip_price' => 'best',
			'max_bet' => 40000,
			'stake_def' => 20000,
			'betslip_price_def' => 40000,
			'expiry_def' => 10,
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);

		DB::table('settings_trade_moresettings')->insert([
			'userid' => 0,
			'sounds' => false,
			'bookie_comp' => 0,
			'bookie_price' => '',
			'reselect_acc' => 'never',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
	}
}