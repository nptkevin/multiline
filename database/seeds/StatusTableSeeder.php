<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder{
	public function run(){
		DB::connection('bit_crm')->table('Statuses')->insert([
			'status_type' => 'User',
			'status_name' => 'Active',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);

		DB::connection('bit_crm')->table('Statuses')->insert([
			'status_type' => 'User',
			'status_name' => 'Inactive',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);

		DB::connection('bit_crm')->table('Statuses')->insert([
			'status_type' => 'Account',
			'status_name' => 'Active',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);

		DB::connection('bit_crm')->table('Statuses')->insert([
			'status_type' => 'Account',
			'status_name' => 'Inactive',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now()
		]);
	}
}