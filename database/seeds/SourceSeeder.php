<?php

use Illuminate\Database\Seeder;

class SourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'Charges'

        ]);
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'Membership'

        ]);
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'CRM'

        ]);
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'Wallet'

        ]);
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'Friend'

        ]);
        DB::connection('bit_crm')->table('Sources')->insert([
            'source_name' =>'Register'

        ]);
    }
}
