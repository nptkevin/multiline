<?php

use Illuminate\Database\Seeder;

class CurrencyDefaultValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('bit_crm')->table('Currency')->insert([
            'currency_name' =>'Chinese Yuan',
            'currency_symbol' =>'CNY',
            'currency' =>'CNY',
            'default_registration'=>'1',
            'app_currency' => '0',
        ]);

//        DB::table('Currency')->insert([
//            'currency_name' =>'Philippine Peso',
//            'currency_symbol' =>'PHP',
//            'currency' =>'PHP',
//            'default_registration' => '0',
//            'app_currency' => '0',
//        ]);
//        DB::table('Currency')->insert([
//            'currency_name' =>'US Dollar',
//            'currency_symbol' =>'USD',
//            'currency' =>'USD',
//            'default_registration'=>'0',
//            'app_currency' => '0',
//        ]);
    }
}
