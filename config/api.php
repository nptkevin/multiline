<?php
	/**
	 * PokerAsia API Configurations and Endpoints.
	 *
	 *
	**/
	return [
		'url' => env('API_URL', 'http://ninepine-api.local/api/'),
		'endpoints' => [
			'user' => [
				'login' => 'user/login/RakeBackAsia',
				'profile' => 'user/profile/RakeBackAsia',
				'register' => 'user/web_register/RakeBackAsia',
				'logout' => 'user/logout',
			],
            'wallet' => [
                'mywallet' => 'wallet/mywallet'
            ]
		],
	];
