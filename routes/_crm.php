<?php

Route::namespace('CRM')->prefix('admin')->group(function(){
	Route::get('/', function(){
		return redirect('admin/login');
	});

	Auth::routes();

	Route::namespace('Dashboard')->prefix('dashboard')->group(function(){
		Route::get('/', 'DashboardController@index')->name('crm.dashboard.index');
	});

	Route::get('logout', 'Auth\LoginController@logout');
	Route::get('home', 'HomeController@index')->name('home');

	Route::namespace('Security')->prefix('security')->group(function(){
		Route::get('permissions', 'PermissionController@index')->name('permissions.index');
		Route::post('permissions/add', 'PermissionController@add')->name('permissions.add');
		Route::put('permissions/edit', 'PermissionController@edit')->name('permissions.edit');
		// Route::delete('roles/delete/{module}', 'RoleController@delete')->name('roles.delete');
		// Route::get('roles/{role}/json', 'RoleController@json')->name('roles.json');
		Route::get('roles', 'RoleController@index')->name('roles.index');
		// Route::post('roles/add', 'RoleController@add')->name('roles.add');
		// Route::put('roles/{module}', 'RoleController@edit')->name('roles.edit');
		Route::get('assignuser', 'AssignUserController@index')->name('assignuser.index');
		Route::post('assignuser/add', 'AssignUserController@add')->name('assignuser.add');
		Route::delete('assignuser/delete/{assignuser}', 'AssignUserController@delete')->name('assignuser.delete');
		Route::put('assignuser/{assignuser}', 'AssignUserController@update')->name('assignuser.update');
		Route::put('assignuser/{assignuser}/password/reset', 'AssignUserController@passwordReset')->name('assignuser.passwordReset');
		// Route::get('userimg/{dir}/{image}', 'AssignUserController@getImage')->name('assignuser.getImage');
	});

	Route::namespace('CMS')->prefix('cms')->group(function(){
		Route::get('categories/json', 'CategoryController@json')->name('categories.json');
		Route::get('categories/parent/{parent}/positions', 'CategoryController@positions')->name('categories.positions');
		Route::get('categories/datatable', 'CategoryController@dataTable')->name('categories.dataTable');

		Route::resource('categories', 'CategoryController', ['only' => [
			'index', 'store', 'update', 'destroy'
		]]);

		Route::post('contents/image/upload', 'ContentController@upload')->name('contents.upload');
		Route::get('contents/datatable', 'ContentController@dataTable')->name('contents.dataTable');
		Route::resource('contents', 'ContentController', ['except' => ['show']]);
	});

	Route::namespace('News')->prefix('products')->group(function(){
		Route::get('/', 'NewsController@index')->name('crm.products.index');
		Route::get('dataTable', 'NewsController@dataTable')->name('crm.products.datatable');
		Route::get('create', 'NewsController@create')->name('crm.products.create');
		Route::post('store', 'NewsController@store')->name('crm.products.store');
		Route::post('image/upload', 'NewsController@upload')->name('crm.products.upload');
		Route::get('{image}', 'NewsController@getImage')->name('crm.products.getImage');
		Route::get('/getproducts/{id}', 'NewsController@formedit')->name('crm.products.edit');
		Route::put('update', 'NewsController@update')->name('crm.products.update');
		Route::post('delete/{id}', 'NewsController@destroy')->name('crm.products.delete');
	});

	Route::namespace('News')->prefix('promotions')->group(function(){
		Route::get('/', 'NewsController@index')->name('crm.promotions.index');
		Route::get('dataTable', 'NewsController@dataTable')->name('crm.promotions.datatable');
		Route::get('create', 'NewsController@create')->name('crm.promotions.create');
		Route::post('store', 'NewsController@store')->name('crm.promotions.store');
		Route::post('image/upload', 'NewsController@upload')->name('crm.promotions.upload');
		Route::get('{image}', 'NewsController@getImage')->name('crm.promotions.getImage');
		Route::get('/getpromotions/{id}', 'NewsController@formedit')->name('crm.promotions.edit');
		Route::put('update', 'NewsController@update')->name('crm.promotions.update');
		Route::post('delete/{id}', 'NewsController@destroy')->name('crm.promotions.delete');
	});

	Route::namespace('Accounts')->group(function(){
		Route::resource('accounts.billing_addresses', 'BillingAddressController', ['only' => [
			'index', 'store', 'update'
		]]);

		Route::prefix('security_question')->group(function(){
			Route::get('/', 'SecurityQuestionController@index')->name('security_question.index');
			Route::get('list', 'SecurityQuestionController@fetchSecurityQuestionDatable')->name('security_question.dataTable');
			Route::post('create', 'SecurityQuestionController@create')->name('security_question.create');
			Route::post('delete/{id}', 'SecurityQuestionController@delete')->name('security_question.delete');
			Route::post('update/{id}', 'SecurityQuestionController@update')->name('security_question.update');
		});

		Route::prefix('accounts')->group(function(){
			Route::get('/', 'AccountsController@index')->name('accounts.index');
			Route::get('datatable', 'AccountsController@dataTable')->name('accounts.dataTable');
			Route::get('generate_username', 'AccountsController@generateUsername')->name('generate.username');
			Route::post('change_password', 'AccountsController@changePassword')->name('accounts.change_pwd');
			Route::post('add', 'AccountsController@addAccount')->name('accounts.add');
			Route::get('{account}', 'AccountsController@details')->name('accounts.details');
			Route::put('{account}', 'AccountsController@update')->name('accounts.update');

		});
	});

	Route::namespace('Status')->group(function(){
		Route::get('status/', 'StatusController@index')->name('status.index');
		Route::post('status/save', 'StatusController@store')->name('status.store');
		Route::get('status/edit/{id}', 'StatusController@edit')->name('status.edit');
		Route::put('status/update', 'StatusController@update')->name('status.update');
	});


	Route::namespace('Wallet')->prefix('wallet')->group(function () {
		Route::get('registrations/avatars', 'RegistrationController@avatars')->name('wallet.registrations.avatars');

		Route::resource('registrations', 'RegistrationController', ['only' => [
			'index', 'store', 'update'
		]]);

		Route::resource('daily_bonuses', 'DailyBonusController', ['only' => [
			'index', 'store', 'update'
		]]);

		Route::prefix('exchange_rates')->group(function () {
			Route::get('datatable', 'ExchangeRateController@dataTable')->name('wallet.exchange_rates.dataTable');
		});

		Route::resource('exchange_rates', 'ExchangeRateController', ['only' => [
			'index', 'store', 'update'
		]]);

		Route::prefix('currencies')->group(function () {
			Route::get('datatable', 'CurrencyController@dataTable')->name('wallet.currencies.dataTable');
		});

		Route::resource('currencies', 'CurrencyController', ['only' => [
			'index', 'store', 'update'
		]]);

		Route::prefix('transfer')->group(function () {
			Route::get('/', 'TransferController@index')->name('wallet.transfer.index');
			Route::post('/', 'TransferController@transfer')->name('wallet.transfer.transfer');
			Route::get('datatable', 'TransferController@dataTable')->name('wallet.transfer.dataTable');
		});

		Route::get('ledger/{ledger}/source-info', 'WalletController@ledgerSourceInfo')
			->name('wallet.ledgerSourceInfo');
		Route::get('{wallet}/datatable', 'WalletController@dataTable')->name('wallet.dataTable');
	});
});