<?php

/*
|--------------------------------------------------------------------------
| WEB Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){ return view('index'); });

Route::get('trade', 'web\TradeController@index')->name('trade');
Route::post('trade/data', 'web\TradeController@getData')->name('trade.data');
Route::post('trade/leaguedata', 'web\TradeController@getLeagueData')->name('trade.leaguedata');
Route::post('trade/watchlist/save', 'web\TradeController@saveWatchlist')->name('trade.watchlist.save');
Route::post('trade/watchlist/delete', 'web\TradeController@removeWatchlist')->name('trade.watchlist.delete');

Route::get('history', function(){ return view('history'); })->name('history');
Route::get('openorders', function(){ return view('openorders'); })->name('openorders');

Route::get('settings/trade', 'web\SettingsController@index')->name('settings.trade');
Route::post('settings/trade/save', 'web\SettingsController@saveTradeSettings')->name('settings.trade.save');

Route::get('/login', function(){ return view('auth.login'); })->name('web.login.form');
Route::get('/register', function(){ return view('auth.register'); })->name('web.register.form');

Route::prefix('web')->group(function(){
	Route::post('/login', 'Auth\FrontLoginController@login')->name('web.login');
	Route::post('/register', 'Auth\RegisterController@create')->name('web.register');
	Route::post('/logout', 'Auth\FrontLoginController@logout')->name('web.logout');
});

/*
|--------------------------------------------------------------------------
| CRM Routes
|--------------------------------------------------------------------------
|
| Here is where you can register CRM routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "auth:crm" middleware group. Now create something great!
|
*/
include('_crm.php');