$(function(){
	$(document).find('.card-auth-body .form-control').each(function(){
		lift($(this), 'load');
	});

	$('.card-auth-body .form-control').on('focus', function(){
		lift($(this), 'focus');
	}).on('blur', function(){
		lift($(this), 'blur');
	});

	$(document).on('click', '.label-float', function(){
		$(this).closest('.form-group').find('.form-control').focus();
	});
});

/**
 * lift() function
 */
function lift($control, $case){
	var $value = $($control).val().length;

	switch($case){
		case "blur":
			if($value > 0)
				$($control).closest('.form-group').find('.label-float').addClass('lift');
			else
				$($control).closest('.form-group').find('.label-float').removeClass('lift');
			break;

		case "focus":
			$($control).closest('.form-group').find('.label-float').addClass('lift');
			break;

		case "load":
			var $ctrl = $($control).is(':focus');

			if($ctrl)
				$($control).closest('.form-group').find('.label-float').addClass('lift');
			else{
				if($value > 0)
					$($control).closest('.form-group').find('.label-float').addClass('lift');
				else
					$($control).closest('.form-group').find('.label-float').removeClass('lift');
			}

			break;
	}
};