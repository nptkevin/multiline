function showError(){
        Tawk_API.maximize();
}

$('a').click(function(e){
    e.preventDefault();
    var current_target = $(e.currentTarget);
    var base_url = window.location.origin;
    console.log(current_target.context.href == "" && current_target.context.className.indexOf("login-handler") < 0);
    if(current_target.context.href.indexOf(base_url) < 0 && current_target.context.href.indexOf("#") < 0  && current_target.context.href.indexOf("#") < 0 || current_target.context.href == "" )
    {
        if(current_target.context.className.indexOf("login-handler") < 0)
        showError();
    }
    else{
        if(current_target.context.rel != "nofollow" && current_target.context.className.indexOf("login-handler") < 0 && current_target.context.href.indexOf("#") < 0){
            $.ajax(current_target.context.href, {
                statusCode: {
                    404: function() {
                        showError();
                    },
                    200: function () {
                        window.location.href = current_target.context.href;
                    }

                }
            });
        }
    }
});

$('input[type="submit"]').click(function(e){
    e.preventDefault();
    showError();
});

$(window).load(function(){
    $('.lwc-button-icon > svg').html("");
    $('.lwc-button-icon').html("<span style='font-size:20px;'><i class='fab fa-skype'></i></span>");
});


$('#linkPokerRooms').click(function(e){
    e.preventDefault();

    var current_url = window.location.href;
    var base_url = window.location.origin + "/";
    if(current_url != base_url){
          window.location ="/#";
    }
});
