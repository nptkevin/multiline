$(function(){
	var $modalToggleAnimateDuration = 300;

	$('[data-toggle="modal"]').on('click', function(){
		var $this = $(this),
			$target = $this.data('target');

		$($target).fadeIn($modalToggleAnimateDuration);
	});

	$('[data-dismiss="modal"]').on('click', function(){
		$('.modal').fadeOut($modalToggleAnimateDuration);
	});
});